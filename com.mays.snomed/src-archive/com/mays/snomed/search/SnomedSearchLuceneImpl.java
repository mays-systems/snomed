/**
 * Copyright 2008-2017 Mays Systems LLC.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *       http://www.apache.org/licenses/LICENSE-2.0
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package com.mays.snomed.search;

import java.util.Arrays;
import java.util.Collection;
import java.util.List;

import org.apache.lucene.document.Document;

import com.mays.snomed.ConceptBasic;
import com.mays.snomed.SnomedDb;
import com.mays.util.search.SearchLucene;

@Deprecated
public class SnomedSearchLuceneImpl extends SearchLucene implements
		SearchFileWriter {

	// public final static String LUCENE_HOME_KEY = "lucene_home";

	// public static final Set<Long> EMPTY_CONS = Collections.emptySet();

	// public static Logger LOGGER;

	// private Connection conn;

	// private IndexSearcher isearcher;

	// private Analyzer analyzer;

	public SnomedSearchLuceneImpl() {
		super();
		// this.conn = conn;
	}

	// public void init(String lucene_home) throws Exception {
	// this.analyzer = new SnomedAnalyzer();
	// this.isearcher = new IndexSearcher(lucene_home);
	// }

	// public void shutdown() throws Exception {
	// this.isearcher.close();
	// }

	// public ArrayList<String> getTokens(String term) throws Exception {
	// ArrayList<String> ret = new ArrayList<String>();
	// TokenStream ts = this.analyzer.tokenStream("contents",
	// new StringReader(term));
	// Token token;
	// while ((token = ts.next()) != null) {
	// ret.add(token.termText());
	// }
	// return ret;
	// }

	// public String getTokenLine(String term, String delim) throws Exception {
	// List<String> tokens = this.getTokens(term);
	// String ret = "";
	// for (String str : tokens) {
	// ret += str + delim;
	// }
	// return ret;
	// }

	// public ArrayList<Document> search(String term, int page, int page_size)
	// throws Exception {
	// Set<Long> cons = Collections.emptySet();
	// return this.search(term, page, page_size, cons, cons);
	// }

	// // This doesn't belong here
	// public static SortedSet<String> suggest_words_search;

	// public ArrayList<Document> search(String term, int page, int page_size,
	// Collection<Long> includes, Collection<Long> excludes)
	// throws Exception {
	// ArrayList<Document> ret = new ArrayList<Document>();
	// BooleanQuery query = new BooleanQuery();
	// for (String token : getTokens(term)) {
	// query.add(new TermQuery(new Term("contents", token)),
	// BooleanClause.Occur.SHOULD);
	// }
	// BooleanQuery includes_query = null;
	// for (long id : includes) {
	// if (includes_query == null)
	// includes_query = new BooleanQuery();
	// includes_query.add(
	// new TermQuery(new Term("root", String.valueOf(id))),
	// BooleanClause.Occur.SHOULD);
	// }
	// if (includes_query != null) {
	// includes_query.setMinimumNumberShouldMatch(1);
	// query.add(includes_query, BooleanClause.Occur.MUST);
	// }
	// for (long id : excludes) {
	// query.add(new TermQuery(new Term("root", String.valueOf(id))),
	// BooleanClause.Occur.MUST_NOT);
	// }
	// Hits hits = isearcher.search(query);
	// if (page <= 0)
	// throw new Exception("page <= 0: " + page);
	// for (int i = (page - 1) * page_size; i < page * page_size; i++) {
	// if (i >= hits.length())
	// break;
	// ret.add(hits.doc(i));
	// }
	// // This doesn't belong here
	// if (page == 1) {
	// suggest_words_search = new TreeSet<String>();
	// int num_results = Math.min(100, hits.length());
	// for (int i = 0; i < num_results; i++) {
	// for (String word : hits.doc(i).get("contents")
	// .split(SnomedSearchBuildLuceneIndex.WORD_SPLIT)) {
	// suggest_words_search.add(word.toLowerCase());
	// }
	// }
	// }
	// return ret;
	// }

	// public ArrayList<Long> searchConcepts(String term, int page, int
	// page_size,
	// Collection<Long> includes, Collection<Long> excludes)
	// throws Exception {
	// ArrayList<Long> ret = new ArrayList<Long>();
	// for (Document doc : this.search(term, page, page_size, includes,
	// excludes)) {
	// String id = doc.getField("id").stringValue();
	// ret.add(Long.parseLong(id));
	// }
	// return ret;
	// }

	public String getTermLine(String term) {
		return "Term:\t" + term;
	}

	public String getHitLine(String description, ConceptBasic con) {
		return "\tHit:\t" + description + "\t\t\t\t\t"
				+ con.getFullySpecifiedName() + "\t\t\t\t\t"
				+ con.getConceptId();
	}

	public String getHitTags(ConceptBasic con) throws Exception {
		return "";
	}

	public void doSearch(String term, SnomedDb snomed_db, boolean token_trace,
			SearchFileWriter sfw) throws Exception {
		this.doSearch(term, snomed_db, SnomedSearchLuceneImpl.EMPTY_CONS,
				SnomedSearchLuceneImpl.EMPTY_CONS, token_trace, sfw);
	}

	public void doSearch(String term, SnomedDb snomed_db,
			Collection<Long> includes, Collection<Long> excludes,
			boolean token_trace, SearchFileWriter sfw) throws Exception {
		this.doSearch(term, snomed_db, includes, excludes, token_trace, sfw,
				false);
	}

	// public Document findExactMatch(String term, List<Document> docs)
	// throws Exception {
	// Set<String> term_toks = new TreeSet<String>(this.getTokens(term));
	// for (Document doc : docs) {
	// Set<String> doc_toks = new TreeSet<String>(this.getTokens(doc
	// .getField("contents").stringValue()));
	// if (doc_toks.equals(term_toks)) {
	// return doc;
	// }
	// }
	// return null;
	// }

	public void doSearch(String term, SnomedDb snomed_db,
			Collection<Long> includes, Collection<Long> excludes,
			boolean token_trace, SearchFileWriter sfw, boolean exact_cutoff_p)
			throws Exception {
		LOGGER.info(sfw.getTermLine(term));
		if (token_trace) {
			for (Long id : includes) {
				LOGGER.info("Include: "
						+ snomed_db.getConcept(id).getLogString());
			}
			for (Long id : excludes) {
				LOGGER.info("Exclude: "
						+ snomed_db.getConcept(id).getLogString());
			}
			LOGGER.info("Tokens:\t" + this.getTokenLine(term, "\t"));
		}
		List<Document> docs = this.search(term, 1, 20, includes, excludes);
		if (exact_cutoff_p) {
			Document exact_doc = findExactMatch(term, docs);
			if (exact_doc != null)
				docs = Arrays.asList(exact_doc);
		}
		for (Document doc : docs) {
			String id = doc.getField("id").stringValue();
			ConceptBasic con = snomed_db.getConcept(Long.parseLong(id));
			LOGGER.info(sfw.getHitLine(doc.getField("contents").stringValue(),
					con));
			if (token_trace) {
				LOGGER.info("\tTokens:\t"
						+ this.getTokenLine(doc.getField("contents")
								.stringValue(), "\t"));
				LOGGER.info("\tRoot:\t"
						+ this.getTokenLine(doc.getField("root").stringValue(),
								"\t"));
			}
		}
	}

	// public ArrayList<String> suggestWords(String sub_word, int limit,
	// boolean use_context) {
	// ArrayList<String> ret = new ArrayList<String>();
	// // BasicLogger.trace(sub_word + " " + limit);
	// int mcc = BooleanQuery.getMaxClauseCount();
	// try {
	// if (use_context) {
	// for (String word : suggest_words_search) {
	// if (word.startsWith(sub_word))
	// ret.add(word);
	// if (ret.size() == limit)
	// break;
	// }
	// }
	// if (ret.size() < limit) {
	// BooleanQuery.setMaxClauseCount(Integer.MAX_VALUE);
	// Hits hits;
	// hits = isearcher.search(new WildcardQuery(new Term("word",
	// sub_word + "*")), new Sort("freq", true));
	// for (int i = 0; i < limit; i++) {
	// if (ret.size() == limit)
	// break;
	// if (i >= hits.length())
	// break;
	// // BasicLogger.trace(hits.doc(i));
	// ret.add(hits.doc(i).getField("word").stringValue());
	// }
	// }
	// } catch (IOException ex) {
	// // TODO Auto-generated catch block
	// ex.printStackTrace();
	// } finally {
	// BooleanQuery.setMaxClauseCount(mcc);
	// }
	// return ret;
	// }

}
