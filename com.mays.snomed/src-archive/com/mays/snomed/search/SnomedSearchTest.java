/**
 * Copyright 2008-2017 Mays Systems LLC.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *       http://www.apache.org/licenses/LICENSE-2.0
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package com.mays.snomed.search;

import java.sql.Connection;
import java.util.HashMap;

@Deprecated
public class SnomedSearchTest {

	Connection conn;

	private HashMap<String, Integer> words = new HashMap<String, Integer>();

	public void init(Connection conn) throws Exception {
		this.conn = conn;
	}

	private void build() throws Exception {
		// String query = "";
		// query += "SELECT description_id, description_status, concept_id, ";
		// query += " term, initial_capital_status, description_type, ";
		// query += " language_code ";
		// query += " FROM description ";
		// Statement stmt = this.conn.createStatement();
		// ResultSet res = stmt.executeQuery(query);
		// while (res.next()) {
		// // long description_id = res.getLong(1);
		// int description_status = res.getInt(2);
		// // long concept_id = res.getLong(3);
		// String term = res.getString(4);
		// // int initial_capital_status = res.getInt(5);
		// int description_type = res.getInt(6);
		// // String language_code = res.getString(7);
		// if (!Snomed.isStatusActive(description_status))
		// continue;
		// if (Snomed.isDescriptionTypeFullySpecified(description_type))
		// continue;
		// String[] words = term.split("\\s|-|/");
		// for (String word : words) {
		// word = word.toLowerCase();
		// word = word.replaceAll(",*$", "");
		// word = word.replaceAll(":*$", "");
		// word = word.replaceAll(";*$", "");
		// word = word.replaceAll("'s$", "");
		// word = word.replaceAll("\\)*$", "");
		// word = word.replaceAll("^\\)*", "");
		// word = word.replaceAll("\\(*$", "");
		// word = word.replaceAll("^\\(*", "");
		// word = word.replaceAll("^\\[.\\]", "");
		// Integer wc = this.words.get(word);
		// if (wc == null)
		// wc = 0;
		// wc++;
		// this.words.put(word, wc);
		// }
		// }
		// System.out.println("words#: " + words.size());
		// int nwc = 0;
		// for (String word : words.keySet()) {
		// if (word.matches(".*\\W.*")) {
		// System.out.println(word + "\t" + this.words.get(word));
		// nwc++;
		// }
		// }
		// System.out.println("nwc#: " + nwc);
	}

	private void run() throws Exception {
		this.build();
	}

	public static void main(String argv[]) {
		try {
			SnomedSearchTest m = new SnomedSearchTest();
			m.run();
		} catch (Exception ex) {
			System.out.println(ex.getMessage());
			ex.printStackTrace();
		}
	}

}
