/**
 * Copyright 2008-2017 Mays Systems LLC.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *       http://www.apache.org/licenses/LICENSE-2.0
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package com.mays.snomed.search;

import java.sql.Connection;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashMap;
import java.util.SortedMap;
import java.util.SortedSet;

import com.mays.util.search.SearchResult;

@Deprecated
public class SnomedSearch {

	private static final String WORD_SPLIT = "\\W";

	Connection conn;

	private SortedSet<String> words;

	private SortedMap<String, SearchResult> terms;

	private HashMap<Long, String> concept_ids;

	public void init(Connection conn) throws Exception {
		this.conn = conn;
		this.build();
	}

	private void build() throws Exception {
		// this.words = new TreeSet<String>();
		// this.terms = new TreeMap<String, SearchResult>();
		// this.concept_ids = new HashMap<Long, String>();
		// String query = "";
		// query += "SELECT description_id, description_status, concept_id, ";
		// query += " term, initial_capital_status, description_type, ";
		// query += " language_code ";
		// query += " FROM description ";
		// // query += " WHERE description_type != 3 ";
		// // query += " AND description_status IN (0, 6, 11) ";
		// Statement stmt = this.conn.createStatement();
		// ResultSet res = stmt.executeQuery(query);
		// while (res.next()) {
		// // long description_id = res.getLong(1);
		// int description_status = res.getInt(2);
		// long concept_id = res.getLong(3);
		// String term = res.getString(4);
		// // int initial_capital_status = res.getInt(5);
		// int description_type = res.getInt(6);
		// // String language_code = res.getString(7);
		// // Could do this in the query
		// if (!Snomed.isStatusActive(description_status))
		// continue;
		// if (Snomed.isDescriptionTypeFullySpecified(description_type))
		// continue;
		// String[] words = term.split(WORD_SPLIT);
		// for (String word : words) {
		// word = word.toLowerCase();
		// this.words.add(word);
		// }
		// if (!this.terms.containsKey(term))
		// this.terms.put(term, new SearchResult(term, concept_id));
		// if (description_status == 0)
		// concept_ids.put(concept_id, term);
		// }
		// res.close();
		// stmt.close();
		// // words#: 84907
		// // terms#: 468419
		// System.out.println("words#: " + words.size());
		// System.out.println("terms#: " + terms.size());
	}

	public ArrayList<String> suggestWords(String sub_word, int limit) {
		ArrayList<String> result = new ArrayList<String>();
		sub_word = sub_word.toLowerCase();
		int i = 0;
		for (String word : this.words) {
			if (word.startsWith(sub_word)) {
				result.add(word);
				i++;
			}
			if (i == limit)
				break;
		}
		return result;
	}

	public ArrayList<SearchResult> suggestTerms(String sub_term, int limit) {
		return searchTerms(sub_term, 1, limit);
	}

	public ArrayList<SearchResult> searchTerms(String sub_term, int page, int page_size) {
		ArrayList<SearchResult> result = new ArrayList<SearchResult>();
		sub_term = sub_term.toLowerCase();
		try {
			long concept_id = Long.parseLong(sub_term.trim());
			String term = this.concept_ids.get(concept_id);
			if (term != null)
				result.add(this.terms.get(term));
		} catch (NumberFormatException ex) {
		}
		ArrayList<String> words = new ArrayList<String>(Arrays.asList(sub_term.split(WORD_SPLIT)));
		int i = 0;
		for (String term : this.terms.keySet()) {
			ArrayList<String> term_words = new ArrayList<String>(Arrays.asList(term.toLowerCase().split(WORD_SPLIT)));
			if (term_words.containsAll(words)) {
				i++;
				if (i > ((page - 1) * page_size))
					result.add(this.terms.get(term));
			}
			if (i == (page * page_size))
				break;
		}
		return result;
	}

}
