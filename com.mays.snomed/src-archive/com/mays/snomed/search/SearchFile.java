/**
 * Copyright 2008-2017 Mays Systems LLC.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *       http://www.apache.org/licenses/LICENSE-2.0
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package com.mays.snomed.search;

import java.io.BufferedReader;
import java.sql.Connection;
import java.util.Collection;
import java.util.logging.Logger;

import com.mays.snomed.ConceptBasic;
import com.mays.snomed.SnomedDb;
import com.mays.util.BasicLogger;
import com.mays.util.Config;
import com.mays.util.IO;
import com.mays.util.Sql;

@Deprecated
public class SearchFile implements SearchFileWriter {

	protected static Logger LOGGER;

	protected Config config;

	protected Connection conn;

	protected SnomedSearchLuceneImpl snomed_search;

	protected SnomedDb snomed_db;

	public SearchFile(String config_name) throws Exception {
		super();
		this.config = Config.load(config_name, LOGGER);
	}

	public void init() throws Exception {
		String derby_home = config.getProperty(Sql.DERBY_HOME_KEY, LOGGER);
		String schema = config.getProperty(Sql.SCHEMA_KEY, LOGGER);
		this.conn = Sql.getConnection(derby_home, schema);
		//
		SnomedSearchLuceneImpl.LOGGER = LOGGER;
		String lucene_home = config.getProperty(
				SnomedSearchLuceneImpl.LUCENE_HOME_KEY, LOGGER);
		this.snomed_search = new SnomedSearchLuceneImpl();
		this.snomed_search.init(lucene_home);
		//
		snomed_db = new SnomedDb();
		snomed_db.init(conn);
	}

	public void shutdown() throws Exception {
		Sql.shutdown(config.getProperty(Sql.SCHEMA_KEY, LOGGER));
	}

	public String getTerm(String line) {
		String[] field = line.split("\t");
		String term = field[0];
		term = term.trim();
		return term;
	}

	public Collection<Long> getIncludes(String line) {
		return SnomedSearchLuceneImpl.EMPTY_CONS;
	}

	public Collection<Long> getExcludes(String line) {
		return SnomedSearchLuceneImpl.EMPTY_CONS;
	}

	public String getTermLine(String term) {
		return "Term:\t" + term;
	}

	public String getHitLine(String description, ConceptBasic con) {
		return "\tHit:\t" + description + "\t\t\t\t\t"
				+ con.getFullySpecifiedName() + "\t\t\t\t\t"
				+ con.getConceptId();
	}

	private final boolean token_trace = false;

	private boolean exact_cutoff_p = false;

	public boolean getExactCutoff() {
		return exact_cutoff_p;
	}

	public void setExactCutoff(boolean exact_cutoff_p) {
		this.exact_cutoff_p = exact_cutoff_p;
	}

	public void searchFile(String file) throws Exception {
		BufferedReader in = IO.createBufferedUtf8FileReader(file);
		String line;
		int line_i = 0;
		while ((line = in.readLine()) != null) {
			line_i++;
			String term = getTerm(line);
			if (term.equals(""))
				continue;
			this.snomed_search.doSearch(term, snomed_db, getIncludes(line),
					getExcludes(line), token_trace, this, this.exact_cutoff_p);
		}
	}

	public void doSearch() throws Exception {
		this.searchFile("terms.txt");
	}

	public void run() throws Exception {
		this.init();
		this.doSearch();
		this.shutdown();
	}

	public static void main(String[] args) {
		try {
			SearchFile.LOGGER = BasicLogger.init(SearchFile.class);
			Sql.LOGGER = SearchFile.LOGGER;
			String config_name = Config.getConfigName(args, LOGGER);
			SearchFile m = new SearchFile(config_name);
			m.run();
		} catch (Exception ex) {
			BasicLogger.log(ex, LOGGER);
		} finally {
			BasicLogger.end(LOGGER);
		}
	}

}
