/**
 * Copyright 2008-2017 Mays Systems LLC.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *       http://www.apache.org/licenses/LICENSE-2.0
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package com.mays.snomed;

import java.sql.Connection;
import java.util.ArrayList;
import java.util.Collections;
import java.util.logging.Logger;

import org.junit.AfterClass;
import org.junit.BeforeClass;
import org.junit.Test;

import com.mays.snomed.ConceptBasic;
import com.mays.snomed.ConceptRelationship;
import com.mays.snomed.Snomed;
import com.mays.snomed.SnomedDb;
import com.mays.snomed.test.SnomedLoaderTest;
import com.mays.util.BasicLogger;
import com.mays.util.Config;
import com.mays.util.Sql;

public class StatedInferredTest {

	protected static Logger LOGGER;

	protected static Config CONFIG;

	protected static SnomedDb SNOMED_DB;

	@BeforeClass
	public static void setUpBeforeClass() throws Exception {
		LOGGER = BasicLogger.init(StatedInferredTest.class, false, true);
		Sql.LOGGER = LOGGER;
		CONFIG = Config.load(SnomedLoaderTest.CONFIG_FILE_PATH, LOGGER);
		Connection conn = Sql.getConnection(CONFIG);
		SNOMED_DB = new SnomedDb();
		SNOMED_DB.init(conn);
	}

	@AfterClass
	public static void tearDownAfterClass() throws Exception {
	}

	@Test
	public void doCompare() throws Exception {
		ArrayList<ConceptBasic> cons = SNOMED_DB.getConcepts();
		Collections.sort(cons);
		int i = 0;
		for (ConceptBasic con : cons) {
			if ((++i % 1000) == 0)
				LOGGER.fine("Progress: " + (i * 100 / cons.size()));
			if (!Snomed.isStatusActive(con.getActive()))
				continue;
			// if
			// (!con.getFullySpecifiedName().contains("Finding related to ability to"))
			// continue;
			ArrayList<ConceptRelationship> srels = SNOMED_DB
					.getStatedRelationships(con.getConceptId());
			Collections.sort(srels);
			ArrayList<ConceptRelationship> rels = SNOMED_DB
					.getRelationships(con.getConceptId());
			Collections.sort(rels);
			boolean diff_p = false;
			for (ConceptRelationship r : srels) {
				if (!rels.contains(r)) {
					if (!diff_p)
						LOGGER.info(con.getConceptId() + " "
								+ con.getFullySpecifiedName());
					diff_p = true;
					LOGGER.info("\tS:\t" + r.getRelationshipName() + " "
							+ r.getConceptId2() + " " + r.getConceptName2());
				}
			}
			for (ConceptRelationship r : rels) {
				if (!srels.contains(r)) {
					if (!diff_p)
						LOGGER.info(con.getConceptId() + " "
								+ con.getFullySpecifiedName());
					diff_p = true;
					LOGGER.info("\tI:\t" + r.getRelationshipName() + " "
							+ r.getConceptId2() + " " + r.getConceptName2());
				}
			}
		}
	}

}
