/**
 * Copyright 2008-2017 Mays Systems LLC.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *       http://www.apache.org/licenses/LICENSE-2.0
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package com.mays.snomed;

import org.junit.Test;

import com.mays.snomed.StatedLoader;
import com.mays.snomed.test.SnomedLoaderTest;
import com.mays.util.BasicLogger;
import com.mays.util.Config;
import com.mays.util.Sql;

public class StatedLoaderTest {

	@Test
	public void doLoad() throws Exception {
		StatedLoader.LOGGER = BasicLogger.init(StatedLoader.class, true, true);
		Sql.LOGGER = StatedLoader.LOGGER;
		String config_name = Config.getConfigName(
				new String[] { SnomedLoaderTest.CONFIG_FILE_PATH },
				StatedLoader.LOGGER);
		StatedLoader m = new StatedLoader(config_name);
		m.run();
	}

}
