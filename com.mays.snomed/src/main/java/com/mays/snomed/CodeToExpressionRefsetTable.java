/**
 * Copyright 2012-2017 Mays Systems LLC.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *       http://www.apache.org/licenses/LICENSE-2.0
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package com.mays.snomed;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.util.ArrayList;

/**
 * @see <a href=
 *      "https://confluence.ihtsdotools.org/display/DOCRELFMT/4.2.15.+Code+to+Expression+Reference+Set">
 */
public class CodeToExpressionRefsetTable extends SimpleRefsetTable {

	PreparedStatement stmt_map_target;

	PreparedStatement stmt_map;

	public CodeToExpressionRefsetTable(Connection conn) {
		super(conn);
	}

	public CodeToExpressionRefsetRow getByMapTarget(String code) throws Exception {
		if (stmt_map_target == null) {
			String query = "";
			query += "SELECT * ";
			query += "  FROM " + getTableName();
			query += " WHERE " + new CodeToExpressionRefsetRow().getMapTargetColumn() + " = ? ";
			stmt_map_target = conn.prepareStatement(query);
		}
		CodeToExpressionRefsetRow ret = null;
		stmt_map_target.setString(1, code);
		ResultSet res = stmt_map_target.executeQuery();
		if (res.next()) {
			ret = new CodeToExpressionRefsetRow(res);
		}
		res.close();
		return ret;
	}

	public ArrayList<CodeToExpressionRefsetRow> getMembers() throws Exception {
		if (stmt_map == null) {
			String query = "";
			query += "SELECT * ";
			query += "  FROM " + getTableName();
			stmt_map = conn.prepareStatement(query);
		}
		ArrayList<CodeToExpressionRefsetRow> ret = new ArrayList<>();
		ResultSet res = stmt_map.executeQuery();
		while (res.next()) {
			ret.add(new CodeToExpressionRefsetRow(res));
		}
		res.close();
		return ret;
	}

}
