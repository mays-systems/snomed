/**
 * Copyright 2008-2017 Mays Systems LLC.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *       http://www.apache.org/licenses/LICENSE-2.0
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package com.mays.snomed;

import java.io.PrintWriter;
import java.sql.Connection;
import java.util.logging.Logger;

import com.mays.snomed.ConceptBasic;
import com.mays.snomed.SnomedIsa;
import com.mays.snomed.SnomedLoader;
import com.mays.snomed.TransitiveClosure;
import com.mays.util.BasicLogger;
import com.mays.util.Config;
import com.mays.util.IO;
import com.mays.util.Sql;

/**
 * @author emays
 * 
 */
public class TransitiveClosure {

	public static Logger LOGGER;

	private Config config;

	private Connection conn;

	public TransitiveClosure(String config_name) throws Exception {
		super();
		this.config = Config.load(config_name, LOGGER);
	}

	public void run() throws Exception {
		String out_file = config.getProperty("out_file", LOGGER);
		PrintWriter out = IO.createPrintFileWriter(out_file);
		conn = Sql.getConnection(config);
		SnomedIsa snomed_db = new SnomedIsa();
		snomed_db.init(conn);
		for (ConceptBasic con : snomed_db.getConcepts()) {
			for (long ancestor : snomed_db.getAncestors(con.getConceptId())) {
				out.println(con.getConceptId() + "\t" + ancestor);
			}
		}
		out.close();
	}

	/**
	 * Set up logging, call the constructor, and run
	 * 
	 * <p>
	 * Args: <br>
	 * config_file
	 */
	public static void main(String[] args) {
		try {
			TransitiveClosure.LOGGER = BasicLogger.init(SnomedLoader.class,
					true, true);
			Sql.LOGGER = SnomedLoader.LOGGER;
			String config_name = Config.getConfigName(args, LOGGER);
			TransitiveClosure m = new TransitiveClosure(config_name);
			m.run();
		} catch (Exception ex) {
			BasicLogger.log(ex, LOGGER);
		} finally {
			BasicLogger.end(LOGGER);
		}
	}

}
