/**
 * Copyright 2008-2017 Mays Systems LLC.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *       http://www.apache.org/licenses/LICENSE-2.0
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package com.mays.snomed.subset;

import java.io.BufferedReader;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.sql.Connection;
import java.util.ArrayList;
import java.util.List;
import java.util.logging.Logger;
import java.util.stream.Collectors;

import com.mays.snomed.ConceptBasic;
import com.mays.snomed.Snomed;
import com.mays.snomed.SnomedDb;
import com.mays.snomed.subset.SubsetDb;
import com.mays.snomed.subset.SubsetLoader;
import com.mays.util.BasicLogger;
import com.mays.util.Config;
import com.mays.util.IO;
import com.mays.util.Sql;
import com.mays.util.subset.Modifier;
import com.mays.util.subset.Operator;
import com.mays.util.subset.SubsetBasic;
import com.mays.util.subset.SubsetExpression;

/**
 * 
 * Load a subset
 * 
 * Need to provide a configuration file
 * 
 * @author Eric Mays
 * 
 */
public class SubsetBuilder {

	public static Logger LOGGER;

	private Config config;

	private Connection conn;

	public SubsetBuilder(String config_name) throws Exception {
		super();
		this.config = Config.load(config_name, LOGGER);
	}

	public SubsetBuilder(Config config_name) throws Exception {
		super();
		this.config = config_name;
	}

	public void init() throws Exception {
		String derby_home = config.getProperty(Sql.DERBY_HOME_KEY, LOGGER);
		String schema = config.getProperty(Sql.SCHEMA_KEY, LOGGER);
		this.init(Sql.getConnection(derby_home, schema));
	}

	public void init(Connection conn) throws Exception {
		this.conn = conn;
	}

	public void shutdown() throws Exception {
		Sql.shutdown(config.getProperty(Sql.SCHEMA_KEY, LOGGER));
	}

	public void load(String file, String name, boolean overwrite_p, String delim, int field_i, boolean skip_inactive_p,
			int active_field_i, String active_key) throws Exception {
		LOGGER.info("Loading subset " + name);
		LOGGER.info("From file " + file);
		LOGGER.info("Delim: " + delim);
		LOGGER.info("Field: " + field_i);
		SnomedDb snomed_db = new SnomedDb();
		snomed_db.init(conn);
		SubsetDb subset_db = new SubsetDb(conn);
		subset_db.init();
		SubsetBasic subset = subset_db.getSubsetByName(name);
		if (subset != null) {
			if (overwrite_p) {
				subset_db.deleteSubsetMembers(subset.getId());
				LOGGER.info("Deleted subset members");
			} else {
				throw new Exception("Subset exists");
			}
		} else {
			subset_db.createSubset(name);
			subset = subset_db.getSubsetByName(name);
			LOGGER.info("Created subset " + subset.getName() + " " + subset.getId());
		}
		BufferedReader in = IO.createBufferedFileReader(file);
		String line;
		int line_i = 0;
		ArrayList<SubsetExpression> exprs = new ArrayList<SubsetExpression>();
		int not_found = 0;
		int skipped = 0;
		int not_active = 0;
		while ((line = in.readLine()) != null) {
			line_i++;
			if (line_i == 1)
				continue;
			String[] field = line.split(delim, -1);
			long sctid = Long.parseLong(field[field_i]);
			ConceptBasic con = snomed_db.getConcept(sctid);
			if (con == null) {
				LOGGER.warning("Concept not found " + sctid);
				not_found++;
				continue;
			}
			if (active_field_i != -1 && !active_key.equalsIgnoreCase(field[active_field_i])) {
				not_active++;
				continue;
			}
			if (!Snomed.isStatusActive(con.getActive())) {
				// LOGGER.warning("Concept not active " + sctid);
				if (skip_inactive_p) {
					// LOGGER.warning("Skipping " + sctid);
					skipped++;
					continue;
				}
			}
			exprs.add(new SubsetExpression(Operator.INCLUDE, Modifier.ONLY, sctid));
			if (line_i % 10000 == 0) {
				LOGGER.fine("Line: " + line_i);
			}
		}
		LOGGER.info("Processed lines: " + line_i);
		LOGGER.info("Not found: " + not_found);
		LOGGER.info("Not active: " + not_active);
		LOGGER.info("Skipped: " + skipped);
		subset_db.saveSubsetExpression(subset.getId(), exprs);
		subset_db.buildSubset(subset.getId());
		LOGGER.info("Loaded subset");
	}

	public void load() throws Exception {
		String dir = config.getProperty("data_dir", LOGGER);
		SubsetLoader loader = new SubsetLoader(config);
		SubsetLoader.LOGGER = LOGGER;
		loader.init(conn);
		for (Path file : Files.list(Paths.get(dir)).collect(Collectors.toList())) {
			String file_name = file.toString();
			if (file_name.endsWith(("-def.txt")))
				continue;
			LOGGER.info(file_name);
			String subset_name = file_name.substring(file_name.lastIndexOf("/") + 1);
			subset_name = subset_name.replace(".txt", "");
			LOGGER.info(subset_name);
			loader.load(file_name, subset_name, true, "\t", 0, true, -1, "active");
		}
	}

	public void run() throws Exception {
		this.init();
		this.load();
		this.shutdown();
	}

	public static void main(String[] args) {
		try {
			SubsetBuilder.LOGGER = BasicLogger.init(SubsetBuilder.class, true, "target/logs/");
			Sql.LOGGER = SubsetBuilder.LOGGER;
			String config_name = Config.getConfigName(args, LOGGER);
			SubsetBuilder m = new SubsetBuilder(config_name);
			m.run();
		} catch (Exception ex) {
			BasicLogger.log(ex, LOGGER);
		} finally {
			BasicLogger.end(LOGGER);
		}
	}

}
