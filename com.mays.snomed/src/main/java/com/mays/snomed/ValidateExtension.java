/**
 * Copyright 2008-2017 Mays Systems LLC.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *       http://www.apache.org/licenses/LICENSE-2.0
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package com.mays.snomed;

import java.sql.Connection;
import java.util.logging.Logger;

import com.mays.snomed.ConceptBasic;
import com.mays.snomed.ConceptRelationship;
import com.mays.snomed.Snomed;
import com.mays.snomed.SnomedDb;
import com.mays.snomed.ValidateExtension;
import com.mays.util.BasicLogger;
import com.mays.util.Config;
import com.mays.util.Sql;

public class ValidateExtension {

	private static Logger LOGGER;

	private Config config;

	private Connection conn;

	private SnomedDb snomed_db;

	public ValidateExtension(String config_name) throws Exception {
		super();
		this.config = Config.load(config_name, LOGGER);
	}

	public void init() throws Exception {
		String derby_home = config.getProperty(Sql.DERBY_HOME_KEY, LOGGER);
		String schema = config.getProperty(Sql.SCHEMA_KEY, LOGGER);
		this.init(Sql.getConnection(derby_home, schema));
	}

	public void init(Connection conn) throws Exception {
		this.conn = conn;
	}

	public void shutdown() throws Exception {
		String schema = config.getProperty(Sql.SCHEMA_KEY, LOGGER);
		Sql.shutdown(schema);
	}

	public void run() throws Exception {
		this.init();
		snomed_db = new SnomedDb();
		snomed_db.init(conn);
		int con_cnt = 0;
		int ext_cnt = 0;
		for (ConceptBasic con : snomed_db.getConcepts()) {
			con_cnt++;
			if (!Snomed.isExtensionId(con.getConceptId()))
				continue;
			ext_cnt++;
			for (ConceptRelationship rel : snomed_db.getRelationships(con.getConceptId())) {
				ConceptBasic c2 = snomed_db.getConcept(rel.getConceptId2());
				if (!c2.getActive())
					LOGGER.severe("Inactive: " + con.getConceptId() + " " + con.getFullySpecifiedName() + " "
							+ rel.getRelationshipName() + " " + rel.getConceptName2());
			}
			if (snomed_db.getParents(con.getConceptId()).size() == 0)
				LOGGER.severe("No parents: " + con.getConceptId() + " " + con.getFullySpecifiedName());
		}
		this.shutdown();
	}

	public static void main(String[] args) {
		try {
			ValidateExtension.LOGGER = BasicLogger.init(ValidateExtension.class, false, true);
			Sql.LOGGER = ValidateExtension.LOGGER;
			String config_name = Config.getConfigName(args, LOGGER);
			ValidateExtension m = new ValidateExtension(config_name);
			m.run();
		} catch (Exception ex) {
			BasicLogger.log(ex, LOGGER);
		} finally {
			BasicLogger.end(LOGGER);
		}
	}

}
