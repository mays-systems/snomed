/**
 * Copyright 2007-2017 Mays Systems LLC.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *       http://www.apache.org/licenses/LICENSE-2.0
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package com.mays.snomed;

import java.util.UUID;

import com.mays.snomed.ConceptBasic;

/**
 * A basic SNOMED CT concept object. Just the items in the concept table.
 * 
 * @author Eric Mays
 */
public class ConceptBasic implements Comparable<ConceptBasic> {

	private long concept_id;

	private String effective_time;

	private boolean active;

	private long module_id;

	private String ctv3_id;

	private String snomed_id;

	private boolean is_primitive;

	private String fully_specified_name;

	private UUID uuid;

	public ConceptBasic(long concept_id, String effective_time, boolean active, long module_id, boolean is_primitive,
			String fully_specified_name, UUID uuid) {
		super();
		this.concept_id = concept_id;
		this.effective_time = effective_time;
		this.active = active;
		this.module_id = module_id;
		this.ctv3_id = "";
		this.snomed_id = "";
		this.is_primitive = is_primitive;
		this.fully_specified_name = fully_specified_name;
		this.uuid = uuid;
	}

	public long getConceptId() {
		return concept_id;
	}

	public void setConceptId(long concept_id) {
		this.concept_id = concept_id;
	}

	public String getEffectiveTime() {
		return this.effective_time;
	}

	public boolean getActive() {
		return active;
	}

	public void setActive(boolean active) {
		this.active = active;
	}

	public long getModuleId() {
		return module_id;
	}

	public void setModuleId(long module_id) {
		this.module_id = module_id;
	}

	@Deprecated
	public String getCtv3Id() {
		return ctv3_id;
	}

	@Deprecated
	public void setCtv3Id(String ctv3_id) {
		this.ctv3_id = ctv3_id;
	}

	public String getFullySpecifiedName() {
		return fully_specified_name;
	}

	public void setFullySpecifiedName(String fully_specified_name) {
		this.fully_specified_name = fully_specified_name;
	}

	public boolean getIsPrimitive() {
		return is_primitive;
	}

	public void setIsPrimitive(boolean is_primitive) {
		this.is_primitive = is_primitive;
	}

	@Deprecated
	public String getSnomedId() {
		return snomed_id;
	}

	@Deprecated
	public void setSnomedId(String snomed_id) {
		this.snomed_id = snomed_id;
	}

	public UUID getUuid() {
		return uuid;
	}

	public void setUuid(UUID uuid) {
		this.uuid = uuid;
	}

	/**
	 * Comparison based on the FSN
	 */
	public int compareTo(ConceptBasic that) {
		return this.getFullySpecifiedName().compareToIgnoreCase(that.getFullySpecifiedName());
	}

	public String toString() {
		return this.getFullySpecifiedName();
	}

	public String getLogString() {
		return (this.getConceptId() + "\t" + this.getActive() + "\t" + this.getFullySpecifiedName());
	}

}
