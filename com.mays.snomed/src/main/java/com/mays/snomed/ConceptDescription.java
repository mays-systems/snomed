/**
 * Copyright 2007-2017 Mays Systems LLC.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *       http://www.apache.org/licenses/LICENSE-2.0
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package com.mays.snomed;

import java.util.Comparator;

import com.mays.snomed.ConceptDescription;
import com.mays.snomed.Snomed;
import com.mays.snomed.Snomed.CaseSensitive;
import com.mays.snomed.Snomed.DescriptionType;

/**
 * Just the items in the description table.
 * 
 * @author Eric Mays
 */
public class ConceptDescription implements Comparable<ConceptDescription> {

	long description_id;

	String effective_time;

	boolean active;

	long concept_id;

	String language_code;

	DescriptionType description_type;

	String term;

	CaseSensitive initial_capital_status;

	public ConceptDescription(long description_id, String effective_time, boolean active, long concept_id,
			String language_code, long description_type, String term, long case_significance_id) {
		super();
		this.description_id = description_id;
		this.effective_time = effective_time;
		this.active = active;
		this.concept_id = concept_id;
		this.language_code = language_code;
		this.description_type = Snomed.DescriptionType.getDescriptionType(description_type);
		this.term = term;
		this.initial_capital_status = Snomed.CaseSensitive.getCaseSensitive(case_significance_id);
	}

	public long getConceptId() {
		return concept_id;
	}

	public void setConceptId(long concept_id) {
		this.concept_id = concept_id;
	}

	public long getDescriptionId() {
		return description_id;
	}

	public void setDescriptionId(long description_id) {
		this.description_id = description_id;
	}

	public String getEffectiveTime() {
		return this.effective_time;
	}

	public boolean getActive() {
		return active;
	}

	public void setActive(boolean description_status) {
		this.active = description_status;
	}

	public DescriptionType getDescriptionType() {
		return description_type;
	}

	public void setDescriptionType(DescriptionType description_type) {
		this.description_type = description_type;
	}

	public CaseSensitive getInitialCapitalStatus() {
		return initial_capital_status;
	}

	public void setInitialCapitalStatus(CaseSensitive initial_capital_status) {
		this.initial_capital_status = initial_capital_status;
	}

	public String getLanguageCode() {
		return language_code;
	}

	public void setLanguageCode(String language_code) {
		this.language_code = language_code;
	}

	public String getTerm() {
		return term;
	}

	public void setTerm(String term) {
		this.term = term;
	}

	public static class TypeComparator implements Comparator<ConceptDescription> {

		@Override
		public int compare(ConceptDescription d1, ConceptDescription d2) {
			if (d1.getDescriptionType() != d2.getDescriptionType()) {
				return d1.getDescriptionType().compareTo(d2.getDescriptionType());
			}
			return d1.compareTo(d2);
		}

	}

	/**
	 * Compare the terms
	 */
	public int compareTo(ConceptDescription that) {
		return this.getTerm().compareTo(that.getTerm());
	}

	public String getLogString() {
		return (this.getConceptId() + "\t" + this.getDescriptionId() + "\t" + this.getActive() + "\t"
				+ this.getDescriptionType() + "\t" + this.getTerm());
	}

}
