/**
 * Copyright 2008-2017 Mays Systems LLC.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *       http://www.apache.org/licenses/LICENSE-2.0
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package com.mays.snomed.owl;

import java.io.PrintWriter;
import java.sql.Connection;
import java.util.Arrays;
import java.util.Date;
import java.util.List;
import java.util.UUID;
import java.util.logging.Logger;

import com.mays.snomed.ConceptBasic;
import com.mays.snomed.ConceptDescription;
import com.mays.snomed.SnomedDb;
import com.mays.snomed.subset.SubsetDb;
import com.mays.util.Xml;
import com.mays.util.subset.SubsetBasic;

public class OwlWriter {

	public static Logger LOGGER;

	private Connection conn;

	private PrintWriter out;

	SnomedDb snomed_db;

	SubsetDb subset_db;

	// MapDb map_db;
	//
	// CptDtkDb cptdtk_db;

	public OwlWriter(Connection conn, PrintWriter out) throws Exception {
		super();
		this.conn = conn;
		this.out = out;
	}

	public void writeOntology(String url, String name, UUID subset_id, List<UUID> subsets) throws Exception {
		this.snomed_db = new SnomedDb();
		snomed_db.init(conn);
		this.subset_db = new SubsetDb(conn);
		subset_db.init();
		// this.map_db = new MapDb();
		// map_db.init(conn);
		// this.cptdtk_db = new CptDtkDb(conn);
		String parenthetical = "";
		parenthetical = " (" + subset_id + " - " + subset_db.getSubset(subset_id).getName() + ")";
		out.println(Xml.getDeclaration("UTF-8"));
		out.println(Xml.startElement("rdf:RDF",
				new String[] { "xmlns", url + "#", "xmlns:rdf", "http://www.w3.org/1999/02/22-rdf-syntax-ns#",
						"xmlns:xsd", "http://www.w3.org/2001/XMLSchema#", "xmlns:rdfs",
						"http://www.w3.org/2000/01/rdf-schema#", "xmlns:owl", "http://www.w3.org/2002/07/owl#",
						"xml:base", url }));
		out.println();
		// <owl:Ontology rdf:about="">
		// <rdfs:comment>
		// SNODENT
		// </rdfs:comment>
		// <owl:versionInfo>200803</owl:versionInfo>
		// </owl:Ontology>
		out.println(Xml.startElement("owl:Ontology", new String[] { "rdf:about", "" }));
		out.println(Xml.valueElement("rdfs:comment", name + parenthetical));
		out.println(Xml.valueElement("owl:versionInfo", (new Date()).toString()));
		out.println(Xml.endElement("owl:Ontology"));
		out.println();
		this.writeProperties();
		this.writeConcepts(subset_id, subsets);
		out.println(Xml.endElement("rdf:RDF"));
		out.flush();
	}

	public String cleanName(String name) {
		name = name.replaceAll("\\W", "_");
		if (name.matches("^\\d.*")) {
			name = "_" + name;
		}
		return name;
	}

	private void writeConcepts(UUID subset_id, List<UUID> subsets) throws Exception {
		for (UUID subset : subsets) {
			SubsetBasic s = this.subset_db.getSubset(subset);
			out.println(Xml.startElement("Subset", new String[] { "rdf:ID", "Subset" + subset }));
			out.println(Xml.valueElement("rdfs:label", s.getName()));
			out.println(Xml.endElement("Subset"));
		}
		int i = 0;
		for (long concept_id : this.subset_db.getSubsetMembers(subset_id)) {
			ConceptBasic con = this.snomed_db.getConcept(concept_id);
			out.println(
					Xml.startElement("owl:Class", new String[] { "rdf:ID", cleanName(con.getFullySpecifiedName()) }));
			out.println(Xml.valueElement("SCT_ID", con.getConceptId()));
			out.println(Xml.valueElement("Status", (con.getActive() ? "Active" : "Inactive")));
			out.println(Xml.valueElement("Fully_Specified_Name", con.getFullySpecifiedName()));
			out.println(Xml.valueElement("SNOMED_ID", con.getSnomedId()));
			out.println(Xml.valueElement("rdfs:label", con.getFullySpecifiedName()));
			for (long parent_id : this.subset_db.getParents(subset_id, concept_id)) {
				if (parent_id == 0)
					continue;
				ConceptBasic parent = this.snomed_db.getConcept(parent_id);
				// <rdfs:subClassOf rdf:resource="#Chemotherapy"/>
				out.println(Xml.emptyElement("rdfs:subClassOf",
						new String[] { "rdf:resource", "#" + cleanName(parent.getFullySpecifiedName()) }));
			}
			for (ConceptDescription descr : this.snomed_db.getDescriptions(concept_id)) {
				out.println(Xml.startElement("hasDescr"));
				out.println(
						Xml.startElement("Descr", new String[] { "rdf:ID", String.valueOf(descr.getDescriptionId()) }));
				out.println(Xml.valueElement("SCT_ID", descr.getDescriptionId()));
				out.println(Xml.valueElement("Status", (descr.getActive() ? "Active" : "Inactive")));
				out.println(Xml.valueElement("Term", descr.getTerm()));
				out.println(Xml.valueElement("DescrType", descr.getDescriptionType().name));
				out.println(Xml.valueElement("InitialCapitalStatus", descr.getInitialCapitalStatus().name));
				out.println(Xml.valueElement("LanguageCode", descr.getLanguageCode()));
				out.println(Xml.valueElement("rdfs:label", descr.getTerm()));
				out.println(Xml.endElement("Descr"));
				out.println(Xml.endElement("hasDescr"));
			}
			for (UUID subset : subsets) {
				if (this.subset_db.isSubsetMember(subset, concept_id)) {
					out.println(Xml.emptyElement("memberOf", new String[] { "rdf:resource", "#" + "Subset" + subset }));
				}
			}
			// /////////////////
			// for (SnomedMap map : map_db.getMap(con.getConceptId())) {
			// CptDtkConcept code_con = cptdtk_db.getDtkConceptByCode(map
			// .getTargetCodes());
			// String code_descr = "";
			// if (code_con != null)
			// code_descr = code_con.getDescriptor();
			// String code_str = "(" + map.getTargetCodes() + ") "
			// + code_descr;
			// out.println(Xml.startElement("hasMap"));
			// out.println(Xml.startElement("Map", new String[] { "rdf:ID",
			// String.valueOf(map.getMapId()) }));
			// out.println(Xml.valueElement("Group", map.getMapGroup()));
			// out.println(Xml.valueElement("Option", map.getMapOption()));
			// out.println(Xml.valueElement("Priority", map.getMapPriority()));
			// out.println(Xml.valueElement("Category", map.getMapCategory()));
			// out.println(Xml.valueElement("Code", code_str));
			// out.println(Xml.valueElement("Advice", map.getMapAdvice()));
			// out.println(Xml.valueElement("rdfs:label", "G-"
			// + map.getMapGroup() + ", O-" + map.getMapOption()
			// + ", P-" + map.getMapPriority() + ", C-"
			// + map.getMapCategory() + ", Code-"
			// + code_str + ", Advice-"
			// + map.getMapAdvice()));
			// out.println(Xml.endElement("Map"));
			// out.println(Xml.endElement("hasMap"));
			// }
			// /////////////////////
			out.println(Xml.endElement("owl:Class"));
			out.println();
			if (++i % 100 == 0) {
				LOGGER.fine("Concept: " + i);
			}
		}
	}

	public static String asDatatypeProperty(String name) {
		String str = "";
		str += Xml.startElement("owl:DatatypeProperty", new String[] { "rdf:ID", name });
		// <rdfs:range rdf:resource="http://www.w3.org/2001/XMLSchema#string"/>
		// <rdf:type
		// rdf:resource="http://www.w3.org/2002/07/owl#AnnotationProperty"/>
		str += Xml.emptyElement("rdfs:range",
				new String[] { "rdf:resource", "http://www.w3.org/2001/XMLSchema#string" });
		str += Xml.emptyElement("rdf:type",
				new String[] { "rdf:resource", "http://www.w3.org/2002/07/owl#AnnotationProperty" });
		str += Xml.endElement("owl:DatatypeProperty");
		return str;
	}

	private void writeProperties() {
		out.println(asDatatypeProperty("SCT_ID"));
		out.println();
		out.println(asDatatypeProperty("Fully_Specified_Name"));
		out.println();
		out.println(asDatatypeProperty("SNOMED_ID"));
		out.println();
		out.println(asDatatypeProperty("Status"));
		out.println();
		out.println(asDatatypeProperty("Descr"));
		out.println();
		// why not subset
		out.println(asDatatypeProperty("Map"));
		out.println();

		out.println(Xml.startElement("owl:Class", new String[] { "rdf:ID", "Descr" }));
		out.println(Xml.endElement("owl:Class"));
		out.println();

		out.println(Xml.startElement("owl:Class", new String[] { "rdf:ID", "Subset" }));
		out.println(Xml.endElement("owl:Class"));
		out.println();

		out.println(Xml.startElement("owl:Class", new String[] { "rdf:ID", "Map" }));
		out.println(Xml.endElement("owl:Class"));
		out.println();

		for (String str : Arrays.asList("memberOf", "hasDescr", "Term", "DescrType", "InitialCapitalStatus",
				"LanguageCode", "hasMap", "Group", "Option", "Priority", "Category", "Code", "Advice")) {
			out.println(Xml.emptyElement("owl:AnnotationProperty", new String[] { "rdf:ID", str }));
			out.println();
		}
	}

}
