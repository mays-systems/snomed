/**
 * Copyright 2008-2017 Mays Systems LLC.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *       http://www.apache.org/licenses/LICENSE-2.0
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package com.mays.snomed.owl;

import java.io.PrintWriter;
import java.sql.Connection;
import java.util.ArrayList;
import java.util.UUID;
import java.util.logging.Logger;

import com.mays.util.subset.SubsetBasic;
import com.mays.snomed.subset.SubsetDb;
import com.mays.util.BasicLogger;
import com.mays.util.Config;
import com.mays.util.IO;
import com.mays.util.Sql;

public class OwlWriterMain {

	public static Logger LOGGER;

	protected Config config;

	protected Connection conn;

	public OwlWriterMain(String config_name) throws Exception {
		super();
		this.config = Config.load(config_name, LOGGER);
	}

	public void init() throws Exception {
		String derby_home = config.getProperty(Sql.DERBY_HOME_KEY, LOGGER);
		String schema = config.getProperty(Sql.SCHEMA_KEY, LOGGER);
		this.init(Sql.getConnection(derby_home, schema));
	}

	public void init(Connection conn) throws Exception {
		this.conn = conn;
	}

	public void shutdown() throws Exception {
		String schema = config.getProperty(Sql.SCHEMA_KEY, LOGGER);
		Sql.shutdown(schema);
	}

	public void run() throws Exception {
		this.init();
		SubsetDb subset_db = new SubsetDb(conn);
		subset_db.init();
		String subset_name = config.getProperty("subset_name", LOGGER);
		SubsetBasic subset = subset_db.getSubsetByName(subset_name);
		PrintWriter out = IO.createPrintUtf8FileWriter(subset.getName() + "-"
				+ subset.getId() + ".owl");
		OwlWriter ow = new OwlWriter(conn, out);
		ow.writeOntology("http://snomed.org/snomed", subset.getName(),
				subset.getId(), new ArrayList<UUID>());
		out.close();
		this.shutdown();
	}

	public static void main(String[] args) {
		try {
			OwlWriterMain.LOGGER = BasicLogger.init(OwlWriterMain.class);
			Sql.LOGGER = OwlWriterMain.LOGGER;
			OwlWriter.LOGGER = OwlWriterMain.LOGGER;
			String config_name = Config.getConfigName(args, LOGGER);
			OwlWriterMain m = new OwlWriterMain(config_name);
			m.run();
		} catch (Exception ex) {
			BasicLogger.log(ex, LOGGER);
		} finally {
			BasicLogger.end(LOGGER);
		}
	}

}
