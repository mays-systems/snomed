/**
 * Copyright 2008-2017 Mays Systems LLC.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *       http://www.apache.org/licenses/LICENSE-2.0
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package com.mays.snomed.search;

import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.Statement;
import java.util.HashMap;
import java.util.Set;
import java.util.logging.Logger;

import com.mays.snomed.DescriptionRow;
import com.mays.snomed.Snomed;
import com.mays.snomed.Snomed.CaseSensitive;
import com.mays.snomed.Snomed.DescriptionType;
import com.mays.util.spell.Spell;

public class SnomedWords {

	public static Logger LOGGER;

	private Connection conn;

	public SnomedWords(Connection conn) {
		super();
		this.conn = conn;
	}

	// public static String trimWord(String word) {
	// String orig = word;
	// word = word.replaceAll(",$", "");
	// word = word.replaceAll("\\.$", "");
	// word = word.replaceAll("^\\(", "");
	// word = word.replaceAll("\\)$", "");
	// word = word.replaceAll("^\"", "");
	// word = word.replaceAll("\"$", "");
	// word = word.replaceAll("^'", "");
	// word = word.replaceAll("'$", "");
	// if (orig.equals(word))
	// return word;
	// return trimWord(word);
	// }
	//
	// private String trimPoss(String word) {
	// word = word.replaceAll("'$", "");
	// word = word.replaceAll("'s$", "");
	// return word;
	// }

	private HashMap<String, Integer> word_counts = new HashMap<String, Integer>();

	private void addWord(String word) {
		Integer freq = word_counts.get(word);
		if (freq == null)
			freq = 0;
		freq++;
		word_counts.put(word, freq);
	}

	public Set<String> getWords() {
		return word_counts.keySet();
	}

	public HashMap<String, Integer> getWordFreqs() {
		return word_counts;
	}

	public Integer getWord(String word) {
		Integer ret;
		word = Spell.trimWord(word);
		ret = word_counts.get(word);
		if (ret != null)
			return ret;
		word = word.toLowerCase();
		ret = word_counts.get(word);
		if (ret != null)
			return ret;
		// word = trimPoss(word);
		// ret = word_counts.get(word);
		// if (ret != null)
		// return ret;
		if (word.contains("-")) {
			boolean found_all = true;
			for (String w : word.split("\\-")) {
				if (getWord(w) == null) {
					found_all = false;
					break;
				}
			}
			if (found_all)
				return 1;
		}
		return null;
	}

	public void init() throws Exception {
		String query = "";
		query += "SELECT " + DescriptionRow.COLUMNS;
		query += "  FROM description ";
		Statement stmt = this.conn.createStatement();
		ResultSet res = stmt.executeQuery(query);
		while (res.next()) {
			DescriptionRow dr = new DescriptionRow(res);
			if (Snomed.isExtensionId(dr.id))
				continue;
			if (dr.active == 0)
				continue;
			String term = dr.term;
			if (dr.type_id == DescriptionType.FULLY_SPECIFIED.id)
				term = Snomed.trimFsn(term);
			String[] words = term.split(Spell.NON_WORD_CHAR_CLASS);
			int i = 0;
			for (String word : words) {
				String trim_word = word;
				i++;
				if (i == 1 && dr.case_significance_id != CaseSensitive.CASE_INSENSITIVE_INITIAL.id)
					trim_word = trim_word.toLowerCase();
				trim_word = Spell.trimWord(trim_word);
				trim_word = trim_word.replaceAll("^\\-+", "");
				if (trim_word.length() > 1) {
					addWord(trim_word);
					if (LOGGER != null)
						LOGGER.info("SNOMED word: " + word + " -> " + dr.case_significance_id + " " + trim_word + " "
								+ dr.term);
				}
				// if (!word.equals(trimPoss(word)))
				// addWord(trimPoss(word));
			}
		}
	}
}
