/**
 * Copyright 2008-2017 Mays Systems LLC.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *       http://www.apache.org/licenses/LICENSE-2.0
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package com.mays.snomed.search;

import java.sql.ResultSet;
import java.sql.Statement;
import java.util.HashSet;
import java.util.List;

import com.mays.snomed.DescriptionRow;
import com.mays.snomed.Snomed;
import com.mays.snomed.SnomedIsa;
import com.mays.snomed.Snomed.DescriptionType;
import com.mays.snomed.search.SnomedSearchBuildLuceneIndex;
import com.mays.util.BasicLogger;
import com.mays.util.Config;
import com.mays.util.Sql;
import com.mays.util.search.BuildLuceneIndexBase;

public class SnomedSearchBuildLuceneIndex extends BuildLuceneIndexBase {

	private boolean log_once_p = false;

	private SnomedIsa snomed_isa;

	public SnomedSearchBuildLuceneIndex(String config_name) throws Exception {
		super(config_name);
	}

	public SnomedSearchBuildLuceneIndex(Config config) throws Exception {
		super(config);
	}

	public void init() throws Exception {
		super.init();
		snomed_isa = new SnomedIsa();
		snomed_isa.init(conn);
	}

	public void setIndexRoots() throws Exception {
		this.setIndexRoots(new HashSet<Long>(snomed_isa.getChildren(Snomed.ROOT_ID)));
		for (long root_id : getIndexRoots()) {
			LOGGER.info("" + root_id);
			LOGGER.info("Index root: " + snomed_isa.getConcept(root_id).getLogString());
		}
	}

	@Override
	protected boolean indexRootP(long root_id, long concept_id) throws Exception {
		return snomed_isa.isaSubsumesP(root_id, concept_id);
	}

	protected boolean conceptFilterP(DescriptionRow dr) throws Exception {
		return snomed_isa.getConcept(dr.concept_id).getActive();
	}

	protected boolean semanticFilterP(DescriptionRow dr, List<Long> concepts) throws Exception {
		if (!log_once_p) {
			for (long id : concepts) {
				LOGGER.info("Semantic filter on: " + snomed_isa.getConcept(id));
			}
			this.log_once_p = true;
		}
		for (long id : concepts) {
			if (snomed_isa.isaSubsumesP(id, dr.concept_id)) {
				return true;
			}
		}
		return false;
	}

	protected boolean descriptionFilterP(DescriptionRow dr) throws Exception {
		if (dr.active == 0)
			return false;
		if (dr.type_id == DescriptionType.FULLY_SPECIFIED.id)
			return false;
		return true;
	}

	protected void build() throws Exception {
		String query = "";
		query += "SELECT " + DescriptionRow.COLUMNS;
		query += "  FROM description ";
		Statement stmt = this.conn.createStatement();
		ResultSet res = stmt.executeQuery(query);
		int line_i = 0;
		while (res.next()) {
			line_i++;
			// Do this early because of the filters
			if (line_i % 10000 == 0) {
				LOGGER.fine("Line: " + line_i);
				progress_monitor.subTask("Search index descriptions " + line_i);
			}
			DescriptionRow dr = new DescriptionRow(res);
			if (!this.descriptionFilterP(dr))
				continue;
			if (!this.conceptFilterP(dr))
				continue;
			indexTerm(dr.term, dr.concept_id);
		}
		res.close();
		stmt.close();
	}

	public void shutdown() throws Exception {
		snomed_isa = null;
		super.shutdown();
	}

	public static void main(String[] args) {
		try {
			String log_dir = BasicLogger.getLogDir(args);
			if (log_dir != null) {
				SnomedSearchBuildLuceneIndex.LOGGER = BasicLogger.init(SnomedSearchBuildLuceneIndex.class, true,
						log_dir);
			} else {
				SnomedSearchBuildLuceneIndex.LOGGER = BasicLogger.init(SnomedSearchBuildLuceneIndex.class, true, true);
			}
			Sql.LOGGER = SnomedSearchBuildLuceneIndex.LOGGER;
			String config_name = Config.getConfigName(args, LOGGER);
			SnomedSearchBuildLuceneIndex m = new SnomedSearchBuildLuceneIndex(config_name);
			m.run();
		} catch (Exception ex) {
			BasicLogger.log(ex, LOGGER);
		} finally {
			BasicLogger.end(LOGGER);
		}
	}

}
