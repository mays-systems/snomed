/**
 * Copyright 2012-2017 Mays Systems LLC.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *       http://www.apache.org/licenses/LICENSE-2.0
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package com.mays.snomed;

import java.sql.ResultSet;

// https://confluence.ihtsdotools.org/display/DOCRELFMT/4.2.1.+Simple+Reference+Set
import java.util.ArrayList;
import java.util.List;
import java.util.UUID;

/**
 * @see <a href=
 *      "https://confluence.ihtsdotools.org/display/DOCRELFMT/4.2.1.+Simple+Reference+Set">
 */
public class SimpleRefsetRow {

	protected static final String UUID_TYPE = "CHAR(36)";

	protected int columnIndex = 1;

	// id effectiveTime active moduleId refsetId referencedComponentId

	public UUID id;

	public String effectiveTime;

	public int active;

	public long moduleId;

	public long refsetId;

	public long referencedComponentId;

	SimpleRefsetRow() {
	}

	public SimpleRefsetRow(ResultSet res) throws Exception {
		id = UUID.fromString(res.getString(columnIndex++));
		effectiveTime = res.getString(columnIndex++);
		active = res.getInt(columnIndex++);
		moduleId = res.getLong(columnIndex++);
		refsetId = res.getLong(columnIndex++);
		referencedComponentId = res.getLong(columnIndex++);
	}

	public String getKeyColumn() {
		return "id";
	}

	public List<String> getColumnDef() {
		ArrayList<String> ret = new ArrayList<>();
		ret.add("id " + SimpleRefsetRow.UUID_TYPE + " PRIMARY KEY");
		ret.add("effectiveTime " + "VARCHAR(8)");
		ret.add("active " + "INT");
		ret.add("moduleId " + "BIGINT");
		ret.add("refsetId " + "BIGINT");
		ret.add("referencedComponentId " + "BIGINT");

		return ret;
	}

	public List<String> getIndexColumns() {
		ArrayList<String> ret = new ArrayList<>();
		ret.add("refsetId");
		ret.add("referencedComponentId");
		return ret;
	}

}