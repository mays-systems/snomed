/**
 * Copyright 2007-2017 Mays Systems LLC.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *       http://www.apache.org/licenses/LICENSE-2.0
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package com.mays.snomed;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.Collection;
import java.util.HashMap;
import java.util.HashSet;
import java.util.UUID;

/**
 * Simple access to SNOMED. Load all concepts into memory. Then go to the db for
 * remainder.
 * 
 * <p>
 * To use this class, call the constructor, then init with a JDBC connection.
 * 
 * <p>
 * {@code Connection conn = Sql.getConnection(derby_home, schema); } <br>
 * {@code SnomedDb snomed_db = new SnomedDb(); } <br>
 * {@code snomed_db.init(conn); } <br>
 * 
 * @author Eric Mays
 */
public class SnomedDb {

	private static final int INITIAL_CONCEPT_COUNT = 350000;

	protected Connection conn;

	private HashMap<Long, ConceptBasic> concepts;

	private HashMap<UUID, ConceptBasic> concepts_uuid;

	private HashSet<Long> concepts_with_children;

	public Connection getConnection() {
		return this.conn;
	}

	/**
	 * Loads concepts into memory
	 * 
	 * @param conn
	 *            a JDBC connection
	 */
	public void init(Connection conn) throws Exception {
		long beg = System.currentTimeMillis();
		this.conn = conn;
		this.initConceptMap();
		System.out.println("initConcepts: " + ((System.currentTimeMillis() - beg) / 1000));
		System.out.println("concepts#: " + this.concepts.size());
		this.initConceptsWithChildren();
		System.out.println("initConceptsWithChildren: " + ((System.currentTimeMillis() - beg) / 1000));
		System.out.println("concepts_with_children#: " + this.concepts_with_children.size());
	}

	/**
	 * Loads concepts into memory
	 */
	protected void initConceptMap() throws Exception {
		this.concepts = new HashMap<Long, ConceptBasic>(SnomedDb.INITIAL_CONCEPT_COUNT / 4);
		this.concepts_uuid = new HashMap<UUID, ConceptBasic>(SnomedDb.INITIAL_CONCEPT_COUNT / 4);
		String query = "";
		query += "SELECT " + ConceptRow.COLUMNS;
		query += "  FROM concept ";
		Statement stmt = this.conn.createStatement();
		//
		// String query_fsn = "";
		// query_fsn += "SELECT " + DescriptionRow.COLUMNS;
		// query_fsn += " FROM description ";
		// query_fsn += " WHERE concept_id = ? ";
		// query_fsn += " AND active = 1 ";
		// query_fsn += " AND type_id = " + DescriptionType.FULLY_SPECIFIED.id;
		// PreparedStatement stmt_fsn = this.conn.prepareStatement(query_fsn);
		//
		ResultSet res = stmt.executeQuery(query);
		while (res.next()) {
			ConceptRow row = new ConceptRow(res);
			// String fully_specified_name = "error";
			// stmt_fsn.setLong(1, row.id);
			// ResultSet res_fsn = stmt_fsn.executeQuery();
			// while (res_fsn.next()) {
			// DescriptionRow row_fsn = new DescriptionRow(res_fsn);
			// if (row_fsn.active == 0)
			// continue;
			// if (row_fsn.type_id == DescriptionType.FULLY_SPECIFIED.id)
			// fully_specified_name = row_fsn.term;
			// }
			// res_fsn.close();
			ConceptBasic con = new ConceptBasic(row.id, row.effective_time, row.active == 1, row.module_id,
					row.definition_status_id == Snomed.DefinitionStatus.PRIMITIVE.id, row.fsn,
					UUID.fromString(row.uuid));
			this.concepts.put(con.getConceptId(), con);
			this.concepts_uuid.put(con.getUuid(), con);
		}
		// stmt_fsn.close();
		res.close();
		stmt.close();
	}

	/**
	 * Load up the cons with children
	 */
	protected void initConceptsWithChildren() throws Exception {
		this.concepts_with_children = new HashSet<Long>(SnomedDb.INITIAL_CONCEPT_COUNT);
		String query = "";
		query += "SELECT concept_id2 ";
		query += "  FROM isa ";
		Statement stmt = this.conn.createStatement();
		ResultSet res = stmt.executeQuery(query);
		while (res.next()) {
			long c2 = res.getLong(1);
			this.concepts_with_children.add(c2);
		}
		res.close();
		stmt.close();
	}

	public ConceptBasic getConcept(long con) {
		return this.concepts.get(con);
	}

	public ConceptBasic getConcept(UUID uuid) {
		return this.concepts_uuid.get(uuid);
	}

	public ConceptBasic getConceptByFullySpecifiedName(String fsn) throws Exception {
		ConceptBasic ret = null;
		String query = "";
		query += "SELECT id ";
		query += "  FROM concept ";
		query += " WHERE fsn = ? ";
		query += "   AND active = 1 ";
		PreparedStatement stmt = this.conn.prepareStatement(query);
		stmt.setString(1, fsn);
		ResultSet res = stmt.executeQuery();
		while (res.next()) {
			long concept_id = res.getLong(1);
			ret = this.getConcept(concept_id);
		}
		res.close();
		stmt.close();
		return ret;
	}

	/**
	 * @param con
	 *            an sctid
	 * 
	 * @return true iff the con has children
	 */
	public boolean hasChildren(long con) {
		return this.concepts_with_children.contains(con);
	}

	public ArrayList<ConceptDescription> getDescriptions(long con) throws Exception {
		return getDescriptions(con, false);
	}

	/**
	 * @param con
	 *            an sctid
	 * 
	 * @return descriptions
	 */
	public ArrayList<ConceptDescription> getDescriptions(long con, boolean active_only) throws Exception {
		ArrayList<ConceptDescription> result = new ArrayList<ConceptDescription>();
		String query = "";
		query += "SELECT " + DescriptionRow.COLUMNS;
		query += "  FROM description ";
		query += " WHERE concept_id = ? ";
		PreparedStatement stmt = this.conn.prepareStatement(query);
		stmt.setLong(1, con);
		ResultSet res = stmt.executeQuery();
		while (res.next()) {
			DescriptionRow row = new DescriptionRow(res);
			if (active_only && row.active == 0)
				continue;
			result.add(new ConceptDescription(row.id, row.effective_time, row.active == 1, row.concept_id,
					row.language_code, row.type_id, row.term, row.case_significance_id));
		}
		res.close();
		stmt.close();
		return result;
	}

	public ConceptDescription getDescription(long descr) throws Exception {
		ConceptDescription result = null;
		String query = "";
		query += "SELECT " + DescriptionRow.COLUMNS;
		query += "  FROM description ";
		query += " WHERE description_id = ? ";
		PreparedStatement stmt = this.conn.prepareStatement(query);
		stmt.setLong(1, descr);
		ResultSet res = stmt.executeQuery();
		while (res.next()) {
			DescriptionRow row = new DescriptionRow(res);
			result = new ConceptDescription(row.id, row.effective_time, row.active == 1, row.concept_id,
					row.language_code, row.type_id, row.term, row.case_significance_id);
		}
		res.close();
		stmt.close();
		return result;
	}

	/**
	 * @param con
	 *            an sctid
	 * 
	 * @return relationships
	 */
	public ArrayList<ConceptRelationship> getRelationships(long con) throws Exception {
		ArrayList<ConceptRelationship> result = new ArrayList<ConceptRelationship>();
		String query = "";
		query += "SELECT " + RelationshipRow.COLUMNS;
		query += "  FROM relationship ";
		query += " WHERE source_id = ? ";
		PreparedStatement stmt = this.conn.prepareStatement(query);
		stmt.setLong(1, con);
		ResultSet res = stmt.executeQuery();
		while (res.next()) {
			RelationshipRow row = new RelationshipRow(res);
			String relationship_type_fsn = this.getConcept(row.type_id).getFullySpecifiedName();
			String dest_fsn = this.getConcept(row.destination_id).getFullySpecifiedName();
			result.add(new ConceptRelationship(row.id, row.active == 1, row.type_id, relationship_type_fsn,
					row.destination_id, dest_fsn, row.characteristic_type_id, row.modifier_id, row.relationship_group));
		}
		res.close();
		stmt.close();
		return result;
	}

	public ArrayList<ConceptRelationship> getRelationships(long con, boolean active_only_p) throws Exception {
		ArrayList<ConceptRelationship> result = new ArrayList<ConceptRelationship>();
		getRelationships(con).stream().filter(r -> (active_only_p ? r.active : true)).forEach(r -> result.add(r));
		return result;
	}

	public ArrayList<ConceptRelationship> getRelationships(long con, long rel, boolean active_only_p) throws Exception {
		ArrayList<ConceptRelationship> result = new ArrayList<ConceptRelationship>();
		getRelationships(con, active_only_p).stream().filter(r -> r.relationship_type == rel)
				.forEach(r -> result.add(r));
		return result;
	}

	// TODO This is an ugly hack
	public ArrayList<ConceptRelationship> getRelationshipsTo(long con) throws Exception {
		ArrayList<ConceptRelationship> result = new ArrayList<ConceptRelationship>();
		String query = "";
		query += "SELECT " + RelationshipRow.COLUMNS;
		query += "  FROM relationship ";
		query += " WHERE destination_id = ? ";
		PreparedStatement stmt = this.conn.prepareStatement(query);
		stmt.setLong(1, con);
		ResultSet res = stmt.executeQuery();
		while (res.next()) {
			RelationshipRow row = new RelationshipRow(res);
			String relationship_type_fsn = this.getConcept(row.type_id).getFullySpecifiedName();
			String source_fsn = this.getConcept(row.source_id).getFullySpecifiedName();
			result.add(new ConceptRelationship(row.id, row.active == 1, row.type_id, relationship_type_fsn,
					row.source_id, source_fsn, row.characteristic_type_id, row.modifier_id, row.relationship_group));
		}
		res.close();
		stmt.close();
		return result;
	}

	public ArrayList<ConceptRelationship> getRelationshipsTo(long con, long rel) throws Exception {
		ArrayList<ConceptRelationship> result = new ArrayList<ConceptRelationship>();
		String query = "";
		query += "SELECT " + RelationshipRow.COLUMNS;
		query += "  FROM relationship ";
		query += " WHERE destination_id = ? ";
		query += "   AND type_id = ? ";
		PreparedStatement stmt = this.conn.prepareStatement(query);
		stmt.setLong(1, con);
		stmt.setLong(2, rel);
		ResultSet res = stmt.executeQuery();
		while (res.next()) {
			RelationshipRow row = new RelationshipRow(res);
			String relationship_type_fsn = this.getConcept(row.type_id).getFullySpecifiedName();
			String source_fsn = this.getConcept(row.source_id).getFullySpecifiedName();
			result.add(new ConceptRelationship(row.id, row.active == 1, row.type_id, relationship_type_fsn,
					row.source_id, source_fsn, row.characteristic_type_id, row.modifier_id, row.relationship_group));
		}
		res.close();
		stmt.close();
		return result;
	}

	public ArrayList<ConceptRelationship> getStatedRelationships(long con) throws Exception {
		ArrayList<ConceptRelationship> result = new ArrayList<ConceptRelationship>();
		String query = "";
		query += "SELECT " + RelationshipRow.COLUMNS;
		query += "  FROM stated_relationship ";
		query += " WHERE source_id = ? ";
		PreparedStatement stmt = this.conn.prepareStatement(query);
		stmt.setLong(1, con);
		ResultSet res = stmt.executeQuery();
		while (res.next()) {
			RelationshipRow row = new RelationshipRow(res);
			String relationship_type_fsn = this.getConcept(row.type_id).getFullySpecifiedName();
			String dest_fsn = this.getConcept(row.destination_id).getFullySpecifiedName();
			result.add(new ConceptRelationship(row.id, row.active == 1, row.type_id, relationship_type_fsn,
					row.destination_id, dest_fsn, row.characteristic_type_id, row.modifier_id, row.relationship_group));
		}
		res.close();
		stmt.close();
		return result;
	}

	/**
	 * @param con
	 *            an sctid
	 * 
	 * @return sctids of children
	 */
	public ArrayList<Long> getChildren(long con) throws Exception {
		ArrayList<Long> result = new ArrayList<Long>();
		String query = "";
		query += "SELECT concept_id1 ";
		query += "  FROM isa ";
		query += " WHERE concept_id2 = ? ";
		// query += " FROM relationship ";
		// query += " WHERE relationship_type = " + Snomed.ISA_ID + " ";
		// query += " AND concept_id2 = ? ";
		PreparedStatement stmt = this.conn.prepareStatement(query);
		stmt.setLong(1, con);
		ResultSet res = stmt.executeQuery();
		while (res.next()) {
			long concept_id1 = res.getLong(1);
			result.add(concept_id1);
		}
		res.close();
		stmt.close();
		return result;
	}

	/**
	 * @param con
	 *            an sctid
	 * 
	 * @return sctids of parents
	 */
	public ArrayList<Long> getParents(long con) throws Exception {
		ArrayList<Long> result = new ArrayList<Long>();
		String query = "";
		query += "SELECT concept_id2 ";
		query += "  FROM isa ";
		query += " WHERE concept_id1 = ? ";
		// query += " FROM relationship ";
		// query += " WHERE relationship_type = " + Snomed.ISA_ID + " ";
		// query += " AND concept_id2 = ? ";
		PreparedStatement stmt = this.conn.prepareStatement(query);
		stmt.setLong(1, con);
		ResultSet res = stmt.executeQuery();
		while (res.next()) {
			long concept_id1 = res.getLong(1);
			result.add(concept_id1);
		}
		res.close();
		stmt.close();
		return result;
	}

	/**
	 * @param con
	 *            an sctid
	 * 
	 * @return list of sibling sctids
	 * @throws Exception
	 */
	public ArrayList<Long> getSiblings(long con) throws Exception {
		ArrayList<Long> res = new ArrayList<Long>();
		for (long parent : this.getParents(con)) {
			for (long child : this.getChildren(parent)) {
				if (child == con)
					continue;
				if (res.contains(child))
					continue;
				res.add(child);
			}
		}
		return res;
	}

	/**
	 * Convenience method.
	 */
	public ArrayList<ConceptBasic> getConcepts(Collection<Long> concepts) {
		ArrayList<ConceptBasic> res = new ArrayList<ConceptBasic>(concepts.size());
		for (long con : concepts) {
			res.add(this.getConcept(con));
		}
		return res;
	}

	/**
	 * Get all concepts
	 * 
	 * @return - all the concepts
	 */
	public ArrayList<ConceptBasic> getConcepts() {
		ArrayList<ConceptBasic> res = new ArrayList<ConceptBasic>(this.concepts.values().size());
		for (ConceptBasic con : this.concepts.values()) {
			res.add(con);
		}
		return res;
	}

	public ComponentHistory getComponentHistory(long id) throws Exception {
		ComponentHistory result = new ComponentHistory(id);
		String query = "";
		query += "SELECT release_version, change_type, ";
		query += "       status, reason ";
		query += "  FROM component_history ";
		query += " WHERE component_id = ? ";
		PreparedStatement stmt = this.conn.prepareStatement(query);
		stmt.setLong(1, id);
		ResultSet res = stmt.executeQuery();
		while (res.next()) {
			int release_version = res.getInt(1);
			int change_type = res.getInt(2);
			int status = res.getInt(3);
			String reason = res.getString(4);
			result.addChange(String.valueOf(release_version), change_type, status, reason);
		}
		res.close();
		stmt.close();
		return result;
	}

	private void deleteConcept(long concept_id) throws Exception {
		deleteConceptHelper(concept_id, "concept", "concept_id");
		deleteConceptHelper(concept_id, "description", "concept_id");
		// As long as these are leaf nodes this is OK
		deleteConceptHelper(concept_id, "relationship", "concept_id1");
		deleteConceptHelper(concept_id, "isa", "concept_id1");
		this.concepts.remove(concept_id);
	}

	private void deleteConceptHelper(long concept_id, String table, String key) throws Exception {
		String query = "";
		query += "DELETE FROM " + table + " ";
		query += " WHERE " + key + " = ? ";
		PreparedStatement stmt = this.conn.prepareStatement(query);
		stmt.setLong(1, concept_id);
		stmt.executeUpdate();
	}

	// private void saveConcept(ConceptEdit con) throws Exception {
	// String query = "";
	// query += "INSERT INTO concept ";
	// query += " (concept_id, concept_status, fully_specified_name, ";
	// query += " ctv3_id, snomed_id, is_primitive) ";
	// query += " VALUES (?, ?, ?, ?, ?, ?) ";
	// PreparedStatement stmt = this.conn.prepareStatement(query);
	// stmt.setLong(1, con.getConceptId());
	// stmt.setInt(2, con.getActive());
	// stmt.setString(3, con.getFullySpecifiedName());
	// stmt.setString(4, con.getCtv3Id());
	// stmt.setString(5, con.getSnomedId());
	// stmt.setInt(6, (con.getIsPrimitive() ? 1 : 0));
	// stmt.executeUpdate();
	// this.concepts.put(con.getConceptId(), con);
	// }

	// private void saveRelationships(long concept_id, List<ConceptRelationship>
	// rels) throws Exception {
	// String query = "";
	// query += "INSERT INTO relationship ";
	// query += " (relationship_id, concept_id1, relationship_type, concept_id2,
	// ";
	// query += " characteristic_type, refinability, relationship_group) ";
	// query += " VALUES (?, ?, ?, ?, ?, ?, ?) ";
	// PreparedStatement stmt = this.conn.prepareStatement(query);
	// String query_isa = "";
	// query_isa += "INSERT INTO isa ";
	// query_isa += " (concept_id1, concept_id2) ";
	// query_isa += " VALUES (?, ?) ";
	// PreparedStatement stmt_isa = this.conn.prepareStatement(query_isa);
	// for (ConceptRelationship rel : rels) {
	// stmt.setLong(1, rel.getRelationshipId());
	// stmt.setLong(2, concept_id);
	// stmt.setLong(3, rel.getRelationshipType());
	// stmt.setLong(4, rel.getConceptId2());
	// stmt.setInt(5, rel.getCharacteristicType());
	// stmt.setInt(6, rel.getRefinability());
	// stmt.setInt(7, rel.getRelationshipGroup());
	// stmt.executeUpdate();
	// if (rel.getRelationshipType() == Snomed.ISA_ID) {
	// stmt_isa.setLong(1, concept_id);
	// stmt_isa.setLong(2, rel.getConceptId2());
	// stmt_isa.executeUpdate();
	// }
	// }
	// }

	// private void saveDescriptions(long concept_id, List<ConceptDescription>
	// descrs) throws Exception {
	// String query = "";
	// query += "INSERT INTO description ";
	// query += " (description_id, description_status, concept_id, ";
	// query += " term, initial_capital_status, description_type, ";
	// query += " language_code) ";
	// query += " VALUES (?, ?, ?, ?, ?, ?, ?) ";
	// PreparedStatement stmt = this.conn.prepareStatement(query);
	// for (ConceptDescription descr : descrs) {
	// stmt.setLong(1, descr.getDescriptionId());
	// stmt.setInt(2, descr.getDescriptionStatus());
	// stmt.setLong(3, concept_id);
	// stmt.setString(4, descr.getTerm());
	// stmt.setInt(5, (descr.getInitialCapitalStatus() ? 1 : 0));
	// stmt.setInt(6, descr.getDescriptionType());
	// stmt.setString(7, descr.getLanguageCode());
	// stmt.executeUpdate();
	// }
	// }

	// public void saveConcept(ConceptEdit con, boolean commit_p) throws
	// Exception {
	// deleteConcept(con.getConceptId());
	// saveConcept(con);
	// saveRelationships(con.getConceptId(), con.getRelationships());
	// saveDescriptions(con.getConceptId(), con.getDescriptions());
	// if (commit_p)
	// conn.commit();
	// }

	public void fetchConcept(ConceptEdit con) throws Exception {
		con.setDescriptions(this.getDescriptions(con.getConceptId()));
		con.setRelationships(this.getRelationships(con.getConceptId()));
	}

}
