package com.mays.snomed;

import java.util.ArrayList;

public class SnomedExpression {

	public static class Attribute {

		private ConceptBasic attribute;

		private ConceptBasic value;

		Attribute(ConceptBasic attribute, ConceptBasic value) {
			super();
			this.attribute = attribute;
			this.value = value;
		}

		public ConceptBasic getAttribute() {
			return attribute;
		}

		public ConceptBasic getValue() {
			return value;
		}

	}

	private String expr;

	private ArrayList<ConceptBasic> parents = new ArrayList<>();

	private ArrayList<Attribute> attributes = new ArrayList<>();

	public SnomedExpression(String expr) {
		super();
		this.expr = expr;
	}

	public void parse(SnomedDb db) {
		// System.out.println("E: " + expr);
		int i = expr.indexOf(":");
		String p_str = expr.substring(0, i);
		for (String str : p_str.split(",")) {
			// System.out.println("P: " + p_str);
			// System.out.println(" " + str);
			parents.add(db.getConcept(Long.parseLong(str)));
		}
		String a_str = expr.substring(i + 1);
		for (String str : a_str.split(",")) {
			// System.out.println("A: " + a_str);
			// System.out.println(" " + str);
			// System.out.println("= " + str.split("=")[0]);
			// System.out.println("= " + str.split("=")[1]);
			attributes.add(new Attribute(db.getConcept(Long.parseLong(str.split("=")[0])),
					db.getConcept(Long.parseLong(str.split("=")[1]))));
		}
	}

	public ArrayList<ConceptBasic> getParents() {
		return parents;
	}

	public ArrayList<Attribute> getAttributes() {
		return attributes;
	}

}
