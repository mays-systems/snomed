/**
 * Copyright 2008-2017 Mays Systems LLC.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *       http://www.apache.org/licenses/LICENSE-2.0
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package com.mays.snomed;

import java.util.TreeSet;

public class ComponentHistory {

	public class Change implements Comparable<Change> {

		private String release_version;

		private int change_type;

		// 0 - Added. The component was added to SNOMED CT in this Release
		// Version.

		// 1 - Status Change. The status of the component has changed since the
		// last Release Version.

		// 2 - Minor Change. A minor change has been made to this component
		// since the last Release Version. All other changes require a component
		// to be inactivated and replaced with a new component with a different
		// SCTID.

		private int status;

		private String reason;

		public Change(String release_version, int change_type, int status,
				String reason) {
			this.release_version = release_version;
			this.change_type = change_type;
			this.status = status;
			this.reason = reason;
		}

		public String getReleaseVersion() {
			return release_version;
		}

		public int getChangeType() {
			return change_type;
		}

		public String toString() {
			String ret = "";
			ret += release_version + " ";
			switch (change_type) {
			case 0:
				ret += "Added";
				break;
			case 1:
				ret += "Status Change";
				break;
			case 2:
				ret += "Minor Change";
				break;
			default:
				ret += "<unknown>";
			}
			ret += " " + reason;
			return ret;
		}

		public int getStatus() {
			return status;
		}

		public String getReason() {
			return reason;
		}

		@Override
		public int compareTo(Change that) {
			return this.release_version.compareTo(that.release_version);
		}

	}

	private long component_id;

	private TreeSet<Change> changes = new TreeSet<Change>();

	public ComponentHistory(long component_id) {
		super();
		this.component_id = component_id;
	}

	public long getComponentId() {
		return component_id;
	}

	public TreeSet<Change> getChanges() {
		return changes;
	}

	public void addChange(String release_version, int change_type, int status,
			String reason) {
		changes.add(new Change(release_version, change_type, status, reason));
	}

	public Change getLastMajorChange() {
		Change ret = null;
		for (Change c : changes) {
			if (c.change_type < 2)
				ret = c;
		}
		return ret;
	}

}
