/**
 * Copyright 2009-2017 Mays Systems LLC.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *       http://www.apache.org/licenses/LICENSE-2.0
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package com.mays.snomed;

import java.util.ArrayList;

import com.mays.snomed.ConceptBasic;
import com.mays.snomed.ConceptDescription;
import com.mays.snomed.ConceptRelationship;

/**
 * @author emays
 * 
 */
public class ConceptEdit extends ConceptBasic {

	private ArrayList<ConceptDescription> descriptions;

	private ArrayList<ConceptRelationship> relationships;

	public ConceptEdit(long concept_id, String effective_time, boolean active, long module_id,
			boolean is_primitive, String fully_specified_name) {
		super(concept_id, effective_time, active, module_id, is_primitive, fully_specified_name,
				UuidT3Generator.fromSNOMED(concept_id));
		this.descriptions = new ArrayList<ConceptDescription>();
		this.relationships = new ArrayList<ConceptRelationship>();
	}

	public ConceptEdit(ConceptBasic con) {
		this(con.getConceptId(), con.getEffectiveTime(), con.getActive(), con.getModuleId(), con.getIsPrimitive(),
				con.getFullySpecifiedName());
	}

	public ArrayList<ConceptDescription> getDescriptions() {
		return this.descriptions;
	}

	public void setDescriptions(ArrayList<ConceptDescription> descriptions) {
		this.descriptions = descriptions;
	}

	public void addDescription(ConceptDescription description) {
		this.descriptions.add(description);
	}

	public ArrayList<ConceptRelationship> getRelationships() {
		return this.relationships;
	}

	public void setRelationships(ArrayList<ConceptRelationship> relationships) {
		this.relationships = relationships;
	}

	public void addRelationship(ConceptRelationship relationship) {
		this.relationships.add(relationship);
	}

}
