/**
 * Copyright 2012-2017 Mays Systems LLC.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *       http://www.apache.org/licenses/LICENSE-2.0
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package com.mays.snomed;

import java.sql.ResultSet;

public class RelationshipRow {

	public final static String COLUMNS = "id, effective_time, active, module_id, "
			+ "source_id, destination_id, relationship_group, " + "  type_id, characteristic_type_id, modifier_id ";

	public final static String KEY = "id";

	public long id;

	public String effective_time;

	public int active;

	public long module_id;

	public long source_id;

	public long destination_id;

	public int relationship_group;

	public long type_id;

	public long characteristic_type_id;

	public long modifier_id;

	public RelationshipRow(ResultSet res) throws Exception {
		id = res.getLong(1);
		effective_time = res.getString(2);
		active = res.getInt(3);
		module_id = res.getLong(4);
		source_id = res.getLong(5);
		destination_id = res.getLong(6);
		relationship_group = res.getInt(7);
		type_id = res.getLong(8);
		characteristic_type_id = res.getLong(9);
		modifier_id = res.getLong(10);
	}

}