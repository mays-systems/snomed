/**
 * Copyright 2007-2017 Mays Systems LLC.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *       http://www.apache.org/licenses/LICENSE-2.0
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package com.mays.snomed;

import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.HashMap;

import com.mays.snomed.Snomed;
import com.mays.snomed.SnomedDb;
import com.mays.util.subset.I_Isa;

/**
 * Extends SnomedDb to also load the isa rels into memory.
 * 
 * To use this class, call the constructor, then init with a JDBC connection.
 * 
 * @author Eric Mays
 */
public class SnomedIsa extends SnomedDb implements I_Isa {

	protected HashMap<Long, ArrayList<Long>> parents;

	protected HashMap<Long, ArrayList<Long>> children;

	public String getLogString(long con) throws Exception {
		return con + " " + getConcept(con).getFullySpecifiedName();
	}

	@Override
	public ArrayList<Long> getChildren(long con) {
		// They are in memory here, no need to hit the db
		ArrayList<Long> res = this.children.get(con);
		if (res == null)
			res = new ArrayList<Long>(0);
		// Return a copy, otherwise someone(EKM) might change it
		return new ArrayList<Long>(res);
	}

	@Override
	public ArrayList<Long> getParents(long con) {
		// They are in memory here, no need to hit the db
		ArrayList<Long> res = this.parents.get(con);
		if (res == null)
			res = new ArrayList<Long>(0);
		// Return a copy, otherwise someone(EKM) might change it
		return new ArrayList<Long>(res);
	}

	/**
	 * Calls init on the superclass (SnomedDB) to load concepts into memory.
	 * Then loads the isa rels into memory.
	 * 
	 * @param conn
	 *            a JDBC connection
	 */
	public void init(Connection conn) throws Exception {
		long beg = System.currentTimeMillis();
		super.init(conn);
		Runtime rt = Runtime.getRuntime();
		rt.gc();
		System.out.println("Used memory: " + ((rt.totalMemory() - rt.freeMemory()) / (1024 * 1024)));
		this.buildIsaMaps();
		rt.gc();
		System.out.println("Used memory: " + ((rt.totalMemory() - rt.freeMemory()) / (1024 * 1024)));
		System.out.println("buildIsaMaps: " + ((System.currentTimeMillis() - beg) / 1000));
		System.out.println("parents#: " + this.parents.size());
		System.out.println("children#: " + this.children.size());
	}

	protected void buildIsaMaps() throws Exception {
		this.parents = new HashMap<Long, ArrayList<Long>>();
		this.children = new HashMap<Long, ArrayList<Long>>();
		String query = "";
		query += "SELECT concept_id1, concept_id2 ";
		query += "  FROM isa ";
		Statement stmt = this.conn.createStatement();
		ResultSet res = stmt.executeQuery(query);
		while (res.next()) {
			long c1 = res.getLong(1);
			long c2 = res.getLong(2);
			ArrayList<Long> p = this.parents.get(c1);
			if (p == null) {
				p = new ArrayList<Long>();
				this.parents.put(c1, p);
			}
			p.add(c2);
			ArrayList<Long> c = this.children.get(c2);
			if (c == null) {
				c = new ArrayList<Long>();
				this.children.put(c2, c);
			}
			c.add(c1);
		}
		res.close();
		stmt.close();
		for (ArrayList<Long> cl : this.children.values()) {
			cl.trimToSize();
		}
		for (ArrayList<Long> pl : this.parents.values()) {
			pl.trimToSize();
		}
	}

	/**
	 * @param con
	 *            an sctid
	 * 
	 * @return a list of the children of the Snomed root that subsume this
	 *         concept.
	 */
	public ArrayList<Long> getTopLevelSubsumers(long con) {
		ArrayList<Long> top_level_concepts = new ArrayList<Long>();
		for (long child : this.getChildren(Snomed.ROOT_ID)) {
			if (this.isaSubsumesP(child, con)) {
				top_level_concepts.add(child);
			}
		}
		return top_level_concepts;
	}

	/**
	 * Does c1 subsume c2
	 * 
	 * Note: true if c1 == c2
	 * 
	 * @param c1
	 *            an sctid
	 * 
	 * @param c2
	 *            an sctid
	 * 
	 * @return true iff c1 subsumes c2
	 */
	public boolean isaSubsumesP(long c1, long c2) {
		return this.isaSubsumesPx(c1, c2, new HashMap<Long, Boolean>());
	}

	// private boolean isaSubsumesP1(long c1, long c2,
	// HashMap<Long, Boolean> visited) {
	// if (c1 == c2)
	// return true;
	// if (visited.containsKey(c1))
	// return false;
	// ArrayList<Long> children = this.getChildren(c1);
	// if (children == null)
	// return false;
	// if (children.contains(c2))
	// return true;
	// visited.put(c1, true);
	// for (long child : children) {
	// if (isaSubsumesP1(child, c2, visited))
	// return true;
	// }
	// return false;
	// }

	private boolean isaSubsumesPx(long c1, long c2, HashMap<Long, Boolean> visited) {
		if (c1 == c2)
			return true;
		if (visited.containsKey(c2))
			return false;
		ArrayList<Long> parents = this.getParents(c2);
		if (parents.contains(c1))
			return true;
		visited.put(c2, true);
		for (long parent : parents) {
			if (isaSubsumesPx(c1, parent, visited))
				return true;
		}
		return false;
	}

	/**
	 * @param con
	 *            an sctid
	 * 
	 * @return list of descendant sctids (does not include con)
	 */
	public ArrayList<Long> getDescendants(long con) {
		ArrayList<Long> res = new ArrayList<Long>();
		HashMap<Long, Boolean> visited = new HashMap<Long, Boolean>();
		this.getDescendants(con, visited);
		for (Long key : visited.keySet()) {
			res.add(key);
		}
		return res;
	}

	private void getDescendants(long con, HashMap<Long, Boolean> visited) {
		ArrayList<Long> children = this.getChildren(con);
		for (long child : children) {
			if (visited.containsKey(child))
				continue;
			visited.put(child, true);
			getDescendants(child, visited);
		}
	}

	/**
	 * @param con
	 *            an sctid
	 * 
	 * @return list of ancestor sctids
	 */
	public ArrayList<Long> getAncestors(long con) {
		ArrayList<Long> res = new ArrayList<Long>();
		HashMap<Long, Boolean> visited = new HashMap<Long, Boolean>();
		this.getAncestors(con, visited);
		for (Long key : visited.keySet()) {
			res.add(key);
		}
		return res;
	}

	private void getAncestors(long con, HashMap<Long, Boolean> visited) {
		ArrayList<Long> parents = this.getParents(con);
		for (long parent : parents) {
			if (visited.containsKey(parent))
				continue;
			visited.put(parent, true);
			getAncestors(parent, visited);
		}
	}

	// Moved to SnomedDb
	// /**
	// * @param con
	// * an sctid
	// *
	// * @return list of sibling sctids
	// */
	// public ArrayList<Long> getSiblings(long con) {
	// ArrayList<Long> res = new ArrayList<Long>();
	// for (long parent : this.getParents(con)) {
	// for (long child : this.getChildren(parent)) {
	// if (child == con)
	// continue;
	// if (res.contains(child))
	// continue;
	// res.add(child);
	// }
	// }
	// return res;
	// }

}
