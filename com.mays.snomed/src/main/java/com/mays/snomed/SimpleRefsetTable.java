/**
 * Copyright 2012-2017 Mays Systems LLC.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *       http://www.apache.org/licenses/LICENSE-2.0
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package com.mays.snomed;

import java.io.BufferedReader;
import java.sql.Connection;
import java.sql.ParameterMetaData;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;
import java.util.logging.Logger;

import com.mays.util.IO;
import com.mays.util.Sql;

/**
 * @see <a href=
 *      "https://confluence.ihtsdotools.org/display/DOCRELFMT/4.2.1.+Simple+Reference+Set">
 */
public class SimpleRefsetTable {

	public static Logger LOGGER;

	protected Connection conn;

	public SimpleRefsetTable(Connection conn) {
		super();
		this.conn = conn;
	}

	public String getTableName() {
		return this.getClass().getSimpleName().replace("Table", "");
	}

	public String getTableDef() {
		boolean first_p = true;
		String def = "";
		for (String col : getColumnDef()) {
			def += (first_p ? "" : ", ");
			def += col;
			first_p = false;
		}
		return def;
	}

	private SimpleRefsetRow getRowInstance() {
		SimpleRefsetRow row = null;
		String row_class = this.getClass().getName().replace("Table", "Row");
		try {
			row = (SimpleRefsetRow) Class.forName(row_class).newInstance();
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		return row;
	}

	public List<String> getColumnDef() {
		return getRowInstance().getColumnDef();
	}

	public List<String> getIndexColumns() {
		return getRowInstance().getIndexColumns();
	}

	public void createTable() throws SQLException {
		Sql.dropTable(conn, getTableName());
		Sql.createTable(conn, getTableName(), getTableDef());
		for (String col : getIndexColumns()) {
			Sql.createIndex(conn, getTableName(), col);
		}
	}

	public List<String> getColumns() {
		ArrayList<String> ret = new ArrayList<>();
		for (String col : getColumnDef()) {
			col = col.split(" ")[0];
			ret.add(col);
		}
		return ret;
	}

	public String getInsert() {
		String query = "";
		query += "INSERT INTO " + getTableName();
		query += "(";
		String values = " VALUES (";
		boolean first_p = true;
		for (String col : getColumns()) {
			query += (first_p ? "" : ", ");
			query += col;
			values += (first_p ? "" : ", ");
			values += "?";
			first_p = false;
		}
		query += ")";
		values += ")";
		query += values;
		return query;
	}

	public void load(String file) throws Exception {
		String query = getInsert();
		// LOGGER.info(query);
		PreparedStatement stmt = conn.prepareStatement(query);
		ParameterMetaData md = stmt.getParameterMetaData();
		BufferedReader in = IO.createBufferedUtf8FileReader(file);
		String line;
		int line_i = 0;
		while ((line = in.readLine()) != null) {
			line_i++;
			if (line_i == 1)
				continue;
			String[] field = line.split("\\t", -1);
			if (field.length != md.getParameterCount())
				throw new Exception("Field count " + field.length + " expecting " + md.getParameterCount());
			stmt.clearParameters();
			for (int i = 1; i <= md.getParameterCount(); i++) {
				// LOGGER.info(i + " " + md.getParameterType(i) + " " +
				// md.getParameterClassName(i));
				switch (md.getParameterType(i)) {
				case java.sql.Types.CHAR: {
					stmt.setString(i, field[i - 1]);
					break;
				}
				case java.sql.Types.VARCHAR: {
					stmt.setString(i, field[i - 1]);
					break;
				}
				case java.sql.Types.BIGINT: {
					stmt.setLong(i, Long.parseLong(field[i - 1]));
					break;
				}
				case java.sql.Types.INTEGER: {
					stmt.setInt(i, Integer.parseInt(field[i - 1]));
					break;
				}
				default:
					throw new Exception("Unhandled type: " + md.getParameterType(i) + " at parameter " + i);
				}
			}
			stmt.executeUpdate();
		}
		conn.commit();
		stmt.close();
		LOGGER.info("Processed lines: " + line_i);
		LOGGER.info("Loaded " + getTableName() + " from " + file);
	}

	PreparedStatement stmt_get_members;

	public ArrayList<SimpleRefsetRow> getMembers(long refsetId) throws Exception {
		if (stmt_get_members == null) {
			String query = "";
			query += "SELECT * ";
			query += "  FROM " + getTableName();
			query += " WHERE refsetId = ? ";
			stmt_get_members = conn.prepareStatement(query);
		}
		ArrayList<SimpleRefsetRow> ret = new ArrayList<>();
		stmt_get_members.setLong(1, refsetId);
		ResultSet res = stmt_get_members.executeQuery();
		while (res.next()) {
			SimpleRefsetRow row = new SimpleRefsetRow(res);
			if (row.active == 1)
				ret.add(new SimpleRefsetRow(res));
		}
		res.close();
		return ret;
	}

}
