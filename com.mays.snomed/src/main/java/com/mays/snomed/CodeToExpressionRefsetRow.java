/**
 * Copyright 2012-2017 Mays Systems LLC.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *       http://www.apache.org/licenses/LICENSE-2.0
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package com.mays.snomed;

import java.sql.ResultSet;
import java.util.List;

/**
 * @see <a href=
 *      "https://confluence.ihtsdotools.org/display/DOCRELFMT/4.2.15.+Code+to+Expression+Reference+Set">
 */
public class CodeToExpressionRefsetRow extends SimpleRefsetRow {

	// id effectiveTime active moduleId refsetId referencedComponentId
	//
	// mapTarget expression definitionStatusId correlationId contentOriginId

	public String mapTarget;

	public String expression;

	public long definitionStatusId;

	public long correlationId;

	public long contentOriginId;

	CodeToExpressionRefsetRow() {
	}

	public CodeToExpressionRefsetRow(ResultSet res) throws Exception {
		super(res);
		mapTarget = res.getString(columnIndex++);
		expression = res.getString(columnIndex++);
		definitionStatusId = res.getLong(columnIndex++);
		correlationId = res.getLong(columnIndex++);
		contentOriginId = res.getLong(columnIndex++);
	}

	public List<String> getColumnDef() {
		List<String> ret = super.getColumnDef();
		ret.add(getMapTargetColumn()+ " " + "VARCHAR(2048)");
		ret.add("expression " + "VARCHAR(2048)");
		ret.add("definitionStatusId " + "BIGINT");
		ret.add("correlationId " + "BIGINT");
		ret.add("contentOriginId " + "BIGINT");
		return ret;
	}

	public List<String> getIndexColumns() {
		List<String> ret = super.getIndexColumns();
		ret.add(getMapTargetColumn());
		return ret;
	}

	// Since this is used in the table query
	String getMapTargetColumn() {
		return "mapTarget";
	}

}