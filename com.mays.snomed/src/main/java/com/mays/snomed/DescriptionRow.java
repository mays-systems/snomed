/**
 * Copyright 2012-2017 Mays Systems LLC.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *       http://www.apache.org/licenses/LICENSE-2.0
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package com.mays.snomed;

import java.sql.ResultSet;

public class DescriptionRow {

	public final static String COLUMNS = "id, effective_time, active, module_id, "
			+ "concept_id, language_code, type_id, " + "term, case_significance_id ";

	public long id;

	public String effective_time;

	public int active;

	public long module_id;

	public long concept_id;

	public String language_code;

	public long type_id;

	public String term;

	public long case_significance_id;

	public DescriptionRow(ResultSet res) throws Exception {
		id = res.getLong(1);
		effective_time = res.getString(2);
		active = res.getInt(3);
		module_id = res.getLong(4);
		concept_id = res.getLong(5);
		language_code = res.getString(6);
		type_id = res.getLong(7);
		term = res.getString(8);
		case_significance_id = res.getLong(9);
	}

}