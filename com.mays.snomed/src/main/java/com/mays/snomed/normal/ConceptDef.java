/**
 * Copyright 2008-2017 Mays Systems LLC.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *       http://www.apache.org/licenses/LICENSE-2.0
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package com.mays.snomed.normal;

import java.util.ArrayList;

public class ConceptDef extends ConceptRef {

	private boolean is_primitive;

	private ArrayList<ConceptRef> super_concepts = new ArrayList<ConceptRef>();

	private ArrayList<Role> ungrouped_roles = new ArrayList<Role>();

	private ArrayList<RoleGroup> role_groups = new ArrayList<RoleGroup>();

	public ConceptDef(long concept_id, String fully_specified_name) {
		super(concept_id, fully_specified_name);
	}

	public boolean isPrimitive() {
		return is_primitive;
	}

	public void setPrimitive(boolean is_primitive) {
		this.is_primitive = is_primitive;
	}

	public ArrayList<ConceptRef> getSuperConcepts() {
		return super_concepts;
	}

	public void addSuperConcept(ConceptRef con) {
		this.super_concepts.add(con);
	}

	public ArrayList<Role> getUngroupedRoles() {
		return ungrouped_roles;
	}

	public void addUngroupedRole(Role role) {
		this.ungrouped_roles.add(role);
	}

	public ArrayList<RoleGroup> getRoleGroups() {
		return role_groups;
	}

	public void addGroupedRole(int group, Role role) {
		RoleGroup role_group = null;
		for (RoleGroup rg : this.role_groups) {
			if (rg.getId() == group) {
				role_group = rg;
				break;
			}
		}
		if (role_group == null) {
			role_group = new RoleGroup(group);
			this.role_groups.add(role_group);
		}
		role_group.addRole(role);
	}

}
