/**
 * Copyright 2008-2017 Mays Systems LLC.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *       http://www.apache.org/licenses/LICENSE-2.0
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package com.mays.snomed.normal;

public class ConceptRef implements Comparable<ConceptRef> {

	private long concept_id;

	private String fully_specified_name;

	public ConceptRef(long concept_id, String fully_specified_name) {
		super();
		this.concept_id = concept_id;
		this.fully_specified_name = fully_specified_name;
	}

	public long getConceptId() {
		return concept_id;
	}

	public String getFullySpecifiedName() {
		return fully_specified_name;
	}

	@Override
	public boolean equals(Object obj) {
		if (!(obj instanceof ConceptRef))
			return false;
		ConceptRef concept_ref = (ConceptRef) obj;
		return this.getConceptId() == concept_ref.getConceptId();
	}

	@Override
	public int compareTo(ConceptRef that) {
		return new Long(this.getConceptId()).compareTo(that.getConceptId());
	}

}
