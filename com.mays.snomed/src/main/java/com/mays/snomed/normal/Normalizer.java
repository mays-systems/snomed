/**
 * Copyright 2008-2017 Mays Systems LLC.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *       http://www.apache.org/licenses/LICENSE-2.0
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package com.mays.snomed.normal;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashSet;

import com.mays.snomed.ConceptBasic;
import com.mays.snomed.ConceptRelationship;
import com.mays.snomed.Snomed;
import com.mays.snomed.SnomedIsa;
import com.mays.snomed.Snomed.CharacteristicType;
import com.mays.util.Sql;

public class Normalizer {

	private Connection conn;

	private SnomedIsa snomed_db;

	private PreparedStatement stmt_canonical_rels;

	public void init() throws Exception {
		this.conn = Sql.getConnection("c:/ddb", "snomed");
		this.snomed_db = new SnomedIsa();
		this.snomed_db.init(this.conn);
		String query = "";
		query += "SELECT relationship_type, concept_id2, relationship_group ";
		query += "  FROM canonical ";
		query += " WHERE concept_id1 = ? ";
		stmt_canonical_rels = this.conn.prepareStatement(query);
	}

	public ConceptRef makeConceptRef(ConceptBasic con) {
		return new ConceptRef(con.getConceptId(), con.getFullySpecifiedName());
	}

	public ConceptDef getConceptDef(long id) throws Exception {
		ConceptBasic con = this.snomed_db.getConcept(id);
		ConceptDef cond = new ConceptDef(con.getConceptId(), con.getFullySpecifiedName());
		for (ConceptRelationship rel : this.snomed_db.getRelationships(id)) {
			ConceptRef rel_con = makeConceptRef(this.snomed_db.getConcept(rel.getRelationshipType()));
			ConceptRef con2 = makeConceptRef(this.snomed_db.getConcept(rel.getConceptId2()));
			if (rel.getRelationshipType() == Snomed.ISA_ID) {
				cond.addSuperConcept(con2);
				continue;
			}
			if (rel.getCharacteristicType().equals(CharacteristicType.DEFINING)) {
				Role role = new Role(rel_con, con2);
				if (rel.getRelationshipGroup() == 0) {
					cond.addUngroupedRole(role);
				} else {
					cond.addGroupedRole(rel.getRelationshipGroup(), role);
				}
			}
		}
		return cond;
	}

	public ConceptDef getConceptDefCanonical(long id) throws Exception {
		ConceptBasic con = this.snomed_db.getConcept(id);
		ConceptDef cond = new ConceptDef(con.getConceptId(), con.getFullySpecifiedName());
		this.stmt_canonical_rels.setLong(1, id);
		ResultSet res = this.stmt_canonical_rels.executeQuery();
		while (res.next()) {
			long relationship_type = res.getLong(1);
			long concept_id2 = res.getLong(2);
			int relationship_group = res.getInt(3);
			ConceptRef rel_con = makeConceptRef(this.snomed_db.getConcept(relationship_type));
			ConceptRef con2 = makeConceptRef(this.snomed_db.getConcept(concept_id2));
			if (relationship_type == Snomed.ISA_ID) {
				cond.addSuperConcept(con2);
				continue;
			}
			Role role = new Role(rel_con, con2);
			if (relationship_group == 0) {
				cond.addUngroupedRole(role);
			} else {
				cond.addGroupedRole(relationship_group, role);
			}
		}
		return cond;
	}

	public void doConcept(long id) throws Exception {
		ConceptDef cond = this.getConceptDef(id);
		System.out.println("DEF");
		printConceptDef(cond);
		System.out.println("LONG");
		printConceptDef(toLongNormal(cond));
		System.out.println("SHORT");
		printConceptDef(toShortNormal(cond));
	}

	public void printConceptDef(ConceptDef cond) {
		System.out.println(cond.getFullySpecifiedName());
		for (ConceptRef c : cond.getSuperConcepts()) {
			System.out.println("\t" + c.getFullySpecifiedName());
		}
		for (Role r : cond.getUngroupedRoles()) {
			System.out.println(
					"\t" + r.getRoleType().getFullySpecifiedName() + " : " + r.getRoleValue().getFullySpecifiedName());
		}
		for (RoleGroup rg : cond.getRoleGroups()) {
			for (Role r : rg.getRoles()) {
				System.out.println("\t" + rg.getId() + "-" + r.getRoleType().getFullySpecifiedName() + " : "
						+ r.getRoleValue().getFullySpecifiedName());
			}
		}
	}

	public ArrayList<ConceptRef> findProximalPrimitives(ConceptRef con) {
		return this.findProximalPrimitives(new ArrayList<ConceptRef>(Arrays.asList(con)));
	}

	public ArrayList<ConceptRef> findProximalPrimitives(ArrayList<ConceptRef> cons) {
		ArrayList<ConceptRef> res = new ArrayList<ConceptRef>();
		for (long c : getLeastPrimitiveAncestors(cons)) {
			res.add(new ConceptRef(c, this.snomed_db.getConcept(c).getFullySpecifiedName()));
		}
		return res;
	}

	public ArrayList<Long> getLeastPrimitiveAncestors(ArrayList<ConceptRef> cons) {
		HashSet<Long> prims = new HashSet<Long>();
		for (ConceptRef con : cons) {
			getLeastPrimitiveAncestors(con.getConceptId(), new HashSet<Long>(), prims);
		}
		ArrayList<Long> res = new ArrayList<Long>();
		for (long c1 : prims) {
			boolean least_p = true;
			for (long c2 : prims) {
				if (c1 != c2 && this.snomed_db.isaSubsumesP(c1, c2)) {
					least_p = false;
					break;
				}
			}
			if (least_p)
				res.add(c1);
		}
		return res;
	}

	public void getLeastPrimitiveAncestors(long con, HashSet<Long> visited, HashSet<Long> res) {
		if (visited.contains(con))
			return;
		visited.add(con);
		if (this.snomed_db.getConcept(con).getIsPrimitive()) {
			res.add(con);
			return;
		}
		for (long sup : this.snomed_db.getParents(con)) {
			getLeastPrimitiveAncestors(sup, visited, res);
		}
	}

	public ConceptDef toLongNormal(ConceptDef con) {
		ConceptDef conln = new ConceptDef(con.getConceptId(), con.getFullySpecifiedName());
		for (ConceptRef c : findProximalPrimitives(con.getSuperConcepts())) {
			conln.addSuperConcept(c);
		}
		// Copy these
		for (Role r : con.getUngroupedRoles()) {
			conln.addUngroupedRole(r);
		}
		for (RoleGroup rg : con.getRoleGroups()) {
			for (Role r : rg.getRoles()) {
				conln.addGroupedRole(rg.getId(), r);
			}
		}
		return conln;
	}

	public boolean roleSubsumes(Role r1, Role r2) {
		return r1.getRoleType().equals(r2.getRoleType())
				&& this.snomed_db.isaSubsumesP(r1.getRoleValue().getConceptId(), r2.getRoleValue().getConceptId());
	}

	public boolean roleGroupSubsumes(RoleGroup rg1, RoleGroup rg2) {
		for (Role r1 : rg1.getRoles()) {
			boolean subsumes_p = false;
			for (Role r2 : rg2.getRoles()) {
				if (roleSubsumes(r1, r2)) {
					subsumes_p = true;
					break;
				}
			}
			if (!subsumes_p)
				return false;
		}
		return true;
	}

	public ConceptDef toShortNormal(ConceptDef con) throws Exception {
		ConceptDef conln = new ConceptDef(con.getConceptId(), con.getFullySpecifiedName());
		for (ConceptRef c : findProximalPrimitives(con.getSuperConcepts())) {
			conln.addSuperConcept(c);
		}
		ArrayList<Role> sups_roles = new ArrayList<Role>();
		ArrayList<RoleGroup> sups_role_groups = new ArrayList<RoleGroup>();
		for (ConceptRef c : conln.getSuperConcepts()) {
			for (Role r : this.getConceptDef(c.getConceptId()).getUngroupedRoles()) {
				sups_roles.add(r);
			}
			for (RoleGroup rg : this.getConceptDef(c.getConceptId()).getRoleGroups()) {
				sups_role_groups.add(rg);
			}
		}
		for (Role r : con.getUngroupedRoles()) {
			boolean restricts_p = true;
			for (Role sup_r : sups_roles) {
				if (roleSubsumes(r, sup_r)) {
					restricts_p = false;
					break;
				}
			}
			if (restricts_p)
				conln.addUngroupedRole(r);
		}
		for (RoleGroup rg : con.getRoleGroups()) {
			boolean restricts_p = true;
			for (RoleGroup sup_rg : sups_role_groups) {
				if (roleGroupSubsumes(rg, sup_rg)) {
					restricts_p = false;
					break;
				}
			}
			if (restricts_p) {
				for (Role r : rg.getRoles()) {
					conln.addGroupedRole(rg.getId(), r);
				}
			}
		}
		return conln;
	}

	public void printRoles(String tag, ArrayList<Role> roles) {
		for (Role r : roles) {
			System.out.println(tag + " " + r.getRoleType().getFullySpecifiedName() + " -> "
					+ r.getRoleValue().getFullySpecifiedName());
		}
	}

	public void printRoleGroups(String tag, ArrayList<RoleGroup> role_groups) {
		for (RoleGroup rg : role_groups) {
			System.out.println(tag + " Group: ");
			for (Role r : rg.getRoles()) {
				System.out.println(tag + " " + r.getRoleType().getFullySpecifiedName() + " -> "
						+ r.getRoleValue().getFullySpecifiedName());
			}
		}
	}

	public void compareConceptDefs(ConceptDef c1, ConceptDef c2, boolean sups_only) {
		ArrayList<ConceptRef> c1_sups_d = new ArrayList<ConceptRef>(c1.getSuperConcepts());
		ArrayList<ConceptRef> c2_sups_d = new ArrayList<ConceptRef>(c2.getSuperConcepts());
		c1_sups_d.removeAll(c2.getSuperConcepts());
		c2_sups_d.removeAll(c1.getSuperConcepts());
		if (c1_sups_d.size() != 0 || c2_sups_d.size() != 0) {
			System.out.println("BOGUS SUPS: " + c1.getFullySpecifiedName());
		}
		if (sups_only)
			return;
		ArrayList<Role> c1_roles_d = new ArrayList<Role>(c1.getUngroupedRoles());
		ArrayList<Role> c2_roles_d = new ArrayList<Role>(c2.getUngroupedRoles());
		c1_roles_d.removeAll(c2.getUngroupedRoles());
		c2_roles_d.removeAll(c1.getUngroupedRoles());
		if (c1_roles_d.size() != 0 || c2_roles_d.size() != 0) {
			System.out.println("BOGUS ROLES: " + c1.getFullySpecifiedName());
			printRoles("1:", c1_roles_d);
			printRoles("2:", c2_roles_d);
			printConceptDef(c1);
			printConceptDef(c2);
		}
		ArrayList<RoleGroup> c1_roles_grouped_d = new ArrayList<RoleGroup>(c1.getRoleGroups());
		ArrayList<RoleGroup> c2_roles_grouped_d = new ArrayList<RoleGroup>(c2.getRoleGroups());
		c1_roles_grouped_d.removeAll(c2.getRoleGroups());
		c2_roles_grouped_d.removeAll(c1.getRoleGroups());
		if (c1_roles_grouped_d.size() != 0 || c2_roles_grouped_d.size() != 0) {
			System.out.println("BOGUS ROLES: " + c1.getFullySpecifiedName());
			printRoleGroups("1:", c1_roles_grouped_d);
			printRoleGroups("2:", c2_roles_grouped_d);
			printConceptDef(c1);
			printConceptDef(c2);
		}
	}

	public boolean isEquivalent(ConceptDef input, ConceptDef list) {
		return true;
	}

	public boolean isRedundant(ConceptDef input, ArrayList<ConceptDef> list) {
		for (ConceptDef con : list) {
			if (isEquivalent(input, con))
				return true;
		}
		return false;
	}

	public void doConcepts() throws Exception {
		doConcept(71620000);
		doConcept(195967001);
		doConcept(389145006);
		doConcept(126716006);
		int line_i = 0;
		long beg = System.currentTimeMillis();
		long long_normal_time = 0;
		int long_normal_count = 0;
		long short_normal_time = 0;
		int short_normal_count = 0;
		long compare_concept_defs_time = 0;
		int compare_concept_defs_count = 0;
		for (ConceptBasic con : snomed_db.getConcepts()) {
			line_i++;
			if (line_i % 1000 == 0) {
				System.out.println("Line: " + line_i + " - " + (System.currentTimeMillis() - beg) / 1000);
				System.out.println("long_normal_time: " + long_normal_time);
				System.out.println("long_normal_count: " + long_normal_count);
				System.out.println("long_normal_time per: " + (long_normal_time / long_normal_count));
				System.out.println("short_normal_time: " + short_normal_time);
				System.out.println("short_normal_count: " + short_normal_count);
				System.out.println("short_normal_time per: " + (short_normal_time / short_normal_count));
				System.out.println("compare_concept_defs_time: " + compare_concept_defs_time);
				System.out.println("compare_concept_defs_count: " + compare_concept_defs_count);
				System.out.println(
						"compare_concept_defs_time per: " + (compare_concept_defs_time / compare_concept_defs_count));
			}
			if (con.getIsPrimitive())
				continue;
			ConceptDef concept_def = this.getConceptDef(con.getConceptId());
			ConceptDef canonical_def = this.getConceptDefCanonical(con.getConceptId());
			long lnt = System.currentTimeMillis();
			long_normal_count++;
			ConceptDef longnf_def = toLongNormal(concept_def);
			long_normal_time = long_normal_time + (System.currentTimeMillis() - lnt);
			// ArrayList<Long> canonical = new ArrayList<Long>();
			// for (ConceptRef c : canonical_def.getSuperConcepts()) {
			// canonical.add(c.getConceptId());
			// }
			// ArrayList<Long> longnf = new ArrayList<Long>();
			// for (ConceptRef c : longnf_def.getSuperConcepts()) {
			// longnf.add(c.getConceptId());
			// }
			// if (!(canonical.containsAll(longnf) && longnf
			// .containsAll(canonical))) {
			// System.out.println("BOGUS: " + con.getFullySpecifiedName());
			// for (long p : longnf) {
			// if (!canonical.contains(p)) {
			// System.out.println("\tP: " + p);
			// }
			// }
			// for (long c : canonical) {
			// if (!longnf.contains(c)) {
			// System.out.println("\tC: " + c);
			// }
			// }
			// }
			compareConceptDefs(canonical_def, longnf_def, true);
			long snt = System.currentTimeMillis();
			short_normal_count++;
			ConceptDef shortnf_def = toShortNormal(concept_def);
			short_normal_time = short_normal_time + (System.currentTimeMillis() - snt);
			long ccdt = System.currentTimeMillis();
			compare_concept_defs_count++;
			compareConceptDefs(canonical_def, shortnf_def, false);
			compare_concept_defs_time = compare_concept_defs_time + (System.currentTimeMillis() - ccdt);
		}
		System.out.println("long_normal_time: " + long_normal_time);
		System.out.println("long_normal_count: " + long_normal_count);
		System.out.println("long_normal_time per: " + (long_normal_time / long_normal_count));
		System.out.println("short_normal_time: " + short_normal_time);
		System.out.println("short_normal_count: " + short_normal_count);
		System.out.println("short_normal_time per: " + (short_normal_time / short_normal_count));
		System.out.println("compare_concept_defs_time: " + compare_concept_defs_time);
		System.out.println("compare_concept_defs_count: " + compare_concept_defs_count);
		System.out
				.println("compare_concept_defs_time per: " + (compare_concept_defs_time / compare_concept_defs_count));
	}

	// private PreparedStatement stmt_canonical_isa;
	//
	// public ArrayList<Long> getCanonicalIsa(long con) throws Exception {
	// ArrayList<Long> cons = new ArrayList<Long>();
	// stmt_canonical_isa.setLong(1, con);
	// ResultSet res = stmt_canonical_isa.executeQuery();
	// while (res.next()) {
	// long c1 = res.getLong(1);
	// cons.add(c1);
	// }
	// return cons;
	// }

	// public void doConceptsOld() throws Exception {
	// doConcept(71620000);
	// doConcept(195967001);
	// doConcept(389145006);
	// doConcept(126716006);
	// String query = "";
	// query += "SELECT concept_id2 ";
	// query += " FROM canonical ";
	// query += " WHERE relationship_type = " + Snomed.ISA_ID + " ";
	// query += " AND concept_id1 = ? ";
	// stmt_canonical_isa = this.conn.prepareStatement(query);
	// int line_i = 0;
	// long beg = System.currentTimeMillis();
	// for (ConceptBasic con : snomed_db.getConcepts()) {
	// line_i++;
	// if (line_i % 10000 == 0) {
	// System.out.println("Line: " + line_i + " - "
	// + (System.currentTimeMillis() - beg) / 1000);
	// }
	// if (con.getIsPrimitive())
	// continue;
	// ArrayList<Long> canonical = this
	// .getCanonicalIsa(con.getConceptId());
	// ArrayList<Long> proximal = new ArrayList<Long>();
	// for (ConceptRef c : this
	// .findProximalPrimitives(makeConceptRef(con))) {
	// proximal.add(c.getConceptId());
	// }
	// if (!(canonical.containsAll(proximal) && proximal
	// .containsAll(canonical))) {
	// System.out.println("BOGUS: " + con.getFullySpecifiedName());
	// for (long p : proximal) {
	// if (!canonical.contains(p)) {
	// System.out.println("\tP: " + p);
	// }
	// }
	// for (long c : canonical) {
	// if (!proximal.contains(c)) {
	// System.out.println("\tC: " + c);
	// }
	// }
	// }
	// }
	// }

	public void run() throws Exception {
		this.init();
		this.doConcepts();
	}

	public static void main(String argv[]) {
		try {
			Normalizer m = new Normalizer();
			m.run();
		} catch (Exception ex) {
			System.out.println(ex.getMessage());
			ex.printStackTrace();
		}
	}
}
