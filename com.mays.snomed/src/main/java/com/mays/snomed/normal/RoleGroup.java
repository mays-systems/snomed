/**
 * Copyright 2008-2017 Mays Systems LLC.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *       http://www.apache.org/licenses/LICENSE-2.0
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package com.mays.snomed.normal;

import java.util.ArrayList;

public class RoleGroup {

	int id;

	private ArrayList<Role> roles = new ArrayList<Role>();

	public RoleGroup(int id) {
		super();
		this.id = id;
	}

	public ArrayList<Role> getRoles() {
		return roles;
	}

	public void addRole(Role role) {
		this.roles.add(role);
	}

	public int getId() {
		return id;
	}

	@Override
	public boolean equals(Object obj) {
		if (!(obj instanceof RoleGroup))
			return false;
		RoleGroup role_group = (RoleGroup) obj;
		return this.getRoles().containsAll(role_group.getRoles())
				&& role_group.getRoles().containsAll(this.getRoles());
	}

}
