/**
 * Copyright 2008-2017 Mays Systems LLC.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *       http://www.apache.org/licenses/LICENSE-2.0
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package com.mays.snomed.normal;

public class Role {

	private ConceptRef role_type;

	private ConceptRef role_value;

	public Role(ConceptRef role_type, ConceptRef role_value) {
		super();
		this.role_type = role_type;
		this.role_value = role_value;
	}

	public ConceptRef getRoleType() {
		return role_type;
	}

	public ConceptRef getRoleValue() {
		return role_value;
	}

	@Override
	public boolean equals(Object obj) {
		if (!(obj instanceof Role))
			return false;
		Role role = (Role) obj;
		return this.getRoleType().equals(role.getRoleType())
				&& this.getRoleValue().equals(role.getRoleValue());
	}

}
