/**
 * Copyright 2008-2017 Mays Systems LLC.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *       http://www.apache.org/licenses/LICENSE-2.0
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package com.mays.snomed;

import java.io.BufferedReader;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.util.logging.Logger;

import com.mays.snomed.CanonicalLoader;
import com.mays.util.BasicLogger;
import com.mays.util.Config;
import com.mays.util.IO;
import com.mays.util.Sql;

public class CanonicalLoader {

	private static Logger LOGGER;

	private Config config;

	private Connection conn;

	public CanonicalLoader(String config_name) throws Exception {
		super();
		this.config = Config.load(config_name, LOGGER);
	}

	public void init() throws Exception {
		String derby_home = config.getProperty(Sql.DERBY_HOME_KEY, LOGGER);
		String schema = config.getProperty(Sql.SCHEMA_KEY, LOGGER);
		this.init(Sql.getConnection(derby_home, schema));
	}

	public void init(Connection conn) throws Exception {
		this.conn = conn;
	}

	public void shutdown() throws Exception {
		String schema = config.getProperty(Sql.SCHEMA_KEY, LOGGER);
		Sql.shutdown(schema);
	}

	public void createSchema() throws Exception {
		String query;
		//
		Sql.dropTable(conn, "canonical");
		query = "";
		// query += "relationship_id BIGINT PRIMARY KEY, ";
		query += "concept_id1 BIGINT , ";
		query += "relationship_type BIGINT , ";
		query += "concept_id2 BIGINT , ";
		// query += "characteristic_type INT, ";
		// query += "refinability INT, ";
		query += "relationship_group INT ";
		Sql.createTable(conn, "canonical", query);
		conn.commit();
		LOGGER.info("Created schema");
	}

	public void createIndexes() throws Exception {
		Sql.createIndex(conn, "canonical", "concept_id1");
		Sql.createIndex(conn, "canonical", "concept_id2");
		this.conn.commit();
		LOGGER.info("Created indexes");
	}

	public void loadCanonical() throws Exception {
		String data_dir = config.getProperty("data_dir", LOGGER);
		String file = config.getProperty("canonical_file", LOGGER);
		file = data_dir + file;
		String query = "";
		query += "INSERT INTO canonical ";
		query += " (concept_id1, relationship_type, ";
		query += "  concept_id2, ";
		query += "  relationship_group )";
		query += " VALUES (?, ?, ?, ?)";
		PreparedStatement stmt = conn.prepareStatement(query);
		BufferedReader in = IO.createBufferedUtf8FileReader(file);
		String line;
		int line_i = 0;
		while ((line = in.readLine()) != null) {
			line_i++;
			if (line_i == 1)
				continue;
			String[] field = line.split("\t");
			stmt.setLong(1, Long.parseLong(field[0]));
			stmt.setLong(2, Long.parseLong(field[1]));
			stmt.setLong(3, Long.parseLong(field[2]));
			stmt.setInt(4, Integer.parseInt(field[3]));
			stmt.executeUpdate();
			if (line_i % 10000 == 0) {
				LOGGER.fine("Line: " + line_i);
			}
		}
		conn.commit();
		stmt.close();
		LOGGER.info("Processed lines: " + line_i);
		LOGGER.info("Loaded canonical");
	}

	public void run() throws Exception {
		this.init();
		this.createSchema();
		this.loadCanonical();
		this.createIndexes();
		this.shutdown();
	}

	public static void main(String[] args) {
		try {
			CanonicalLoader.LOGGER = BasicLogger.init(CanonicalLoader.class);
			Sql.LOGGER = CanonicalLoader.LOGGER;
			String config_name = Config.getConfigName(args, LOGGER);
			CanonicalLoader m = new CanonicalLoader(config_name);
			m.run();
		} catch (Exception ex) {
			BasicLogger.log(ex, LOGGER);
		} finally {
			BasicLogger.end(LOGGER);
		}
	}

}
