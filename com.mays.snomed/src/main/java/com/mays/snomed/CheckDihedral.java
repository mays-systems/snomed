/**
 * Copyright 2007-2017 Mays Systems LLC.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *       http://www.apache.org/licenses/LICENSE-2.0
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package com.mays.snomed;

/**
 * An implementation of Verhoeff�s Dihedral Check as specified in the SNOMED CT
 * technical documentation
 * 
 * @author Eric Mays
 * 
 */
public class CheckDihedral {

	private static final int d[][] = { { 0, 1, 2, 3, 4, 5, 6, 7, 8, 9 },
			{ 1, 2, 3, 4, 0, 6, 7, 8, 9, 5 }, { 2, 3, 4, 0, 1, 7, 8, 9, 5, 6 },
			{ 3, 4, 0, 1, 2, 8, 9, 5, 6, 7 }, { 4, 0, 1, 2, 3, 9, 5, 6, 7, 8 },
			{ 5, 9, 8, 7, 6, 0, 4, 3, 2, 1 }, { 6, 5, 9, 8, 7, 1, 0, 4, 3, 2 },
			{ 7, 6, 5, 9, 8, 2, 1, 0, 4, 3 }, { 8, 7, 6, 5, 9, 3, 2, 1, 0, 4 },
			{ 9, 8, 7, 6, 5, 4, 3, 2, 1, 0 } };

	private static final int inv[] = { 0, 4, 3, 2, 1, 5, 6, 7, 8, 9 };

	private static final int f[][] = { { 0, 1, 2, 3, 4, 5, 6, 7, 8, 9 },
			{ 1, 5, 7, 6, 2, 8, 3, 0, 9, 4 }, { 5, 8, 0, 3, 7, 9, 6, 1, 4, 2 },
			{ 8, 9, 1, 6, 0, 4, 3, 5, 2, 7 }, { 9, 4, 5, 3, 1, 2, 6, 8, 7, 0 },
			{ 4, 2, 8, 6, 5, 7, 3, 9, 0, 1 }, { 2, 7, 9, 3, 8, 0, 6, 4, 1, 5 },
			{ 7, 0, 4, 6, 9, 1, 3, 2, 5, 8 } };

	/**
	 * Verify the checksum
	 * 
	 * @param digits 
	 *            a string of digits to verify
	 * 
	 * @return true iff the checksum is verified
	 */
	public static boolean verify(String digits) {
		int check = 0;
		for (int i = digits.length() - 1; i >= 0; i--) {
			int c = Integer.parseInt(Character.toString(digits.charAt(i)));
			check = d[check][f[(digits.length() - i - 1) % 8][c]];
		}
		return (check == 0);
	}

	/**
	 * Compute the checksum
	 * 
	 * @param digits 
	 *            a string of digits
	 * 
	 * @return the check digit
	 */
	public static int compute(String digits) {
		int check = 0;
		for (int i = digits.length() - 1; i >= 0; i--) {
			int c = Integer.parseInt(Character.toString(digits.charAt(i)));
			check = d[check][f[(digits.length() - i) % 8][c]];
		}
		return inv[check];
	}

}
