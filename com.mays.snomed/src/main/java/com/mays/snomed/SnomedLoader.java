/**
 * Copyright 2007-2017 Mays Systems LLC.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *       http://www.apache.org/licenses/LICENSE-2.0
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package com.mays.snomed;

import java.io.BufferedReader;
import java.io.PrintWriter;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.ResultSetMetaData;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.Arrays;
import java.util.logging.Logger;

import com.mays.snomed.Snomed.DescriptionType;
import com.mays.util.BasicLogger;
import com.mays.util.BasicProgressMonitor;
import com.mays.util.BasicProgressMonitorNoOp;
import com.mays.util.Config;
import com.mays.util.IO;
import com.mays.util.Sql;

/**
 * Load SNOMED from the standard distribution into an Apache Derby db.
 * 
 * <p>
 * Core tables only, concepts, relationships, descriptions
 * 
 * <p>
 * Need to provide a configuration file
 * 
 * <br>
 * derby home - the directory to store the Derby db
 * 
 * <br>
 * schema - the Derby schema name
 * 
 * <br>
 * data_dir - the directory containing the data files
 * 
 * <br>
 * concept_file = the concepts file
 * 
 * <br>
 * description_file = the descriptions file
 * 
 * <br>
 * relationship_file = the relationships file
 * 
 * <br>
 * stated_relationship_file = the relationships file
 * 
 * <p>
 * Sample SnomedLoader.properties file:
 * 
 * <br>
 * derby_home = /ddb
 * 
 * <br>
 * schema = snomed
 * 
 * <br>
 * data_dir =
 * /data/snomed/SnomedCT_RF2Release_US1000124_20160901/Snapshot/Terminology/
 * 
 * <br>
 * concept_file = sct2_Concept_Snapshot_US1000124_20160901.txt
 * 
 * <br>
 * description_file = sct2_Description_Snapshot-en_US1000124_20160901.txt
 * 
 * <br>
 * relationship_file = sct2_Relationship_Snapshot_US1000124_20160901.txt
 * 
 * <br>
 * stated_relationship_file =
 * sct2_StatedRelationship_Snapshot_US1000124_20160901.txt
 * 
 * @author Eric Mays
 * 
 */
public class SnomedLoader {

	public final static String DATA_DIR_KEY = "data_dir";

	public final static String CONCEPT_FILE_KEY = "concept_file";

	public final static String DESCRIPTION_FILE_KEY = "description_file";

	public final static String RELATIONSHIP_FILE_KEY = "relationship_file";

	public final static String STATED_RELATIONSHIP_FILE_KEY = "stated_relationship_file";

	public final static String REFSET_DATA_DIR_KEY = "refset_data_dir";

	public final static String REFSET_SIMPLE_FILE_KEY = "refset_simple_file";

	public static Logger LOGGER;

	protected Config config;

	private boolean ext_p = false;

	protected Connection conn;

	protected BasicProgressMonitor progress_monitor;

	public SnomedLoader(String config_name) throws Exception {
		super();
		this.config = Config.load(config_name, LOGGER);
	}

	public SnomedLoader(Config config) throws Exception {
		super();
		this.config = config;
	}

	/**
	 * @param mon
	 */
	public void setProgressMonitor(BasicProgressMonitor progress_monitor) {
		this.progress_monitor = progress_monitor;
	}

	public void setExtension(boolean extension) {
		this.ext_p = extension;
	}

	public boolean getExtension() {
		return ext_p;
	}

	/**
	 * Get the JDBC connection
	 */
	public void init() throws Exception {
		String derby_home = config.getProperty(Sql.DERBY_HOME_KEY, LOGGER);
		String schema = config.getProperty(Sql.SCHEMA_KEY, LOGGER);
		this.init(Sql.getConnection(derby_home, schema, true));
	}

	public void init(Connection conn) throws Exception {
		this.conn = conn;
	}

	public void shutdown() throws Exception {
		conn.close();
		Sql.shutdown(config.getProperty(Sql.SCHEMA_KEY, LOGGER));
	}

	public void createSchema() throws Exception {
		String query;
		//
		Sql.dropTable(conn, "concept");
		query = "";
		query += "id BIGINT PRIMARY KEY, ";
		query += "effective_time VARCHAR(8), ";
		query += "active INT, ";
		query += "module_id BIGINT, ";
		query += "definition_status_id BIGINT, ";
		query += "fsn VARCHAR(2048), ";
		query += "uuid CHAR(36) ";
		Sql.createTable(conn, "concept", query);
		//
		Sql.dropTable(conn, "description");
		query = "";
		query += "id BIGINT PRIMARY KEY, ";
		query += "effective_time VARCHAR(8), ";
		query += "active INT, ";
		query += "module_id BIGINT, ";
		query += "concept_id BIGINT, ";
		query += "language_code VARCHAR(8), ";
		query += "type_id BIGINT, ";
		query += "term VARCHAR(2048), ";
		query += "case_significance_id BIGINT ";
		Sql.createTable(conn, "description", query);
		//
		Sql.dropTable(conn, "relationship");
		query = "";
		query += "id BIGINT PRIMARY KEY, ";
		query += "effective_time VARCHAR(8), ";
		query += "active INT, ";
		query += "module_id BIGINT, ";
		query += "source_id BIGINT, ";
		query += "destination_id BIGINT, ";
		query += "relationship_group INT, ";
		query += "type_id BIGINT, ";
		query += "characteristic_type_id BIGINT, ";
		query += "modifier_id BIGINT ";
		Sql.createTable(conn, "relationship", query);
		//
		Sql.dropTable(conn, "stated_relationship");
		query = "";
		query += "id BIGINT PRIMARY KEY, ";
		query += "effective_time VARCHAR(8), ";
		query += "active INT, ";
		query += "module_id BIGINT, ";
		query += "source_id BIGINT, ";
		query += "destination_id BIGINT, ";
		query += "relationship_group INT, ";
		query += "type_id BIGINT, ";
		query += "characteristic_type_id BIGINT, ";
		query += "modifier_id BIGINT ";
		Sql.createTable(conn, "stated_relationship", query);
		//
		// identifierSchemeId alternateIdentifier effectiveTime active moduleId
		// referencedComponentId
		Sql.dropTable(conn, "identifier");
		query = "";
		query += "identifier_scheme_id BIGINT, ";
		query += "alternate_identifier VARCHAR(2048), ";
		query += "effective_time VARCHAR(8), ";
		query += "active INT, ";
		query += "module_id BIGINT, ";
		query += "referenced_component_id BIGINT ";
		Sql.createTable(conn, "identifier", query);
		//
		SimpleRefsetTable.LOGGER = LOGGER;
		SimpleRefsetTable refset_table = new SimpleRefsetTable(conn);
		refset_table.createTable();
		//
		conn.commit();
		LOGGER.info("Created schema");
	}

	public void createIndexes() throws Exception {
		Sql.createIndex(conn, "description", "concept_id");
		Sql.createIndex(conn, "relationship", "source_id");
		Sql.createIndex(conn, "relationship", "destination_id");
		Sql.createIndex(conn, "stated_relationship", "source_id");
		Sql.createIndex(conn, "stated_relationship", "destination_id");
		Sql.createIndex(conn, "identifier", "alternate_identifier");
		Sql.createIndex(conn, "identifier", "referenced_component_id");
		this.conn.commit();
		LOGGER.info("Created indexes");
	}

	public void loadConcepts() throws Exception {
		String data_dir = config.getProperty(DATA_DIR_KEY, LOGGER);
		String file = config.getProperty(CONCEPT_FILE_KEY, LOGGER);
		file = data_dir + file;
		String query = "";
		query += "INSERT INTO concept ";
		query += " ( " + ConceptRow.COLUMNS + " )";
		query += " VALUES (?, ?, ?, ?, ?, ?, ?)";
		PreparedStatement stmt = conn.prepareStatement(query);
		BufferedReader in = IO.createBufferedUtf8FileReader(file);
		String line;
		int line_i = 0;
		while ((line = in.readLine()) != null) {
			line_i++;
			if (line_i == 1)
				continue;
			String[] field = line.split("\\t");
			stmt.setLong(1, Long.parseLong(field[0]));
			stmt.setString(2, field[1]);
			stmt.setInt(3, Integer.parseInt(field[2]));
			stmt.setLong(4, Long.parseLong(field[3]));
			stmt.setLong(5, Long.parseLong(field[4]));
			stmt.setNull(6, java.sql.Types.VARCHAR);
			stmt.setString(7, UuidT3Generator.fromSNOMED(Long.parseLong(field[0])).toString());
			stmt.executeUpdate();
			if (line_i % 10000 == 0) {
				LOGGER.fine("Line: " + line_i);
				progress_monitor.subTask("Concepts " + line_i);
			}
		}
		conn.commit();
		stmt.close();
		LOGGER.info("Processed lines: " + line_i);
		LOGGER.info("Loaded concepts");
	}

	public void loadDescriptions() throws Exception {
		String data_dir = config.getProperty(DATA_DIR_KEY, LOGGER);
		String file = config.getProperty(DESCRIPTION_FILE_KEY, LOGGER);
		file = data_dir + file;
		String query = "";
		query += "INSERT INTO description ";
		query += " (id, effective_time, active, module_id, ";
		query += "  concept_id, language_code, type_id, ";
		query += "  term, case_significance_id )";
		query += " VALUES (?, ?, ?, ?, ?, ?, ?, ?, ?)";
		PreparedStatement stmt = conn.prepareStatement(query);
		BufferedReader in = IO.createBufferedUtf8FileReader(file);
		String line;
		int line_i = 0;
		while ((line = in.readLine()) != null) {
			line_i++;
			if (line_i == 1)
				continue;
			String[] field = line.split("\\t");
			stmt.setLong(1, Long.parseLong(field[0]));
			stmt.setString(2, field[1]);
			stmt.setInt(3, Integer.parseInt(field[2]));
			stmt.setLong(4, Long.parseLong(field[3]));
			stmt.setLong(5, Long.parseLong(field[4]));
			stmt.setString(6, field[5]);
			stmt.setLong(7, Long.parseLong(field[6]));
			stmt.setString(8, field[7]);
			stmt.setLong(9, Long.parseLong(field[8]));
			stmt.executeUpdate();
			if (line_i % 10000 == 0) {
				LOGGER.fine("Line: " + line_i);
				progress_monitor.subTask("Descriptions " + line_i);
			}
		}
		conn.commit();
		stmt.close();
		LOGGER.info("Processed lines: " + line_i);
		LOGGER.info("Loaded descriptions");
		setFsn();
	}

	protected void setFsn() throws Exception {
		String query_fsn = "";
		query_fsn += "SELECT " + DescriptionRow.COLUMNS;
		query_fsn += "  FROM description ";
		query_fsn += " WHERE active = 1 ";
		query_fsn += "   AND type_id = " + DescriptionType.FULLY_SPECIFIED.id;
		Statement stmt_fsn = this.conn.createStatement();
		String update_fsn = "";
		update_fsn += "UPDATE concept ";
		update_fsn += "   SET fsn = ? ";
		update_fsn += " WHERE id = ? ";
		PreparedStatement stmt_update_fsn = this.conn.prepareStatement(update_fsn);
		ResultSet res_fsn = stmt_fsn.executeQuery(query_fsn);
		while (res_fsn.next()) {
			DescriptionRow row_fsn = new DescriptionRow(res_fsn);
			stmt_update_fsn.setString(1, row_fsn.term);
			stmt_update_fsn.setLong(2, row_fsn.concept_id);
			stmt_update_fsn.executeUpdate();
		}
		conn.commit();
		res_fsn.close();
		stmt_fsn.close();
		stmt_update_fsn.close();
		LOGGER.info("Set FSN");
	}

	public void loadRelationships() throws Exception {
		loadRelationships(false);
	}

	public void loadStatedRelationships() throws Exception {
		loadRelationships(true);
	}

	private void loadRelationships(boolean stated_p) throws Exception {
		String data_dir = config.getProperty(DATA_DIR_KEY, LOGGER);
		String file = config.getProperty((stated_p ? STATED_RELATIONSHIP_FILE_KEY : RELATIONSHIP_FILE_KEY), LOGGER);
		file = data_dir + file;
		String table = (stated_p ? "stated_" : "") + "relationship ";
		String query = "";
		query += "INSERT INTO " + table;
		query += " ( " + RelationshipRow.COLUMNS + " )";
		query += " VALUES (?, ?, ?, ?, ?, ?, ?, ?, ?, ?)";
		PreparedStatement stmt = conn.prepareStatement(query);
		String query_del = "";
		query_del += "DELETE FROM " + table + " ";
		query_del += " WHERE " + RelationshipRow.KEY + " = ? ";
		PreparedStatement stmt_del = this.conn.prepareStatement(query_del);
		BufferedReader in = IO.createBufferedUtf8FileReader(file);
		String line;
		int line_i = 0;
		while ((line = in.readLine()) != null) {
			line_i++;
			if (line_i == 1)
				continue;
			String[] field = line.split("\\t");
			long id = Long.parseLong(field[0]);
			if (ext_p && !stated_p) {
				stmt_del.setLong(1, id);
				int del_cnt = stmt_del.executeUpdate();
				if (del_cnt > 0)
					LOGGER.warning("Replaced relationship with " + line);
			}
			stmt.setLong(1, Long.parseLong(field[0]));
			stmt.setString(2, field[1]);
			stmt.setInt(3, Integer.parseInt(field[2]));
			stmt.setLong(4, Long.parseLong(field[3]));
			stmt.setLong(5, Long.parseLong(field[4]));
			stmt.setLong(6, Long.parseLong(field[5]));
			stmt.setInt(7, Integer.parseInt(field[6]));
			stmt.setLong(8, Long.parseLong(field[7]));
			stmt.setLong(9, Long.parseLong(field[8]));
			stmt.setLong(10, Long.parseLong(field[9]));
			try {
				stmt.executeUpdate();
			} catch (SQLException ex) {
				LOGGER.severe(line);
				LOGGER.severe(ex.getMessage());
				LOGGER.info(Snomed.getItemId(id) + " " + Snomed.getNamespaceId(id) + " " + Snomed.getPartitionId(id));
			}
			if (line_i % 10000 == 0) {
				LOGGER.fine("Line: " + line_i);
				progress_monitor.subTask("Relationships " + line_i);
			}
		}
		conn.commit();
		stmt.close();
		stmt_del.close();
		LOGGER.info("Processed lines: " + line_i);
		LOGGER.info("Loaded relationships");
	}

	public void buildIsaTable() throws Exception {
		Sql.dropTable(conn, "isa");
		//
		String query;
		//
		query = "";
		query += "concept_id1 BIGINT , ";
		query += "concept_id2 BIGINT   ";
		Sql.createTable(conn, "isa", query);
		//
		query = "";
		query += "INSERT INTO isa ";
		query += "(concept_id1, concept_id2) ";
		query += "SELECT DISTINCT source_id, destination_id ";
		query += "  FROM relationship ";
		query += " WHERE type_id = " + Snomed.ISA_ID + " ";
		query += "   AND active = 1 ";
		Statement stmt = this.conn.createStatement();
		stmt.executeUpdate(query);
		stmt.close();
		//
		Sql.createIndex(conn, "isa", "concept_id1");
		Sql.createIndex(conn, "isa", "concept_id2");
		this.conn.commit();
		LOGGER.info("Created isa");
	}

	private void exportTable(String table_name, String file_name) throws Exception {
		PrintWriter out = IO.createPrintUtf8FileWriter(file_name);
		String query;
		query = "";
		query += "SELECT * FROM " + table_name;
		Statement stmt = conn.createStatement();
		ResultSet res = stmt.executeQuery(query);
		ResultSetMetaData resmd = res.getMetaData();
		int cols = resmd.getColumnCount();
		for (int i = 1; i <= cols; i++) {
			out.print(resmd.getColumnName(i));
			if (i != cols) {
				out.print("\t");
			}
		}
		out.println();
		int count = 0;
		int ext_count = 0;
		while (res.next()) {
			count++;
			long id = res.getLong(1);
			if (!Snomed.isExtensionId(id))
				continue;
			ext_count++;
			for (int i = 1; i <= cols; i++) {
				out.print(res.getObject(i));
				if (i != cols) {
					out.print("\t");
				}
			}
			out.println();
		}
		res.close();
		stmt.close();
		out.close();
		LOGGER.info("Processed: " + ext_count + " of " + count);
	}

	public void exportExtensions() throws Exception {
		String data_dir = config.getProperty(DATA_DIR_KEY, LOGGER);
		String file;
		file = config.getProperty(CONCEPT_FILE_KEY, LOGGER);
		exportTable("concept", data_dir + file);
		file = config.getProperty(DESCRIPTION_FILE_KEY, LOGGER);
		exportTable("description", data_dir + file);
		file = config.getProperty(RELATIONSHIP_FILE_KEY, LOGGER);
		exportTable("relationship", data_dir + file);
	}

	@Deprecated
	/*
	 * Use setExtension(true) and call run()
	 */
	public void loadExtension() throws Exception {
		this.init();
		ext_p = true;
		this.loadConcepts();
		this.loadDescriptions();
		this.loadRelationships();
		this.loadStatedRelationships();
		ext_p = false;
		this.buildIsaTable();
		this.shutdown();
	}

	public void run() throws Exception {
		if (progress_monitor == null)
			progress_monitor = new BasicProgressMonitorNoOp();
		progress_monitor.beginTask("Snomed Loader", 100);
		this.init();
		progress_monitor.worked(1);
		if (!ext_p)
			this.createSchema();
		progress_monitor.worked(1);
		this.loadConcepts();
		progress_monitor.worked(9);
		this.loadDescriptions();
		progress_monitor.worked(16);
		this.loadRelationships();
		progress_monitor.worked(17);
		this.loadStatedRelationships();
		progress_monitor.worked(39);
		progress_monitor.subTask("Indexes");
		if (!ext_p)
			this.createIndexes();
		progress_monitor.worked(11);
		this.buildIsaTable();
		progress_monitor.worked(5);
		//
		{
			String data_dir = config.getProperty(REFSET_DATA_DIR_KEY, LOGGER);
			String file = config.getProperty(REFSET_SIMPLE_FILE_KEY, LOGGER);
			if (data_dir != null && file != null) {
				file = data_dir + file;
				SimpleRefsetTable.LOGGER = LOGGER;
				SimpleRefsetTable refset_table = new SimpleRefsetTable(conn);
				refset_table.load(file);
				conn.commit();
				LOGGER.info("Loaded simple refset");
			} else {
				LOGGER.info("No simple refset to load");
			}
		}
		//
		this.shutdown();
		progress_monitor.done();
	}

	/**
	 * Set up logging, call the constructor, and run
	 * 
	 * <p>
	 * Args: <br>
	 * config_file
	 */
	public static void main(String[] args) {
		try {
			String log_dir = BasicLogger.getLogDir(args);
			if (log_dir != null) {
				SnomedLoader.LOGGER = BasicLogger.init(SnomedLoader.class, true, log_dir);
			} else {
				SnomedLoader.LOGGER = BasicLogger.init(SnomedLoader.class, true, true);
			}
			Sql.LOGGER = SnomedLoader.LOGGER;
			String config_name = Config.getConfigName(args, LOGGER);
			SnomedLoader m = new SnomedLoader(config_name);
			if (Arrays.asList(args).contains("-extension")) {
				LOGGER.info("Loading as extension");
				m.setExtension(true);
			}
			m.run();
		} catch (Exception ex) {
			BasicLogger.log(ex, LOGGER);
		} finally {
			BasicLogger.end(LOGGER);
		}
	}

}
