/**
 * Copyright 2007-2017 Mays Systems LLC.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *       http://www.apache.org/licenses/LICENSE-2.0
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package com.mays.snomed;

/**
 * General SNOMED CT resources
 * 
 * @author Eric Mays
 */
public class Snomed {

	/**
	 * The concept id of the SNOMED CT concept. That is the root of the isa.
	 */
	public static final long ROOT_ID = 138875005;

	/**
	 * Relationship id for the isa rel
	 */
	public static final long ISA_ID = 116680003;

	/**
	 * The concept id for the root of the ianctive concepts
	 */
	public static final long INACTIVE_CONCEPT_ID = 362955004;

	// SNOMED CT universally unique identifier (core metadata concept)

	public static final long SNOMED_UUID_ID = 900000000000002006l;

	public enum DefinitionStatus {
		// 900000000000073002 |Sufficiently defined concept definition status (core metadata concept)|
		DEFINED(900000000000073002l, "Defined"),
		// 900000000000074008 |Necessary but not sufficient concept definition status (core metadata concept)|
		PRIMITIVE(900000000000074008l, "Primitive");

		public long id;

		String name;

		private DefinitionStatus(long id, String name) {
			this.id = id;
			this.name = name;
		}

		public static DefinitionStatus getDefinitionStatus(long type) {
			for (DefinitionStatus t : values()) {
				if (t.id == type)
					return t;
			}
			return null;
		}

		public static DefinitionStatus getDefinitionStatus(String type) {
			for (DefinitionStatus t : values()) {
				if (t.name.equals(type))
					return t;
			}
			return null;
		}
	}

	/**
	 * Convert from boolean to a display string
	 */
	public static String decodePrimitive(boolean primitive) {
		if (primitive)
			return "Primitive";
		return "Defined";
	}

	public enum CaseSensitive {
		CASE_SENSITIVE(900000000000017005l, "Case sensitive"),
		//
		CASE_INSENSITIVE_INITIAL(900000000000020002l, "Only initial character case insensitive"),
		//
		CASE_INSENSITIVE(900000000000448009l, "Case insensitive");

		public long id;

		public String name;

		private CaseSensitive(long id, String name) {
			this.id = id;
			this.name = name;
		}

		public static CaseSensitive getCaseSensitive(long type) {
			for (CaseSensitive t : values()) {
				if (t.id == type)
					return t;
			}
			return null;
		}

		public static CaseSensitive getCaseSensitive(String type) {
			for (CaseSensitive t : values()) {
				if (t.name.equals(type))
					return t;
			}
			return null;
		}
	}

	public enum DescriptionType {

		FULLY_SPECIFIED(900000000000003001l, "Fully specified name"),

		// PREFERRED(1, "Preferred"),

		SYNONYM(900000000000013009l, "Synonym"),

		DEFINITION(900000000000550004l, "Definition");

		public long id;

		public String name;

		private DescriptionType(long id, String name) {
			this.id = id;
			this.name = name;
		}

		public static DescriptionType getDescriptionType(long type) {
			for (DescriptionType t : values()) {
				if (t.id == type)
					return t;
			}
			return null;
		}

		public static DescriptionType getDescriptionType(String type) {
			for (DescriptionType t : values()) {
				if (t.name.equals(type))
					return t;
			}
			return null;
		}

	}

	public enum CharacteristicType {
		DEFINING(900000000000006009l, "Defining"),
		// This relationship represents a defining characteristic of the
		// source concept. Hierarchical relationships (e.g. "ISA") are also
		// regarded as defining relationships
		INFERRED(900000000000011006l, "Inferred"),
		//
		STATED(900000000000010007l, "Stated"),
		//
		QUALIFYING(900000000000225001l, "Qualifying"),
		// This relationship represents an optional qualifying
		// characteristic.
		ADDITIONAL(900000000000227009l, "Additional")
		// This relationship represents a context specific characteristic.
		// This is used to convey characteristics of a concept that apply at
		// a particular time within a particular organization but which are
		// not intrinsic to the concept.
		;

		public long id;

		public String name;

		private CharacteristicType(long id, String name) {
			this.id = id;
			this.name = name;
		}

		public static CharacteristicType getCharacteristicType(long id) {
			for (CharacteristicType c : CharacteristicType.values()) {
				if (c.id == id)
					return c;
			}
			return null;
		}

		public static CharacteristicType getCharacteristicType(String type) {
			for (CharacteristicType t : values()) {
				if (t.name.equals(type))
					return t;
			}
			return null;
		}

	}

	public static boolean isStatusActive(boolean active) {
		return active;
	}

	/**
	 * Trim the parenthetical. Useful for indexing.
	 */
	public static String trimFsn(String str) {
		str = str.trim();
		if (str.endsWith(")")) {
			int pos = str.lastIndexOf("(");
			if (pos > 0)
				str = str.substring(0, pos);
		}
		str = str.trim();
		return str;
	}

	public static String getSemanticTag(String str) {
		str = str.trim();
		if (str.endsWith(")")) {
			int pos = str.lastIndexOf("(");
			if (pos > 0)
				str = str.substring(pos + 1, str.length() - 1);
		}
		str = str.trim();
		return str;
	}

	/**
	 * Entity type for id computation
	 */
	public static final int CONCEPT_ENTITY = 0;

	/**
	 * Entity type for id computation
	 */
	public static final int DESCRIPTION_ENTITY = 1;

	/**
	 * Entity type for id computation
	 */
	public static final int RELATIONSHIP_ENTITY = 2;

	/**
	 * Entity type for id computation
	 */
	public static final int SUBSET_ENTITY = 3;

	/**
	 * Entity type for id computation
	 */
	public static final int CROSS_MAP_SET_ENTITY = 4;

	/**
	 * Entity type for id computation
	 */
	public static final int CROSS_MAP_TARGET_ENTITY = 5;

	/**
	 * Compute the SCTID
	 * 
	 * @param item_id
	 *            the id of the concept, description, etc.
	 * 
	 * @param namespace_id
	 * 
	 * @param partition_entity_id
	 *            use one of the static ids depending on the item type
	 * 
	 * @param extension_p
	 *            true iff namespace is an extension
	 * 
	 * @return the SCTID
	 */
	public static long createSctId(long item_id, int namespace_id, int partition_entity_id, boolean extension_p) {
		// Check digit 1
		// Partition id 2
		// Namespace id 7
		// Item id 8
		long id = 0;
		if (extension_p) {
			id = item_id * 10000000000l;
			id = id + (namespace_id * 1000);
		} else {
			id = item_id * 1000;
		}
		if (extension_p)
			partition_entity_id = partition_entity_id + 10;
		id = id + (partition_entity_id * 10);
		String id_str = String.valueOf(id / 10);
		id = id + CheckDihedral.compute(id_str);
		return id;
	}

	public static boolean isExtensionId(long id) {
		long partition_entity_id = (id / 10) % 100;
		return (partition_entity_id >= 10);
	}

	public static long getPartitionId(long id) {
		return (id / 10) % 100;
	}

	public static long getItemId(long id) {
		if (isExtensionId(id))
			return id / 10000000000l;
		return id / 1000;
	}

	public static long getNamespaceId(long id) {
		if (isExtensionId(id))
			return (id / 1000) % 10000000l;
		return 0;
	}

}
