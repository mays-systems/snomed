/**
 * Copyright 2011-2017 Mays Systems LLC.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *       http://www.apache.org/licenses/LICENSE-2.0
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package com.mays.snomed;

import java.util.ArrayList;
import java.util.Collections;
import java.util.HashSet;
import java.util.Set;

public class Traverse {

	private SnomedDb snomed_db;

	private boolean sort_children_p = false;

	private Set<Long> visited = new HashSet<Long>();

	public boolean run_inactive = false;

	public Traverse(SnomedDb snomed_db) {
		super();
		this.snomed_db = snomed_db;
	}

	public boolean getSortChildrenP() {
		return sort_children_p;
	}

	public void setSortChildrenP(boolean sort_children_p) {
		this.sort_children_p = sort_children_p;
	}

	public interface Runner {

		public void run(long id) throws Exception;

	}

	public void run(Runner r) throws Exception {
		run(r, Snomed.ROOT_ID);
	}

	public void run(Runner r, long root) throws Exception {
		visited = new HashSet<Long>();
		run1(r, root);
		runInactive(r);
	}

	public void run(Runner r, long root, boolean reuse_visited_p) throws Exception {
		if (!reuse_visited_p)
			visited = new HashSet<Long>();
		run1(r, root);
		runInactive(r);
	}

	private void runInactive(Runner r) throws Exception {
		if (run_inactive) {
			for (ConceptBasic con : snomed_db.getConcepts()) {
				if (visited.contains(con.getConceptId()))
					continue;
				if (!con.getActive())
					r.run(con.getConceptId());
			}
		}
	}

	private void run1(Runner r, long id) throws Exception {
		if (visited.contains(id))
			return;
		visited.add(id);
		r.run(id);
		ArrayList<ConceptBasic> children = this.snomed_db.getConcepts(this.snomed_db.getChildren(id));
		// if (children != null) {
		if (sort_children_p)
			Collections.sort(children);
		for (ConceptBasic child : children) {
			run1(r, child.getConceptId());
		}
		// }
	}

}
