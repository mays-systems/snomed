/**
 * Copyright 2008-2017 Mays Systems LLC.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *       http://www.apache.org/licenses/LICENSE-2.0
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package com.mays.snomed.rf1;

/**
 * A basic SNOMED CT concept object. Just the items in the concept table.
 * 
 * @author Eric Mays
 */
public class ConceptBasic implements Comparable<ConceptBasic> {

	private long concept_id;

	private int concept_status;

	private String fully_specified_name;

	private String ctv3_id;

	private String snomed_id;

	private boolean is_primitive;

	public ConceptBasic(long concept_id, int concept_status,
			String fully_specified_name, String ctv3_id, String snomed_id,
			boolean is_primitive) {
		super();
		this.concept_id = concept_id;
		this.concept_status = concept_status;
		this.fully_specified_name = fully_specified_name;
		this.ctv3_id = ctv3_id;
		this.snomed_id = snomed_id;
		this.is_primitive = is_primitive;
	}

	public long getConceptId() {
		return concept_id;
	}

	public void setConceptId(long concept_id) {
		this.concept_id = concept_id;
	}

	public int getConceptStatus() {
		return concept_status;
	}

	public void setConceptStatus(int concept_status) {
		this.concept_status = concept_status;
	}

	public String getCtv3Id() {
		return ctv3_id;
	}

	public void setCtv3Id(String ctv3_id) {
		this.ctv3_id = ctv3_id;
	}

	public String getFullySpecifiedName() {
		return fully_specified_name;
	}

	public void setFullySpecifiedName(String fully_specified_name) {
		this.fully_specified_name = fully_specified_name;
	}

	public boolean getIsPrimitive() {
		return is_primitive;
	}

	public void setIsPrimitive(boolean is_primitive) {
		this.is_primitive = is_primitive;
	}

	public String getSnomedId() {
		return snomed_id;
	}

	public void setSnomedId(String snomed_id) {
		this.snomed_id = snomed_id;
	}

	/**
	 * Comparison based on the FSN
	 */
	public int compareTo(ConceptBasic that) {
		return this.getFullySpecifiedName()
				.compareToIgnoreCase(that.getFullySpecifiedName());
	}

	public String toString() {
		return this.getFullySpecifiedName();
	}

	public String getLogString() {
		return (this.getConceptId() + "\t" + this.getConceptStatus() + "\t" + this
				.getFullySpecifiedName());
	}

}
