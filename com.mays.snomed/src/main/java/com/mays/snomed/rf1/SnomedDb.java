/**
 * Copyright 2008-2017 Mays Systems LLC.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *       http://www.apache.org/licenses/LICENSE-2.0
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package com.mays.snomed.rf1;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.Collection;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;

/**
 * Simple access to SNOMED. Load all concepts into memory. Then go to the db for
 * remainder.
 * 
 * <p>
 * To use this class, call the constructor, then init with a JDBC connection.
 * 
 * <p>
 * {@code Connection conn = Sql.getConnection(derby_home, schema); } <br>
 * {@code SnomedDb snomed_db = new SnomedDb(); } <br>
 * {@code snomed_db.init(conn); } <br>
 * 
 * @author Eric Mays
 */
public class SnomedDb {

	private static final int INITIAL_CONCEPT_COUNT = 350000;

	protected Connection conn;

	private HashMap<Long, ConceptBasic> concepts;

	private HashSet<Long> concepts_with_children;

	public Connection getConnection() {
		return this.conn;
	}

	/**
	 * Loads concepts into memory
	 * 
	 * @param conn
	 *            a JDBC connection
	 */
	public void init(Connection conn) throws Exception {
		long beg = System.currentTimeMillis();
		this.conn = conn;
		this.initConceptMap();
		System.out.println("initConcepts: " + ((System.currentTimeMillis() - beg) / 1000));
		System.out.println("concepts#: " + this.concepts.size());
		this.initConceptsWithChildren();
		System.out.println("initConceptsWithChildren: " + ((System.currentTimeMillis() - beg) / 1000));
		System.out.println("concepts_with_children#: " + this.concepts_with_children.size());
	}

	/**
	 * Loads concepts into memory
	 */
	protected void initConceptMap() throws Exception {
		this.concepts = new HashMap<Long, ConceptBasic>(SnomedDb.INITIAL_CONCEPT_COUNT / 4);
		String query = "";
		query += "SELECT concept_id, concept_status, fully_specified_name, ";
		query += "       ctv3_id, snomed_id, is_primitive ";
		query += "  FROM concept ";
		Statement stmt = this.conn.createStatement();
		ResultSet res = stmt.executeQuery(query);
		while (res.next()) {
			long concept_id = res.getLong(1);
			int concept_status = res.getInt(2);
			String fully_specified_name = res.getString(3);
			String ctv3_id = res.getString(4);
			String snomed_id = res.getString(5);
			int is_primitive = res.getInt(6);
			this.concepts.put(concept_id, new ConceptBasic(concept_id, concept_status, fully_specified_name, ctv3_id,
					snomed_id, (is_primitive == 1)));
		}
		res.close();
		stmt.close();
	}

	/**
	 * Load up the cons with children
	 */
	protected void initConceptsWithChildren() throws Exception {
		this.concepts_with_children = new HashSet<Long>(SnomedDb.INITIAL_CONCEPT_COUNT);
		String query = "";
		query += "SELECT concept_id2 ";
		query += "  FROM isa ";
		// query += " FROM relationship ";
		// query += " WHERE relationship_type = " + Snomed.ISA_ID + " ";
		Statement stmt = this.conn.createStatement();
		ResultSet res = stmt.executeQuery(query);
		while (res.next()) {
			long c2 = res.getLong(1);
			this.concepts_with_children.add(c2);
		}
		res.close();
		stmt.close();
	}

	public ConceptBasic getConcept(long con) {
		return this.concepts.get(con);
	}

	public ConceptBasic getConceptByFullySpecifiedName(String fsn) throws Exception {
		ConceptBasic ret = null;
		String query = "";
		query += "SELECT concept_id ";
		query += "  FROM concept ";
		query += " WHERE fully_specified_name = ? ";
		query += "   AND concept_status IN (0, 6, 11) ";
		PreparedStatement stmt = this.conn.prepareStatement(query);
		stmt.setString(1, fsn);
		ResultSet res = stmt.executeQuery();
		while (res.next()) {
			long concept_id = res.getLong(1);
			ret = this.getConcept(concept_id);
		}
		res.close();
		stmt.close();
		return ret;
	}

	/**
	 * @param con
	 *            an sctid
	 * 
	 * @return true iff the con has children
	 */
	public boolean hasChildren(long con) {
		return this.concepts_with_children.contains(con);
	}

	public ArrayList<ConceptDescription> getDescriptions(long con) throws Exception {
		return getDescriptions(con, false);
	}

	/**
	 * @param con
	 *            an sctid
	 * 
	 * @return descriptions
	 */
	public ArrayList<ConceptDescription> getDescriptions(long con, boolean active_only) throws Exception {
		ArrayList<ConceptDescription> result = new ArrayList<ConceptDescription>();
		String query = "";
		query += "SELECT description_id, description_status, concept_id, ";
		query += "       term, initial_capital_status, description_type, ";
		query += "       language_code ";
		query += "  FROM description ";
		query += " WHERE concept_id = ? ";
		PreparedStatement stmt = this.conn.prepareStatement(query);
		stmt.setLong(1, con);
		ResultSet res = stmt.executeQuery();
		while (res.next()) {
			long description_id = res.getLong(1);
			int description_status = res.getInt(2);
			long concept_id = res.getLong(3);
			String term = res.getString(4);
			int initial_capital_status = res.getInt(5);
			int description_type = res.getInt(6);
			String language_code = res.getString(7);
			if (active_only && !Snomed.isStatusActive(description_status))
				continue;
			result.add(new ConceptDescription(description_id, description_status, concept_id, term,
					(initial_capital_status == 1), description_type, language_code));
		}
		res.close();
		stmt.close();
		return result;
	}

	public ConceptDescription getDescription(long descr) throws Exception {
		ConceptDescription result = null;
		String query = "";
		query += "SELECT description_id, description_status, concept_id, ";
		query += "       term, initial_capital_status, description_type, ";
		query += "       language_code ";
		query += "  FROM description ";
		query += " WHERE description_id = ? ";
		PreparedStatement stmt = this.conn.prepareStatement(query);
		stmt.setLong(1, descr);
		ResultSet res = stmt.executeQuery();
		while (res.next()) {
			long description_id = res.getLong(1);
			int description_status = res.getInt(2);
			long concept_id = res.getLong(3);
			String term = res.getString(4);
			int initial_capital_status = res.getInt(5);
			int description_type = res.getInt(6);
			String language_code = res.getString(7);
			result = new ConceptDescription(description_id, description_status, concept_id, term,
					(initial_capital_status == 1), description_type, language_code);
		}
		res.close();
		stmt.close();
		return result;
	}

	/**
	 * @param con
	 *            an sctid
	 * 
	 * @return relationships
	 */
	public ArrayList<ConceptRelationship> getRelationships(long con) throws Exception {
		ArrayList<ConceptRelationship> result = new ArrayList<ConceptRelationship>();
		String query = "";
		query += "SELECT relationship_id, concept_id1, relationship_type, concept_id2, ";
		query += "       characteristic_type, refinability, relationship_group ";
		query += "  FROM relationship ";
		query += " WHERE concept_id1 = ? ";
		PreparedStatement stmt = this.conn.prepareStatement(query);
		stmt.setLong(1, con);
		ResultSet res = stmt.executeQuery();
		while (res.next()) {
			long relationship_id = res.getLong(1);
			// long concept_id1 = res.getLong(2);
			long relationship_type = res.getLong(3);
			long concept_id2 = res.getLong(4);
			int characteristic_type = res.getInt(5);
			int refinability = res.getInt(6);
			int relationship_group = res.getInt(7);
			// This is here for SNODENT subsets
			String relationship_type_fsn = "<unknown>";
			try {
				relationship_type_fsn = this.getConcept(relationship_type).getFullySpecifiedName();
			} catch (Exception ex) {
			}
			result.add(new ConceptRelationship(relationship_id, relationship_type, relationship_type_fsn, concept_id2,
					this.getConcept(concept_id2).getFullySpecifiedName(), characteristic_type, refinability,
					relationship_group));
		}
		res.close();
		stmt.close();
		return result;
	}

	// TODO This is an ugly hack
	public ArrayList<ConceptRelationship> getRelationshipsTo(long con) throws Exception {
		ArrayList<ConceptRelationship> result = new ArrayList<ConceptRelationship>();
		String query = "";
		query += "SELECT relationship_id, concept_id1, relationship_type, concept_id2, ";
		query += "       characteristic_type, refinability, relationship_group ";
		query += "  FROM relationship ";
		query += " WHERE concept_id2 = ? ";
		PreparedStatement stmt = this.conn.prepareStatement(query);
		stmt.setLong(1, con);
		ResultSet res = stmt.executeQuery();
		while (res.next()) {
			long relationship_id = res.getLong(1);
			long concept_id1 = res.getLong(2);
			long relationship_type = res.getLong(3);
			// long concept_id2 = res.getLong(4);
			int characteristic_type = res.getInt(5);
			int refinability = res.getInt(6);
			int relationship_group = res.getInt(7);
			result.add(new ConceptRelationship(relationship_id, relationship_type,
					this.getConcept(relationship_type).getFullySpecifiedName(), concept_id1,
					this.getConcept(concept_id1).getFullySpecifiedName(), characteristic_type, refinability,
					relationship_group));
		}
		res.close();
		stmt.close();
		return result;
	}

	public ArrayList<ConceptRelationship> getStatedRelationships(long con) throws Exception {
		ArrayList<ConceptRelationship> result = new ArrayList<ConceptRelationship>();
		String query = "";
		query += "SELECT 0, concept_id1, relationship_type, concept_id2, ";
		query += "       characteristic_type, refinability, relationship_group ";
		query += "  FROM stated ";
		query += " WHERE concept_id1 = ? ";
		PreparedStatement stmt = this.conn.prepareStatement(query);
		stmt.setLong(1, con);
		ResultSet res = stmt.executeQuery();
		while (res.next()) {
			long relationship_id = res.getLong(1);
			// long concept_id1 = res.getLong(2);
			long relationship_type = res.getLong(3);
			long concept_id2 = res.getLong(4);
			int characteristic_type = res.getInt(5);
			int refinability = res.getInt(6);
			int relationship_group = res.getInt(7);
			result.add(new ConceptRelationship(relationship_id, relationship_type,
					this.getConcept(relationship_type).getFullySpecifiedName(), concept_id2,
					this.getConcept(concept_id2).getFullySpecifiedName(), characteristic_type, refinability,
					relationship_group));
		}
		res.close();
		stmt.close();
		return result;
	}

	/**
	 * @param con
	 *            an sctid
	 * 
	 * @return sctids of children
	 */
	public ArrayList<Long> getChildren(long con) throws Exception {
		ArrayList<Long> result = new ArrayList<Long>();
		String query = "";
		query += "SELECT concept_id1 ";
		query += "  FROM isa ";
		query += " WHERE concept_id2 = ? ";
		// query += " FROM relationship ";
		// query += " WHERE relationship_type = " + Snomed.ISA_ID + " ";
		// query += " AND concept_id2 = ? ";
		PreparedStatement stmt = this.conn.prepareStatement(query);
		stmt.setLong(1, con);
		ResultSet res = stmt.executeQuery();
		while (res.next()) {
			long concept_id1 = res.getLong(1);
			result.add(concept_id1);
		}
		res.close();
		stmt.close();
		return result;
	}

	/**
	 * @param con
	 *            an sctid
	 * 
	 * @return sctids of parents
	 */
	public ArrayList<Long> getParents(long con) throws Exception {
		ArrayList<Long> result = new ArrayList<Long>();
		String query = "";
		query += "SELECT concept_id2 ";
		query += "  FROM isa ";
		query += " WHERE concept_id1 = ? ";
		// query += " FROM relationship ";
		// query += " WHERE relationship_type = " + Snomed.ISA_ID + " ";
		// query += " AND concept_id2 = ? ";
		PreparedStatement stmt = this.conn.prepareStatement(query);
		stmt.setLong(1, con);
		ResultSet res = stmt.executeQuery();
		while (res.next()) {
			long concept_id1 = res.getLong(1);
			result.add(concept_id1);
		}
		res.close();
		stmt.close();
		return result;
	}

	/**
	 * @param con
	 *            an sctid
	 * 
	 * @return list of sibling sctids
	 * @throws Exception
	 */
	public ArrayList<Long> getSiblings(long con) throws Exception {
		ArrayList<Long> res = new ArrayList<Long>();
		for (long parent : this.getParents(con)) {
			for (long child : this.getChildren(parent)) {
				if (child == con)
					continue;
				if (res.contains(child))
					continue;
				res.add(child);
			}
		}
		return res;
	}

	/**
	 * Convenience method.
	 */
	public ArrayList<ConceptBasic> getConcepts(Collection<Long> concepts) {
		ArrayList<ConceptBasic> res = new ArrayList<ConceptBasic>(concepts.size());
		for (long con : concepts) {
			res.add(this.getConcept(con));
		}
		return res;
	}

	/**
	 * Get all concepts
	 * 
	 * @return - all the concepts
	 */
	public ArrayList<ConceptBasic> getConcepts() {
		ArrayList<ConceptBasic> res = new ArrayList<ConceptBasic>(this.concepts.values().size());
		for (ConceptBasic con : this.concepts.values()) {
			res.add(con);
		}
		return res;
	}

	public ComponentHistory getComponentHistory(long id) throws Exception {
		ComponentHistory result = new ComponentHistory(id);
		String query = "";
		query += "SELECT release_version, change_type, ";
		query += "       status, reason ";
		query += "  FROM component_history ";
		query += " WHERE component_id = ? ";
		PreparedStatement stmt = this.conn.prepareStatement(query);
		stmt.setLong(1, id);
		ResultSet res = stmt.executeQuery();
		while (res.next()) {
			int release_version = res.getInt(1);
			int change_type = res.getInt(2);
			int status = res.getInt(3);
			String reason = res.getString(4);
			result.addChange(String.valueOf(release_version), change_type, status, reason);
		}
		res.close();
		stmt.close();
		return result;
	}

	private void deleteConcept(long concept_id) throws Exception {
		deleteConceptHelper(concept_id, "concept", "concept_id");
		deleteConceptHelper(concept_id, "description", "concept_id");
		// As long as these are leaf nodes this is OK
		deleteConceptHelper(concept_id, "relationship", "concept_id1");
		deleteConceptHelper(concept_id, "isa", "concept_id1");
		this.concepts.remove(concept_id);
	}

	private void deleteConceptHelper(long concept_id, String table, String key) throws Exception {
		String query = "";
		query += "DELETE FROM " + table + " ";
		query += " WHERE " + key + " = ? ";
		PreparedStatement stmt = this.conn.prepareStatement(query);
		stmt.setLong(1, concept_id);
		stmt.executeUpdate();
	}

	private void saveConcept(ConceptEdit con) throws Exception {
		String query = "";
		query += "INSERT INTO concept ";
		query += " (concept_id, concept_status, fully_specified_name, ";
		query += "  ctv3_id, snomed_id, is_primitive) ";
		query += " VALUES (?, ?, ?, ?, ?, ?) ";
		PreparedStatement stmt = this.conn.prepareStatement(query);
		stmt.setLong(1, con.getConceptId());
		stmt.setInt(2, con.getConceptStatus());
		stmt.setString(3, con.getFullySpecifiedName());
		stmt.setString(4, con.getCtv3Id());
		stmt.setString(5, con.getSnomedId());
		stmt.setInt(6, (con.getIsPrimitive() ? 1 : 0));
		stmt.executeUpdate();
		this.concepts.put(con.getConceptId(), con);
	}

	private void saveRelationships(long concept_id, List<ConceptRelationship> rels) throws Exception {
		String query = "";
		query += "INSERT INTO relationship ";
		query += " (relationship_id, concept_id1, relationship_type, concept_id2, ";
		query += "  characteristic_type, refinability, relationship_group) ";
		query += " VALUES (?, ?, ?, ?, ?, ?, ?) ";
		PreparedStatement stmt = this.conn.prepareStatement(query);
		String query_isa = "";
		query_isa += "INSERT INTO isa ";
		query_isa += " (concept_id1, concept_id2) ";
		query_isa += " VALUES (?, ?) ";
		PreparedStatement stmt_isa = this.conn.prepareStatement(query_isa);
		for (ConceptRelationship rel : rels) {
			stmt.setLong(1, rel.getRelationshipId());
			stmt.setLong(2, concept_id);
			stmt.setLong(3, rel.getRelationshipType());
			stmt.setLong(4, rel.getConceptId2());
			stmt.setInt(5, rel.getCharacteristicType());
			stmt.setInt(6, rel.getRefinability());
			stmt.setInt(7, rel.getRelationshipGroup());
			stmt.executeUpdate();
			if (rel.getRelationshipType() == Snomed.ISA_ID) {
				stmt_isa.setLong(1, concept_id);
				stmt_isa.setLong(2, rel.getConceptId2());
				stmt_isa.executeUpdate();
			}
		}
	}

	private void saveDescriptions(long concept_id, List<ConceptDescription> descrs) throws Exception {
		String query = "";
		query += "INSERT INTO description ";
		query += " (description_id, description_status, concept_id, ";
		query += "  term, initial_capital_status, description_type, ";
		query += "  language_code) ";
		query += " VALUES (?, ?, ?, ?, ?, ?, ?) ";
		PreparedStatement stmt = this.conn.prepareStatement(query);
		for (ConceptDescription descr : descrs) {
			stmt.setLong(1, descr.getDescriptionId());
			stmt.setInt(2, descr.getDescriptionStatus());
			stmt.setLong(3, concept_id);
			stmt.setString(4, descr.getTerm());
			stmt.setInt(5, (descr.getInitialCapitalStatus() ? 1 : 0));
			stmt.setInt(6, descr.getDescriptionType());
			stmt.setString(7, descr.getLanguageCode());
			stmt.executeUpdate();
		}
	}

	public void saveConcept(ConceptEdit con, boolean commit_p) throws Exception {
		deleteConcept(con.getConceptId());
		saveConcept(con);
		saveRelationships(con.getConceptId(), con.getRelationships());
		saveDescriptions(con.getConceptId(), con.getDescriptions());
		if (commit_p)
			conn.commit();
	}

	public void fetchConcept(ConceptEdit con) throws Exception {
		con.setDescriptions(this.getDescriptions(con.getConceptId()));
		con.setRelationships(this.getRelationships(con.getConceptId()));
	}

}
