/**
 * Copyright 2008-2017 Mays Systems LLC.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *       http://www.apache.org/licenses/LICENSE-2.0
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package com.mays.snomed.rf1;

import com.mays.snomed.CheckDihedral;

/**
 * General SNOMED CT resources
 * 
 * @author Eric Mays
 */
public class Snomed {

	/**
	 * The concept id of the SNOMED CT concept. That is the root of the isa.
	 */
	public static final long ROOT_ID = 138875005;

	/**
	 * Relationship id for the isa rel
	 */
	public static final long ISA_ID = 116680003;

	/**
	 * The concept id for the root of the ianctive concepts
	 */
	public static final long INACTIVE_CONCEPT_ID = 362955004;

	/**
	 * Convert from boolean to a display string
	 */
	public static String decodePrimitive(boolean primitive) {
		if (primitive)
			return "Primitive";
		return "Defined";
	}

	@Deprecated
	public static String decodeStatus(int status) {
		return decodeConceptStatus(status);
	}

	public enum ConceptStatus {

		CURRENT(0, "Current"),
		// The Concept is in current use and is considered active.
		RETIRED(1, "Retired"),
		// The Concept has been withdrawn without a specified reason. These
		// concepts are considered inactive.
		DUPLICATE(2, "Duplicate"),
		// The Concept has been withdrawn from current use because it
		// duplicates another Concept. These concepts are considered
		// inactive.
		OUTDATED(3, "Outdated"),
		// The Concept has been withdrawn from current use because it is no
		// longer recognized as a valid clinical concept. These concepts are
		// considered inactive.
		AMBIGUOUS(4, "Ambiguous"),
		// The Concept has been withdrawn from current use because it is
		// inherently ambiguous. These concepts are considered inactive.
		ERRONEOUS(5, "Erroneous"),
		// The Concept has been withdrawn from current use as it contains an
		// error. A corrected but otherwise similar Concept may have been
		// added to replace it. These concepts are considered inactive.
		LIMITED(6, "Limited"),
		// The Concept is of limited clinical value as it is based on a
		// classification concept or an administrative definition. Concepts
		// with this status are still valid for current use and are
		// considered active.
		MOVED_ELSEWHERE(10, "Moved elsewhere"),
		// The Concept has been moved to an extension, to a different
		// extension, or to the core. Use the �Moved To� Relationship to
		// locate the namespace to which the concept has been moved. These
		// concepts are considered inactive.
		PENDING_MOVE(11, "Pending move")
		// The Concept will be moved to an extension, to a different
		// extension, or to the core. Use the �Moved To� Relationship to
		// locate the namespace to which the concept will be moved when the
		// recipient organization confirms the move. These concepts are
		// considered active.
		;

		public static final String UNDEF = "Undefined concept status value";

		public int id;

		public String name;

		private ConceptStatus(int id, String name) {
			this.id = id;
			this.name = name;
		}

		public static ConceptStatus getConceptStatus(int id) {
			for (ConceptStatus s : ConceptStatus.values()) {
				if (s.id == id)
					return s;
			}
			return null;
		}

		public static ConceptStatus getConceptStatus(String status) {
			for (ConceptStatus t : values()) {
				if (t.name.equals(status))
					return t;
			}
			return null;
		}

	}

	/**
	 * Go from the numeric status to a display string
	 * 
	 * @param status
	 *            the concept status
	 * 
	 * @return a display string for the status
	 */
	public static String decodeConceptStatus(int status) {
		ConceptStatus s = ConceptStatus.getConceptStatus(status);
		if (s != null)
			return s.name;
		return ConceptStatus.UNDEF;
	}

	public static int encodeConceptStatus(String status) {
		ConceptStatus t = ConceptStatus.getConceptStatus(status);
		if (t != null)
			return t.id;
		return -1;
	}

	public enum DescriptionStatus {

		CURRENT(0, "Current"),
		// The Description and its associated Concept are in current use.
		NON_CURRENT(1, "Non-Current"),
		// The Description has been withdrawn without a specified reason.
		DUPLICATE(2, "Duplicate"),
		// The Description has been withdrawn from current use because it
		// duplicates another description containing the same term (or a
		// very similar term) associated with the same Concept.
		OUTDATED(3, "Outdated"),
		// The Description has been withdrawn from current use because this
		// Term is no longer in general clinical use as a label for the
		// associated Concept.
		ERRONEOUS(5, "Erroneous"),
		// The Description has been withdrawn as the Term contains errors.
		LIMITED(6, "Limited"),
		// The Description is a valid Description of a Concept which has
		// "limited" status (i.e. the Concept has ConceptStatus = 6).
		INAPPROPRIATE(7, "Inappropriate"),
		// The Description has been withdrawn as the Term should not refer
		// to this concept.
		CONCEPT_NON_CURRENT(8, "Concept non-current"),
		// The Description is a valid Description of a Concept which has
		// been made non-current (i.e. the Concept has ConceptStatus 1, 2,
		// 3, 4, 5, or 10).
		MOVED_ELSEWHERE(10, "Moved elsewhere"),
		// The Description has been moved to an extension, to a different
		// extension, or to the core. A reference will indicate the
		// namespace to which the description has been moved.
		PENDING_MOVE(11, "Pending move")
		// The Description will be moved to an extension, to a different
		// extension, or to the core. A reference will indicate the
		// namespace to which the description has been moved when the
		// recipient organization confirms the move (Future Use).
		;
		// Status value 4 is only applicable to ConceptStatus.
		/**/
		// A Description is only marked as current (DescriptionStatus = 0) if
		// the associated Concept is also current (ConceptStatus= 0).
		/**/
		// An "inactive" Concept (ConceptStatus = 1, 2, 3, 4 5, or 10),
		// retains its previous current Descriptions but these are marked
		// "Concept non-current" (DescriptionStatus = 8).
		/**/
		// A "limited" Concept (ConceptStatus 6) has valid usable Descriptions
		// but these are also marked "limited" (DescriptionStatus = 6).

		public static final String UNDEF = "Undefined description status value";

		public int id;

		public String name;

		private DescriptionStatus(int id, String name) {
			this.id = id;
			this.name = name;
		}

		public static DescriptionStatus getDescriptionStatus(int id) {
			for (DescriptionStatus s : DescriptionStatus.values()) {
				if (s.id == id)
					return s;
			}
			return null;
		}

		public static DescriptionStatus getDescriptionStatus(String status) {
			for (DescriptionStatus t : values()) {
				if (t.name.equals(status))
					return t;
			}
			return null;
		}

	}

	/**
	 * Go from the numeric status to a display string
	 * 
	 * @param status
	 *            the description status
	 * 
	 * @return a display string for the status
	 */
	public static String decodeDescriptionStatus(int status) {
		DescriptionStatus s = DescriptionStatus.getDescriptionStatus(status);
		if (s != null)
			return s.name;
		return DescriptionStatus.UNDEF;
	}

	public static int encodeDescriptionStatus(String status) {
		DescriptionStatus t = DescriptionStatus.getDescriptionStatus(status);
		if (t != null)
			return t.id;
		return -1;
	}

	/**/
	// Each inactive concept code falls into one of these seven subclasses
	// based upon its ConceptStatus value of 1, 2, 3, 4, 5, 6, or 10. There
	// is no further subclassing of inactive concepts. Note that concept
	// codes with a ConceptStatus value of 6 (Limited) were formerly
	// considered active, but are now inactive and are included in the
	// inactive hierarchy. This also means that the former confusing
	// distinction between " active" and "current" no longer is required.
	// " Active" and "current" now mean the same thing, and "inactive" and
	// "non- current" also now mean the same thing.

	/**
	 * @param status
	 *            a concept or description status
	 * 
	 * @return true iff the status is active
	 */
	public static boolean isStatusActive(int status) {
		switch (status) {
		case 0:
			return true;
			// This changed
			// case 6:
			// return true;
		case 11:
			return true;
		}
		return false;
	}

	@Deprecated
	public static boolean isStatusActive(int status, boolean pre2010_p) {
		if (!pre2010_p)
			return isStatusActive(status);
		switch (status) {
		case 0:
			return true;
		case 6:
			return true;
		case 11:
			return true;
		}
		return false;
	}

	/**
	 * If the status is not active, just return "Inactive"
	 */
	public static String decodeStatusSimple(int status) {
		if (isStatusActive(status))
			return decodeStatus(status);
		return "Inactive";
	}

	/**
	 * Get an abbreviated status, just one char
	 */
	public static String decodeStatusSimple(int status, boolean abbreviation) {
		if (abbreviation)
			return decodeStatusSimple(status).substring(0, 1);
		return decodeStatusSimple(status);
	}

	public enum DescriptionType {
		/*
		 * public final int compareTo(E o) Compares this enum with the specified
		 * object for order. Returns a negative integer, zero, or a positive
		 * integer as this object is less than, equal to, or greater than the
		 * specified object. Enum constants are only comparable to other enum
		 * constants of the same enum type. The natural order implemented by
		 * this method is the order in which the constants are declared.
		 */
		FULLY_SPECIFIED(3, "Fully Specified"),

		PREFERRED(1, "Preferred"),

		SYNONYM(2, "Synonym"),

		UNSPECIFIED(0, "Unspecified");
		// This may be assigned as either a Preferred Term or Synonym by a
		// Description Subset for a language, dialect or realm

		public static final String UNDEF = "Undefined description type value";

		public int id;

		public String name;

		private DescriptionType(int id, String name) {
			this.id = id;
			this.name = name;
		}

		public static DescriptionType getDescriptionType(int type) {
			for (DescriptionType t : values()) {
				if (t.id == type)
					return t;
			}
			return null;
		}

		public static DescriptionType getDescriptionType(String type) {
			for (DescriptionType t : values()) {
				if (t.name.equals(type))
					return t;
			}
			return null;
		}

	}

	/**
	 * Go from the numeric type to a display string
	 * 
	 * @param type
	 *            the description type
	 * 
	 * @return a display string for the type
	 */
	public static String decodeDescriptionType(int type) {
		DescriptionType t = DescriptionType.getDescriptionType(type);
		if (t != null)
			return t.name;
		return DescriptionType.UNDEF;
	}

	@Deprecated
	public static DescriptionType getDescriptionType(int type) {
		return DescriptionType.getDescriptionType(type);
	}

	public static int encodeDescriptionType(String type) {
		DescriptionType t = DescriptionType.getDescriptionType(type);
		if (t != null)
			return t.id;
		return -1;
	}

	/**
	 * @param type
	 *            the description type
	 * 
	 * @return true iff the type is preferred
	 */
	public static boolean isDescriptionTypePreferred(int type) {
		return (type == 1);
	}

	/**
	 * @param type
	 *            the description type
	 * 
	 * @return true iff the type is fully specified
	 */
	public static boolean isDescriptionTypeFullySpecified(int type) {
		return (type == 3);
	}

	public enum CharacteristicType {
		DEFINING(0, "Defining"),
		// This relationship represents a defining characteristic of the
		// source concept. Hierarchical relationships (e.g. "ISA") are also
		// regarded as defining relationships
		QUALIFIER(1, "Qualifier"),
		// This relationship represents an optional qualifying
		// characteristic.
		HISTORICAL(2, "Historical"),
		// This is used to relate an inactive concept to another concept.
		ADDITIONAL(3, "Additional")
		// This relationship represents a context specific characteristic.
		// This is used to convey characteristics of a concept that apply at
		// a particular time within a particular organization but which are
		// not intrinsic to the concept.
		;

		public static final String UNDEF = "Undefined characteristic type value";

		public int id;

		public String name;

		private CharacteristicType(int id, String name) {
			this.id = id;
			this.name = name;
		}

		public static CharacteristicType getCharacteristicType(int id) {
			for (CharacteristicType c : CharacteristicType.values()) {
				if (c.id == id)
					return c;
			}
			return null;
		}

		public static CharacteristicType getCharacteristicType(String type) {
			for (CharacteristicType t : values()) {
				if (t.name.equals(type))
					return t;
			}
			return null;
		}

	}

	/**
	 * Go from the numeric type to a display string
	 * 
	 * @param type
	 *            the characteristic type
	 * 
	 * @return a display string for the type
	 */
	public static String decodeCharacteristicType(int type) {
		CharacteristicType t = CharacteristicType.getCharacteristicType(type);
		if (t != null)
			return t.name;
		return CharacteristicType.UNDEF;
	}

	@Deprecated
	public static CharacteristicType getCharacteristicType(int type) {
		return CharacteristicType.getCharacteristicType(type);
	}

	public static int encodeCharacteristicType(String type) {
		CharacteristicType t = CharacteristicType.getCharacteristicType(type);
		if (t != null)
			return t.id;
		return -1;
	}

	/**
	 * Trim the parenthetical. Useful for indexing.
	 */
	public static String trimFsn(String str) {
		str = str.trim();
		if (str.endsWith(")")) {
			int pos = str.lastIndexOf("(");
			if (pos > 0)
				str = str.substring(0, pos);
		}
		str = str.trim();
		return str;
	}

	public static String getSemanticTag(String str) {
		str = str.trim();
		if (str.endsWith(")")) {
			int pos = str.lastIndexOf("(");
			if (pos > 0)
				str = str.substring(pos + 1, str.length() - 1);
		}
		str = str.trim();
		return str;
	}

	/**
	 * Entity type for id computation
	 */
	public static final int CONCEPT_ENTITY = 0;

	/**
	 * Entity type for id computation
	 */
	public static final int DESCRIPTION_ENTITY = 1;

	/**
	 * Entity type for id computation
	 */
	public static final int RELATIONSHIP_ENTITY = 2;

	/**
	 * Entity type for id computation
	 */
	public static final int SUBSET_ENTITY = 3;

	/**
	 * Entity type for id computation
	 */
	public static final int CROSS_MAP_SET_ENTITY = 4;

	/**
	 * Entity type for id computation
	 */
	public static final int CROSS_MAP_TARGET_ENTITY = 5;

	/**
	 * Compute the SCTID
	 * 
	 * @param item_id
	 *            the id of the concept, description, etc.
	 * 
	 * @param namespace_id
	 * 
	 * @param partition_entity_id
	 *            use one of the static ids depending on the item type
	 * 
	 * @param extension_p
	 *            true iff namespace is an extension
	 * 
	 * @return the SCTID
	 */
	public static long createSctId(long item_id, int namespace_id,
			int partition_entity_id, boolean extension_p) {
		// Check digit 1
		// Partition id 2
		// Namespace id 7
		// Item id 8
		long id = 0;
		if (extension_p) {
			id = item_id * 10000000000l;
			id = id + (namespace_id * 1000);
		} else {
			id = item_id * 1000;
		}
		if (extension_p)
			partition_entity_id = partition_entity_id + 10;
		id = id + (partition_entity_id * 10);
		String id_str = String.valueOf(id / 10);
		id = id + CheckDihedral.compute(id_str);
		return id;
	}

	public static boolean isExtensionId(long id) {
		long partition_entity_id = (id / 10) % 100;
		return (partition_entity_id >= 10);
	}

	public static long getPartitionId(long id) {
		return (id / 10) % 100;
	}

	public static long getItemId(long id) {
		if (isExtensionId(id))
			return id / 10000000000l;
		return id / 1000;
	}

	public static long getNamespaceId(long id) {
		if (isExtensionId(id))
			return (id / 1000) % 10000000l;
		return 0;
	}

}
