/**
 * Copyright 2008-2017 Mays Systems LLC.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *       http://www.apache.org/licenses/LICENSE-2.0
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package com.mays.snomed.rf1;

import java.util.ArrayList;

/**
 * @author emays
 * 
 */
public class ConceptEdit extends ConceptBasic {

	private ArrayList<ConceptDescription> descriptions;

	private ArrayList<ConceptRelationship> relationships;

	public ConceptEdit(long concept_id, int concept_status,
			String fully_specified_name, String ctv3_id, String snomed_id,
			boolean is_primitive) {
		super(concept_id, concept_status, fully_specified_name, ctv3_id,
				snomed_id, is_primitive);
		this.descriptions = new ArrayList<ConceptDescription>();
		this.relationships = new ArrayList<ConceptRelationship>();
	}

	public ConceptEdit(ConceptBasic con) {
		this(con.getConceptId(), con.getConceptStatus(), con
				.getFullySpecifiedName(), con.getCtv3Id(), con.getSnomedId(),
				con.getIsPrimitive());
	}

	public ArrayList<ConceptDescription> getDescriptions() {
		return this.descriptions;
	}

	public void setDescriptions(ArrayList<ConceptDescription> descriptions) {
		this.descriptions = descriptions;
	}

	public void addDescription(ConceptDescription description) {
		this.descriptions.add(description);
	}

	public ArrayList<ConceptRelationship> getRelationships() {
		return this.relationships;
	}

	public void setRelationships(ArrayList<ConceptRelationship> relationships) {
		this.relationships = relationships;
	}

	public void addRelationship(ConceptRelationship relationship) {
		this.relationships.add(relationship);
	}

}
