/**
 * Copyright 2008-2017 Mays Systems LLC.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *       http://www.apache.org/licenses/LICENSE-2.0
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package com.mays.snomed.rf1;

import java.io.BufferedReader;
import java.io.PrintWriter;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.ResultSetMetaData;
import java.sql.Statement;
import java.util.logging.Logger;

import com.mays.util.BasicLogger;
import com.mays.util.BasicProgressMonitor;
import com.mays.util.BasicProgressMonitorNoOp;
import com.mays.util.Config;
import com.mays.util.IO;
import com.mays.util.Sql;

/**
 * Load SNOMED from the standard distribution into an Apache Derby db.
 * 
 * <p>
 * Core tables only, concepts, relationships, descriptions
 * 
 * <p>
 * Need to provide a configuration file
 * 
 * <br>
 * derby home - the directory to store the Derby db
 * 
 * <br>
 * schema - the Derby schema name
 * 
 * <br>
 * data_dir - the directory containing the data files
 * 
 * <br>
 * concept_file = the concepts file
 * 
 * <br>
 * description_file = the descriptions file
 * 
 * <br>
 * relationship_file = the relationships file
 * 
 * <br>
 * canonical_file = the canonical file (not used here)
 * 
 * <p>
 * Sample SnomedLoader.properties file:
 * 
 * <br>
 * derby_home = c:/ddb
 * 
 * <br>
 * schema = snomed
 * 
 * <br>
 * data_dir = ../../data/snomed/20080131/
 * 
 * <br>
 * concept_file = sct_concepts_20080131.txt
 * 
 * <br>
 * description_file = sct_descriptions_20080131.txt
 * 
 * <br>
 * relationship_file = sct_relationships_20080131.txt
 * 
 * <br>
 * canonical_file = sct_canonical_20080131.txt
 * 
 * @author Eric Mays
 * 
 */
public class SnomedLoader {

	public final static String DATA_DIR_KEY = "data_dir";

	public final static String CONCEPT_FILE_KEY = "concept_file";

	public final static String DESCRIPTION_FILE_KEY = "description_file";

	public final static String RELATIONSHIP_FILE_KEY = "relationship_file";

	public final static String COMPONENT_HISTORY_FILE_KEY = "component_history_file";

	public static Logger LOGGER;

	protected Config config;

	protected Connection conn;

	protected BasicProgressMonitor progress_monitor;

	public SnomedLoader(String config_name) throws Exception {
		super();
		this.config = Config.load(config_name, LOGGER);
	}

	public SnomedLoader(Config config) throws Exception {
		super();
		this.config = config;
	}

	/**
	 * @param mon
	 */
	public void setProgressMonitor(BasicProgressMonitor progress_monitor) {
		this.progress_monitor = progress_monitor;
	}

	/**
	 * Get the JDBC connection
	 */
	public void init() throws Exception {
		String derby_home = config.getProperty(Sql.DERBY_HOME_KEY, LOGGER);
		String schema = config.getProperty(Sql.SCHEMA_KEY, LOGGER);
		this.init(Sql.getConnection(derby_home, schema, true));
	}

	public void init(Connection conn) throws Exception {
		this.conn = conn;
	}

	public void shutdown() throws Exception {
		conn.close();
		Sql.shutdown(config.getProperty(Sql.SCHEMA_KEY, LOGGER));
	}

	public void createSchema() throws Exception {
		String query;
		//
		Sql.dropTable(conn, "concept");
		query = "";
		query += "concept_id BIGINT PRIMARY KEY, ";
		query += "concept_status INT, ";
		query += "fully_specified_name VARCHAR(255), ";
		query += "ctv3_id VARCHAR(255), "; // Extend this for the SNODENT
		query += "snomed_id VARCHAR(8), ";
		query += "is_primitive INT ";
		Sql.createTable(conn, "concept", query);
		//
		Sql.dropTable(conn, "description");
		query = "";
		query += "description_id BIGINT PRIMARY KEY, ";
		query += "description_status INT, ";
		query += "concept_id BIGINT , ";
		query += "term VARCHAR(255), ";
		query += "initial_capital_status INT, ";
		query += "description_type INT, ";
		query += "language_code VARCHAR(8) ";
		Sql.createTable(conn, "description", query);
		//
		Sql.dropTable(conn, "relationship");
		query = "";
		query += "relationship_id BIGINT PRIMARY KEY, ";
		query += "concept_id1 BIGINT , ";
		query += "relationship_type BIGINT , ";
		query += "concept_id2 BIGINT , ";
		query += "characteristic_type INT, ";
		query += "refinability INT, ";
		query += "relationship_group INT ";
		Sql.createTable(conn, "relationship", query);
		//
		Sql.dropTable(conn, "component_history");
		query = "";
		query += "component_id BIGINT, ";
		query += "release_version INT, ";
		query += "change_type INT, ";
		query += "status INT, ";
		query += "reason VARCHAR(255) ";
		Sql.createTable(conn, "component_history", query);
		conn.commit();
		LOGGER.info("Created schema");
	}

	public void createIndexes() throws Exception {
		Sql.createIndex(conn, "description", "concept_id");
		Sql.createIndex(conn, "relationship", "concept_id1");
		Sql.createIndex(conn, "relationship", "concept_id2");
		Sql.createIndex(conn, "component_history", "component_id");
		this.conn.commit();
		LOGGER.info("Created indexes");
	}

	public void loadConcepts() throws Exception {
		String data_dir = config.getProperty(DATA_DIR_KEY, LOGGER);
		String file = config.getProperty(CONCEPT_FILE_KEY, LOGGER);
		file = data_dir + file;
		String query = "";
		query += "INSERT INTO concept ";
		query += " (concept_id, concept_status, fully_specified_name, ";
		query += "  ctv3_id, snomed_id, is_primitive )";
		query += " VALUES (?, ?, ?, ?, ?, ?)";
		PreparedStatement stmt = conn.prepareStatement(query);
		//
		query = "";
		query += "SELECT * ";
		query += "  FROM concept ";
		query += " WHERE concept_id = ? ";
		PreparedStatement stmt_q = conn.prepareStatement(query);
		//
		BufferedReader in = IO.createBufferedUtf8FileReader(file);
		String line;
		int line_i = 0;
		while ((line = in.readLine()) != null) {
			line_i++;
			if (line_i == 1)
				continue;
			String[] field = line.split("\t");
			long concept_id = Long.parseLong(field[0]);
			if (ext_p) {
				stmt_q.setLong(1, concept_id);
				ResultSet res = stmt_q.executeQuery();
				if (res.next()) {
					LOGGER.warning("Extension concept exists in core: " + line_i + " " + line);
					res.close();
					continue;
				}
				res.close();
			}
			stmt.setLong(1, concept_id);
			stmt.setInt(2, Integer.parseInt(field[1]));
			stmt.setString(3, field[2]);
			stmt.setString(4, field[3]);
			stmt.setString(5, field[4]);
			stmt.setInt(6, Integer.parseInt(field[5]));
			stmt.executeUpdate();
			if (line_i % 10000 == 0) {
				LOGGER.fine("Line: " + line_i);
				progress_monitor.subTask("Concepts " + line_i);
			}
		}
		conn.commit();
		stmt.close();
		stmt_q.close();
		LOGGER.info("Processed lines: " + line_i);
		LOGGER.info("Loaded concepts");
	}

	public void loadDescriptions() throws Exception {
		String data_dir = config.getProperty(DATA_DIR_KEY, LOGGER);
		String file = config.getProperty(DESCRIPTION_FILE_KEY, LOGGER);
		file = data_dir + file;
		String query = "";
		query += "INSERT INTO description ";
		query += " (description_id, description_status, concept_id, ";
		query += "  term, initial_capital_status, description_type, ";
		query += "  language_code )";
		query += " VALUES (?, ?, ?, ?, ?, ?, ?)";
		PreparedStatement stmt = conn.prepareStatement(query);
		//
		query = "";
		query += "SELECT * ";
		query += "  FROM description ";
		query += " WHERE description_id = ? ";
		PreparedStatement stmt_q = conn.prepareStatement(query);
		//
		BufferedReader in = IO.createBufferedUtf8FileReader(file);
		String line;
		int line_i = 0;
		while ((line = in.readLine()) != null) {
			line_i++;
			if (line_i == 1)
				continue;
			String[] field = line.split("\t");
			long description_id = Long.parseLong(field[0]);
			if (ext_p) {
				stmt_q.setLong(1, description_id);
				ResultSet res = stmt_q.executeQuery();
				if (res.next()) {
					LOGGER.warning("Extension description exists in core: " + line_i + " " + line);
					res.close();
					continue;
				}
				res.close();
			}
			stmt.setLong(1, description_id);
			stmt.setInt(2, Integer.parseInt(field[1]));
			stmt.setLong(3, Long.parseLong(field[2]));
			stmt.setString(4, field[3]);
			stmt.setInt(5, Integer.parseInt(field[4]));
			stmt.setInt(6, Integer.parseInt(field[5]));
			stmt.setString(7, field[6]);
			stmt.executeUpdate();
			if (line_i % 10000 == 0) {
				LOGGER.fine("Line: " + line_i);
				progress_monitor.subTask("Descriptions " + line_i);
			}
		}
		conn.commit();
		stmt.close();
		LOGGER.info("Processed lines: " + line_i);
		LOGGER.info("Loaded descriptions");
	}

	public void loadRelationships() throws Exception {
		String data_dir = config.getProperty(DATA_DIR_KEY, LOGGER);
		String file = config.getProperty(RELATIONSHIP_FILE_KEY, LOGGER);
		file = data_dir + file;
		String query = "";
		query += "INSERT INTO relationship ";
		query += " (relationship_id, concept_id1, relationship_type, ";
		query += "  concept_id2, characteristic_type, refinability, ";
		query += "  relationship_group )";
		query += " VALUES (?, ?, ?, ?, ?, ?, ?)";
		PreparedStatement stmt = conn.prepareStatement(query);
		//
		query = "";
		query += "SELECT * ";
		query += "  FROM relationship ";
		query += " WHERE relationship_id = ? ";
		PreparedStatement stmt_q = conn.prepareStatement(query);
		//
		BufferedReader in = IO.createBufferedUtf8FileReader(file);
		String line;
		int line_i = 0;
		while ((line = in.readLine()) != null) {
			line_i++;
			if (line_i == 1)
				continue;
			String[] field = line.split("\t");
			if (field[0].length() > 18) {
				LOGGER.severe("Length: " + line_i + " " + field[0].length() + " >" + field[0] + "< " + file);
				field[0] = "-" + field[0].substring(0, 18);
			}
			try {
				long relationship_id = Long.parseLong(field[0]);
				if (ext_p) {
					stmt_q.setLong(1, relationship_id);
					ResultSet res = stmt_q.executeQuery();
					if (res.next()) {
						LOGGER.warning("Extension relationship exists in core: " + line_i + " " + line);
						res.close();
						continue;
					}
					res.close();
				}
				stmt.setLong(1, relationship_id);
				stmt.setLong(2, Long.parseLong(field[1]));
				stmt.setLong(3, Long.parseLong(field[2]));
				stmt.setLong(4, Long.parseLong(field[3]));
				stmt.setInt(5, Integer.parseInt(field[4]));
				stmt.setInt(6, Integer.parseInt(field[5]));
				stmt.setInt(7, Integer.parseInt(field[6]));
			} catch (Exception ex) {
				LOGGER.severe("Line " + line_i + " " + " " + ex.getMessage());
				LOGGER.severe("    >" + line + "<");
				continue;
			}
			stmt.executeUpdate();
			if (line_i % 10000 == 0) {
				LOGGER.fine("Line: " + line_i);
				progress_monitor.subTask("Relationships " + line_i);
			}
		}
		conn.commit();
		stmt.close();
		LOGGER.info("Processed lines: " + line_i);
		LOGGER.info("Loaded relationships");
	}

	public void loadComponentHistory() throws Exception {
		String data_dir = config.getProperty(DATA_DIR_KEY, LOGGER);
		String file = config.getProperty(COMPONENT_HISTORY_FILE_KEY, LOGGER);
		if (file == null) {
			LOGGER.warning("No component history file to load");
			return;
		}
		file = data_dir + file;
		String query = "";
		query += "INSERT INTO component_history ";
		query += " (component_id, release_version, change_type, ";
		query += "  status, reason )";
		query += " VALUES (?, ?, ?, ?, ?)";
		PreparedStatement stmt = conn.prepareStatement(query);
		BufferedReader in = IO.createBufferedUtf8FileReader(file);
		String line;
		int line_i = 0;
		while ((line = in.readLine()) != null) {
			line_i++;
			if (line_i == 1)
				continue;
			String[] field = line.split("\t", -1);
			stmt.setLong(1, Long.parseLong(field[0]));
			stmt.setInt(2, Integer.parseInt(field[1]));
			stmt.setInt(3, Integer.parseInt(field[2]));
			stmt.setInt(4, Integer.parseInt(field[3]));
			stmt.setString(5, field[4]);
			stmt.executeUpdate();
			if (line_i % 10000 == 0) {
				LOGGER.fine("Line: " + line_i);
				progress_monitor.subTask("Component history " + line_i);
			}
		}
		conn.commit();
		stmt.close();
		LOGGER.info("Processed lines: " + line_i);
		LOGGER.info("Loaded component history");
	}

	public void buildIsaTable() throws Exception {
		Sql.dropTable(conn, "isa");
		//
		String query;
		//
		query = "";
		query += "concept_id1 BIGINT , ";
		query += "concept_id2 BIGINT   ";
		Sql.createTable(conn, "isa", query);
		//
		query = "";
		query += "INSERT INTO isa ";
		query += "(concept_id1, concept_id2) ";
		query += "SELECT DISTINCT concept_id1, concept_id2 ";
		query += "  FROM relationship ";
		query += " WHERE relationship_type = " + Snomed.ISA_ID + " ";
		Statement stmt = this.conn.createStatement();
		stmt.executeUpdate(query);
		stmt.close();
		//
		Sql.createIndex(conn, "isa", "concept_id1");
		Sql.createIndex(conn, "isa", "concept_id2");
		this.conn.commit();
		LOGGER.info("Created isa");
	}

	private void exportTable(String table_name, String file_name) throws Exception {
		PrintWriter out = IO.createPrintUtf8FileWriter(file_name);
		String query;
		query = "";
		query += "SELECT * FROM " + table_name;
		Statement stmt = conn.createStatement();
		ResultSet res = stmt.executeQuery(query);
		ResultSetMetaData resmd = res.getMetaData();
		int cols = resmd.getColumnCount();
		for (int i = 1; i <= cols; i++) {
			out.print(resmd.getColumnName(i));
			if (i != cols) {
				out.print("\t");
			}
		}
		out.println();
		int count = 0;
		int ext_count = 0;
		while (res.next()) {
			count++;
			long id = res.getLong(1);
			if (!Snomed.isExtensionId(id))
				continue;
			ext_count++;
			for (int i = 1; i <= cols; i++) {
				out.print(res.getObject(i));
				if (i != cols) {
					out.print("\t");
				}
			}
			out.println();
		}
		res.close();
		stmt.close();
		out.close();
		LOGGER.info("Processed: " + ext_count + " of " + count);
	}

	public void exportExtensions() throws Exception {
		String data_dir = config.getProperty(DATA_DIR_KEY, LOGGER);
		String file;
		file = config.getProperty(CONCEPT_FILE_KEY, LOGGER);
		exportTable("concept", data_dir + file);
		file = config.getProperty(DESCRIPTION_FILE_KEY, LOGGER);
		exportTable("description", data_dir + file);
		file = config.getProperty(RELATIONSHIP_FILE_KEY, LOGGER);
		exportTable("relationship", data_dir + file);
	}

	private boolean ext_p = false;

	public void loadExtension() throws Exception {
		this.init();
		ext_p = true;
		this.loadConcepts();
		this.loadDescriptions();
		this.loadRelationships();
		ext_p = false;
		this.buildIsaTable();
		this.shutdown();
	}

	public void run() throws Exception {
		if (progress_monitor == null)
			progress_monitor = new BasicProgressMonitorNoOp();
		progress_monitor.beginTask("Snomed Loader", 100);
		this.init();
		progress_monitor.worked(1);
		this.createSchema();
		progress_monitor.worked(1);
		this.loadConcepts();
		progress_monitor.worked(9);
		this.loadDescriptions();
		progress_monitor.worked(16);
		this.loadRelationships();
		progress_monitor.worked(17);
		this.loadComponentHistory();
		progress_monitor.worked(39);
		progress_monitor.subTask("Indexes");
		this.createIndexes();
		progress_monitor.worked(11);
		this.buildIsaTable();
		progress_monitor.worked(6);
		this.shutdown();
		progress_monitor.done();
	}

	/**
	 * Set up logging, call the constructor, and run
	 * 
	 * <p>
	 * Args: <br>
	 * config_file
	 */
	public static void main(String[] args) {
		try {
			SnomedLoader.LOGGER = BasicLogger.init(SnomedLoader.class, true, true);
			Sql.LOGGER = SnomedLoader.LOGGER;
			String config_name = Config.getConfigName(args, LOGGER);
			SnomedLoader m = new SnomedLoader(config_name);
			m.run();
		} catch (Exception ex) {
			BasicLogger.log(ex, LOGGER);
		} finally {
			BasicLogger.end(LOGGER);
		}
	}

}
