/**
 * Copyright 2008-2017 Mays Systems LLC.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *       http://www.apache.org/licenses/LICENSE-2.0
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package com.mays.snomed.rf1;

/**
 * Just the items in the relationship table. Does not include concept_id1. Add
 * in the relationship name and concept2 name.
 * 
 * @author Eric Mays
 */
public class ConceptRelationship implements Comparable<ConceptRelationship> {

	long relationship_id;

	long relationship_type;

	String relationship_name;

	long concept_id2;

	String concept_name2;

	int characteristic_type;

	int refinability;

	int relationship_group;

	public ConceptRelationship(long relationship_id, long relationship_type,
			String relationship_name, long concept_id2, String concept_name2,
			int characteristic_type, int refinability, int relationship_group) {
		super();
		this.relationship_id = relationship_id;
		this.relationship_type = relationship_type;
		this.relationship_name = relationship_name;
		this.concept_id2 = concept_id2;
		this.concept_name2 = concept_name2;
		this.characteristic_type = characteristic_type;
		this.refinability = refinability;
		this.relationship_group = relationship_group;
	}

	public int getCharacteristicType() {
		return characteristic_type;
	}

	public void setCharacteristicType(int characteristic_type) {
		this.characteristic_type = characteristic_type;
	}

	public long getConceptId2() {
		return concept_id2;
	}

	public void setConceptId2(long concept_id2) {
		this.concept_id2 = concept_id2;
	}

	public String getConceptName2() {
		return concept_name2;
	}

	public void setConceptName2(String concept_name2) {
		this.concept_name2 = concept_name2;
	}

	public int getRefinability() {
		return refinability;
	}

	public void setRefinability(int refinability) {
		this.refinability = refinability;
	}

	public int getRelationshipGroup() {
		return relationship_group;
	}

	public void setRelationshipGroup(int relationship_group) {
		this.relationship_group = relationship_group;
	}

	public long getRelationshipId() {
		return relationship_id;
	}

	public void setRelationshipId(long relationship_id) {
		this.relationship_id = relationship_id;
	}

	public long getRelationshipType() {
		return relationship_type;
	}

	public void setRelationshipType(long relationship_type) {
		this.relationship_type = relationship_type;
	}

	public String getRelationshipName() {
		return relationship_name;
	}

	public void setRelationshipName(String relationship_name) {
		this.relationship_name = relationship_name;
	}

	/**
	 * Order by characteristic type, group, isa, relationship name, then
	 * concept2 name
	 */
	public int compareTo(ConceptRelationship that) {
		if (this.getCharacteristicType() != that.getCharacteristicType())
			return new Integer(this.getCharacteristicType())
					.compareTo(new Integer(that.getCharacteristicType()));
		if (this.getRelationshipGroup() != that.getRelationshipGroup())
			return new Integer(this.getRelationshipGroup())
					.compareTo(new Integer(that.getRelationshipGroup()));
		if ((this.getRelationshipType() == Snomed.ISA_ID)
				&& (that.getRelationshipType() != Snomed.ISA_ID))
			return -1;
		if ((this.getRelationshipType() != Snomed.ISA_ID)
				&& (that.getRelationshipType() == Snomed.ISA_ID))
			return 1;
		if ((this.getRelationshipType() != that.getRelationshipType()))
			return this.getRelationshipName().compareTo(
					that.getRelationshipName());
		return this.getConceptName2().compareTo(that.getConceptName2());
	}

	@Override
	public boolean equals(Object that_obj) {
		if (!(that_obj instanceof ConceptRelationship))
			return false;
		ConceptRelationship that = (ConceptRelationship) that_obj;
		return this.getCharacteristicType() == that.getCharacteristicType()
				&& this.getRelationshipGroup() == that.getRelationshipGroup()
				&& this.getRelationshipType() == that.getRelationshipType()
				&& this.getConceptId2() == that.getConceptId2();
	}

}
