/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.mays.snomed.test;

import com.mays.snomed.test.ordered.SnomedSearchTst;
import com.mays.snomed.test.ordered.SnomedIsaTst;
import com.mays.snomed.test.ordered.SnomedDbTst;
import com.mays.snomed.test.ordered.SnomedAnalyzerTst;
import com.mays.snomed.test.ordered.SnomedSearchBuildIndexTst;
import com.mays.snomed.test.ordered.SnomedLoaderTst;
import org.junit.After;
import org.junit.AfterClass;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;

/**
 *
 * @author kec
 */
public class SuiteTest {

	public SuiteTest() {
	}

	@BeforeClass
	public static void setUpClass() {
	}

	@AfterClass
	public static void tearDownClass() {
	}

	@Before
	public void setUp() {
	}

	@After
	public void tearDown() {
	}

	@Test
	public void doLoad() throws Exception {
		SnomedLoaderTst.doLoad();
		SnomedSearchBuildIndexTst.doLoad();
		SnomedAnalyzerTst.testHyphen();
		SnomedAnalyzerTst.testPG();
		SnomedDbTst snomedDbTest = new SnomedDbTst();
		snomedDbTest.getRoot();
		snomedDbTest.getRootByFsn();
		snomedDbTest.getNone();
		snomedDbTest.getRootDescription();
		snomedDbTest.getRootChildren();
		snomedDbTest.getRootParents();
		snomedDbTest.getConcepts();
		snomedDbTest.getConceptsSorted();
		snomedDbTest.doConcepts();
		snomedDbTest.doConceptsUuid();
		snomedDbTest.doParents();
		snomedDbTest.doChildren();
		snomedDbTest.doFsn();
		// TODO fix this test
		// snomedDbTest.doExtensionId();

		SnomedIsaTst snomedIsaTest = new SnomedIsaTst();
		snomedIsaTest.doChildren();

		// TODO fix this test
		// snomedIsaTest.doParents();
		snomedIsaTest.getTopLevelSubsumers();
		snomedIsaTest.getDescendants();
		snomedIsaTest.getAncestors();
		// TODO fix this test
		// snomedIsaTest.getSiblings();

		SnomedSearchTst snomedSearchTest = new SnomedSearchTst();
		snomedSearchTest.testHyphen();
		snomedSearchTest.testClinicalFinding();
		snomedSearchTest.testBodyStructure();
		snomedSearchTest.testAll();
		snomedSearchTest.testIncludes();
		snomedSearchTest.testIncludesNeg();
		snomedSearchTest.testExcludes();
		snomedSearchTest.testMultipleIncludes();
		snomedSearchTest.testMultipleIncludesEmpty();
		snomedSearchTest.testMultipleIncludesNeg();

	}
}
