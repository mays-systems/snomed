/**
 * Copyright 2008-2017 Mays Systems LLC.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *       http://www.apache.org/licenses/LICENSE-2.0
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package com.mays.snomed.test.ordered;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertNotNull;
import static org.junit.Assert.assertNull;
import static org.junit.Assert.assertTrue;

import java.sql.Connection;
import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import java.util.logging.Logger;

import com.mays.snomed.ConceptBasic;
import com.mays.snomed.Snomed;
import com.mays.snomed.SnomedDb;
import com.mays.util.BasicLogger;
import com.mays.util.Config;
import com.mays.util.Sql;
import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.util.logging.Level;

public class SnomedDbTst {

	protected static Logger LOGGER;

	protected static Config CONFIG;

	protected static SnomedDb SNOMED_DB;

	public SnomedDbTst() throws IOException, Exception {
		Files.createDirectories(Paths.get("logs"));
		LOGGER = BasicLogger.init(SnomedDbTst.class, true, true);
		Sql.LOGGER = LOGGER;
		CONFIG = Config.load(System.getProperty("SnomedLoader.properties"), LOGGER);
		Connection conn = Sql.getConnection(CONFIG);
		SNOMED_DB = new SnomedDb();
		SNOMED_DB.init(conn);
	}

	protected long getRootId() {
		return Snomed.ROOT_ID;
	}

	public void getRoot() throws Exception {
		assertNotNull(SNOMED_DB.getConcept(getRootId()));
	}

	public void getRootByFsn() throws Exception {
		ConceptBasic con = SNOMED_DB.getConcept(getRootId());
		assertTrue(con.getConceptId() == SNOMED_DB.getConceptByFullySpecifiedName(con.getFullySpecifiedName())
				.getConceptId());
	}

	public void getNone() throws Exception {
		assertNull(SNOMED_DB.getConcept(1));
	}

	public void getRootDescription() throws Exception {
		assertTrue(SNOMED_DB.getDescriptions(getRootId()).size() > 1);
	}

	public void getRootChildren() throws Exception {
		assertTrue(SNOMED_DB.getChildren(getRootId()).size() > 1);
	}

	public void getRootParents() throws Exception {
		assertTrue(SNOMED_DB.getParents(getRootId()).isEmpty());
	}

	protected int getExpectedConceptMin() {
		return 300000;
	}

	public void getConcepts() throws Exception {
		LOGGER.log(Level.INFO, "Concept count: {0}", SNOMED_DB.getConcepts().size());
		assertTrue(SNOMED_DB.getConcepts().size() > getExpectedConceptMin());
	}

	public List<Long> getConceptsSorted() {
		List<Long> cons = new ArrayList<>();
		SNOMED_DB.getConcepts().forEach((con) -> {
			cons.add(con.getConceptId());
		});
		Collections.sort(cons);
		return cons;
	}

	public void doConcepts() throws Exception {
		SNOMED_DB.getConcepts().forEach((con) -> {
			assertTrue(con.getConceptId() == SNOMED_DB.getConcept(con.getConceptId()).getConceptId());
		});
	}

	public void doConceptsUuid() throws Exception {
		SNOMED_DB.getConcepts().forEach((con) -> {
			assertTrue(con.getUuid() == SNOMED_DB.getConcept(con.getUuid()).getUuid());
		});
	}

	public void doParents() throws Exception {
		for (ConceptBasic con : SNOMED_DB.getConcepts()) {
			if (!con.getActive())
				continue;
			List<Long> parents = SNOMED_DB.getParents(con.getConceptId());
			if (con.getConceptId() == getRootId()) {
				assertTrue(con + " conceptId == getRootId()", parents.isEmpty());
			} else {
				assertTrue(con.getLogString() + " parents size should be > 0", parents.size() > 0);
			}
		}
	}

	protected int getExpectedChildrenMin() {
		return 50000;
	}

	public void doChildren() throws Exception {
		int has_children_count = 0;
		for (ConceptBasic con : SNOMED_DB.getConcepts()) {
			List<Long> children = SNOMED_DB.getChildren(con.getConceptId());
			assertEquals(children.size() > 0, SNOMED_DB.hasChildren(con.getConceptId()));
			if (children.size() > 0) {
				has_children_count++;
			}
		}
		LOGGER.log(Level.INFO, "Children count: {0}", has_children_count);
		assertTrue(has_children_count > getExpectedChildrenMin());
	}

	public void doFsn() throws Exception {
		List<Long> cons = getConceptsSorted();
		int i = 0;
		for (long id : cons) {
			if ((++i % 1000) == 0) {
				// LOGGER.info("Progress: " + (i * 100 / cons.size()));
				ConceptBasic con = SNOMED_DB.getConcept(id);
				if (Snomed.isStatusActive(con.getActive())) {
					ConceptBasic fsn_con = SNOMED_DB.getConceptByFullySpecifiedName(con.getFullySpecifiedName());
					assertTrue(con.getConceptId() == fsn_con.getConceptId());
				}
			}
		}
	}

	public void doExtensionId() throws Exception {
		SNOMED_DB.getConcepts().forEach((con) -> {
			assertFalse(Snomed.isExtensionId(con.getConceptId()));
		});
	}

}
