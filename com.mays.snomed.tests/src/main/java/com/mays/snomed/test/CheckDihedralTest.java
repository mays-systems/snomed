/**
 * Copyright 2008-2017 Mays Systems LLC.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *       http://www.apache.org/licenses/LICENSE-2.0
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package com.mays.snomed.test;

import static org.junit.Assert.assertEquals;

import java.util.Arrays;

import org.junit.Assert;
import org.junit.Test;

import com.mays.snomed.CheckDihedral;
import com.mays.snomed.Snomed;

/**
 * Some test cases derived from the SNOMED CT documentation
 * 
 * @author Eric Mays
 * 
 */
public class CheckDihedralTest {

	// 100 00 5 00=Concept
	// The Item identifier digits 1000 are the lowest permitted value thus
	// this is the lowest SCTID that can be allocated to a Concept.
	@Test
	public void testConcept() {
		Assert.assertEquals(Long.parseLong("100" + "00" + "5"), Snomed
				.createSctId(100, 0, Snomed.CONCEPT_ENTITY, false));
		Assert.assertTrue(CheckDihedral.verify("100" + "00" + "5"));
		Assert.assertFalse(CheckDihedral.verify("100" + "00" + "4"));
	}

	// 100 01 4 01=Description
	// This is the lowest SCTID that can be allocated to a Description.
	@Test
	public void testDescription() {
		Assert.assertEquals(Long.parseLong("100" + "01" + "4"), Snomed
				.createSctId(100, 0, Snomed.DESCRIPTION_ENTITY, false));
	}

	// 100 02 2 02=Relationship
	// This is the lowest SCTID that can be allocated to a Relationship.
	@Test
	public void testRelationship() {
		Assert.assertEquals(Long.parseLong("100" + "02" + "2"), Snomed
				.createSctId(100, 0, Snomed.RELATIONSHIP_ENTITY, false));
	}

	// 100 03 3 03=Subset
	// This is the lowest SCTID that can be allocated to a Subset.
	@Test
	public void testSubset() {
		Assert.assertEquals(Long.parseLong("100" + "03" + "3"), Snomed
				.createSctId(100, 0, Snomed.SUBSET_ENTITY, false));
	}

	// 101291 00 9 00=Concept
	// A valid SCTID for a Concept.
	@Test
	public void testConcept2() {
		Assert.assertEquals(Long.parseLong("101291" + "00" + "9"), Snomed
				.createSctId(101291, 0, Snomed.CONCEPT_ENTITY, false));
	}

	// 1290023401 01 5 01=Description
	// A valid SCTID for a Description.
	@Test
	public void testDescription2() {
		Assert.assertEquals(Long.parseLong("1290023401" + "01" + "5"), Snomed
				.createSctId(1290023401, 0, Snomed.DESCRIPTION_ENTITY, false));
	}

	// 9940000001 02 9 02=Relationship
	// A valid SCTID for a Relationship.
	@Test
	public void testRelationship2() {
		Assert
				.assertEquals(Long.parseLong("9940000001" + "02" + "9"), Snomed
						.createSctId(9940000001l, 0,
								Snomed.RELATIONSHIP_ENTITY, false));
	}

	// 40099901 03 7 03=Subset
	// A valid SCTID for a Subset.
	@Test
	public void testSubset2() {
		Assert.assertEquals(Long.parseLong("40099901" + "03" + "7"), Snomed
				.createSctId(40099901, 0, Snomed.SUBSET_ENTITY, false));
	}

	// 1 0000001 10 5 10=Extra-Concept
	// A valid SCTID for a Concept in an Extension in the 0000001 namespace.
	@Test
	public void testExtConcept() {
		Assert.assertEquals(Long.parseLong("1" + "0000001" + "10" + "5"),
				Snomed.createSctId(1, 1, Snomed.CONCEPT_ENTITY, true));
	}

	// 1 0989121 10 8 10=Extra-Concept
	// A valid SCTID for a Concept in an Extension
	// in the 0989121 namespace.
	@Test
	public void testExtConcept2() {
		Assert.assertEquals(Long.parseLong("1" + "0989121" + "10" + "8"),
				Snomed.createSctId(1, 989121, Snomed.CONCEPT_ENTITY, true));
	}

	// 129 0989121 10 3 10=Extra-Concept
	// A valid SCTID for a Concept in an Extension in the 0989121 namespace.
	@Test
	public void testExtConcept3() {
		Assert.assertEquals(Long.parseLong("129" + "0989121" + "10" + "3"),
				Snomed.createSctId(129, 989121, Snomed.CONCEPT_ENTITY, true));
	}

	// 129 0000001 11 7 11=Extra-Description
	// A valid SCTID for a Description in an Extension in the 0000001 namespace.
	@Test
	public void testExtDescription() {
		Assert.assertEquals(Long.parseLong("129" + "0000001" + "11" + "7"),
				Snomed.createSctId(129, 1, Snomed.DESCRIPTION_ENTITY, true));
	}

	// 994 0000001 12 6 12=Extra-Relationship
	// A valid SCTID for a Relationship in an Extension in the 0000001
	// namespace.
	@Test
	public void testExtRelationship() {
		Assert.assertEquals(Long.parseLong("994" + "0000001" + "12" + "6"),
				Snomed.createSctId(994, 1, Snomed.RELATIONSHIP_ENTITY, true));
	}

	// 4 0000001 13 2 13=Extra-Subset
	// A valid SCTID for a Subset in an Extension in the 0000001 namespace.
	@Test
	public void testExtSubset() {
		Assert.assertEquals(Long.parseLong("4" + "0000001" + "13" + "2"),
				Snomed.createSctId(4, 1, Snomed.SUBSET_ENTITY, true));
	}

	// 99999999 0989121 10 4
	// The maximum valid SCTID for a Concept in an Extension in the 0989121
	// namespace.
	@Test
	public void testExtConceptMax() {
		Assert.assertEquals(
				Long.parseLong("99999999" + "0989121" + "10" + "4"), Snomed
						.createSctId(99999999, 989121, Snomed.CONCEPT_ENTITY,
								true));
	}

	@Test
	public void testExtId() {
		for (long item_id : Arrays.asList(100l, 99999999l)) {
			for (int nmsp_id : Arrays.asList(1, 989121)) {
				for (int ent_id : Arrays.asList(Snomed.CONCEPT_ENTITY,
						Snomed.DESCRIPTION_ENTITY, Snomed.RELATIONSHIP_ENTITY,
						Snomed.SUBSET_ENTITY)) {
					for (boolean ext_p : Arrays.asList(true, false)) {
						assertEquals(ext_p, Snomed.isExtensionId(Snomed
								.createSctId(item_id, nmsp_id, ent_id, ext_p)));
					}
				}
			}
		}
	}

}
