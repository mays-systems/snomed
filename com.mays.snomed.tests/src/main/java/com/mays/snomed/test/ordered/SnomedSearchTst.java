/**
 * Copyright 2008-2017 Mays Systems LLC.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *       http://www.apache.org/licenses/LICENSE-2.0
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package com.mays.snomed.test.ordered;

import java.sql.Connection;
import java.util.Arrays;
import java.util.Collection;
import java.util.logging.Logger;

import org.junit.Assert;

import com.mays.snomed.ConceptBasic;
import com.mays.snomed.SnomedDb;
import com.mays.util.BasicLogger;
import com.mays.util.Config;
import com.mays.util.Sql;
import com.mays.util.search.SearchLucene;
import java.nio.file.Files;
import java.nio.file.Paths;

public class SnomedSearchTst {

    protected static Logger LOGGER;

    protected static Config CONFIG;

    protected static SnomedDb SNOMED_DB;

    protected static SearchLucene SNOMED_SEARCH;

    public SnomedSearchTst() throws Exception {
        Files.createDirectories(Paths.get("logs"));
        LOGGER = BasicLogger.init(SnomedSearchTst.class, true, true);
        Sql.LOGGER = LOGGER;
        CONFIG = Config.load(System.getProperty("SnomedLoader.properties"), LOGGER);
        Connection conn = Sql.getConnection(CONFIG);
        SNOMED_DB = new SnomedDb();
        SNOMED_DB.init(conn);
        SearchLucene.LOGGER = LOGGER;
        String lucene_home = SearchLucene.getLuceneHome(CONFIG);
        SNOMED_SEARCH = new SearchLucene();
        SNOMED_SEARCH.init(lucene_home);
    }

    private void doSearch(String term, long con) throws Exception {
        doSearch(term, con, SearchLucene.EMPTY_CONS, SearchLucene.EMPTY_CONS,
                true);
    }

    private void doSearch(String term, long con, Collection<Long> includes,
            Collection<Long> excludes, boolean b) throws Exception {
        // SNOMED_SEARCH.doSearch(term, SNOMED_DB, includes, excludes, true,
        // SNOMED_SEARCH);
        Assert.assertTrue(b == SNOMED_SEARCH.searchConcepts(term, 1, 20,
                includes, excludes).contains(con));
    }

    public void testHyphen() throws Exception {
        doSearch("Pregnancy-associated gingivitis", 95212006);
    }

    private final static long CLINICAL_FINDING = 404684003;

    public void testClinicalFinding() throws Exception {
        testConcept(CLINICAL_FINDING);
    }

    private final static long BODY_STRUCTURE = 123037004;

    public void testBodyStructure() throws Exception {
        testConcept(BODY_STRUCTURE);
    }

    public void testConcept(long id) throws Exception {
        ConceptBasic con = SNOMED_DB.getConcept(id);
        Assert.assertNotNull(con);
        LOGGER.info(con.getLogString());
    }

    public void testAll() throws Exception {
        // morphologic abnormality
        doSearch("Diverticulitis", 18126004);
    }

    public void testIncludes() throws Exception {
        // morphologic abnormality
        doSearch("Diverticulitis", 18126004, Arrays.asList(BODY_STRUCTURE),
                SearchLucene.EMPTY_CONS, true);
    }

     public void testIncludesNeg() throws Exception {
        // disorder
        doSearch("Diverticulitis", 307496006, Arrays.asList(BODY_STRUCTURE),
                SearchLucene.EMPTY_CONS, false);
    }

    public void testExcludes() throws Exception {
        // morphologic abnormality
        doSearch("Diverticulitis", 18126004, SearchLucene.EMPTY_CONS,
                Arrays.asList(BODY_STRUCTURE), false);
    }

    public void testMultipleIncludes() throws Exception {
        // morphologic abnormality
        doSearch("Diverticulitis", 18126004,
                Arrays.asList(BODY_STRUCTURE, CLINICAL_FINDING),
                SearchLucene.EMPTY_CONS, true);
    }

    public void testMultipleIncludesEmpty() throws Exception {
        doSearch("Linkage concept", 106237007, SearchLucene.EMPTY_CONS,
                SearchLucene.EMPTY_CONS, true);
    }

    public void testMultipleIncludesNeg() throws Exception {
        doSearch("Linkage concept", 106237007,
                Arrays.asList(BODY_STRUCTURE, CLINICAL_FINDING),
                SearchLucene.EMPTY_CONS, false);
    }

}
