/**
 * Copyright 2008-2017 Mays Systems LLC.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *       http://www.apache.org/licenses/LICENSE-2.0
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package com.mays.snomed.test.ordered;


import org.apache.lucene.document.Document;
import org.junit.Test;

import com.mays.snomed.SnomedLoader;
import com.mays.util.Config;
import com.mays.util.search.SearchLucene;

public class SnomedAnalyzerTst {


	// public ArrayList<Token> getAnalyzedTokens(Analyzer analyzer, String text)
	// throws IOException {
	// TokenStream stream = analyzer.tokenStream("contents", new StringReader(
	// text));
	// ArrayList<Token> token_list = new ArrayList<Token>();
	// while (true) {
	// Token token = stream.next();
	// if (token == null)
	// break;
	// token_list.add(token);
	// }
	// return token_list;
	// }

	@Test
	public static void testPG() throws Exception {
		testOne("Pregnancy gingivitis");
	}

	@Test
	public static void testHyphen() throws Exception {
		testOne("Pregnancy-associated gingivitis");
	}

	public static void testOne(String term) throws Exception {
		SearchLucene searcher = new SearchLucene();
		String config_name = Config.getConfigName(
				new String[] { System.getProperty("SnomedLoader.properties") }, SnomedLoader.LOGGER);
                
                searcher.init(SearchLucene.getLuceneHome(Config.load(config_name)));
		for (Document doc : searcher.search(term, 1, 20)) {
			System.out.println("Hit: " + doc.getField("contents").stringValue());
		}
		// Analyzer analyzer = new SnomedAnalyzer();
		// LOGGER.info("Term: " + term);
		// String token_str = "";
		// BooleanQuery query = new BooleanQuery();
		// for (Token token : getAnalyzedTokens(analyzer, term)) {
		// token_str += "[" + token.termText() + "]";
		// query.add(new TermQuery(new Term("contents", token.termText())),
		// BooleanClause.Occur.SHOULD);
		// }
		// int res_start = 0;
		// int res_count = 20;
		// IndexSearcher isearcher = new IndexSearcher("/lucene-index");
		// LOGGER.info("Query: " + query.toString());
		// Hits hits = isearcher.search(query);
		// for (int i = res_start; i <= res_start + res_count; i++) {
		// if (i >= hits.length())
		// break;
		// LOGGER.info("Hit: "
		// + hits.doc(i).getField("contents").stringValue());
		// }
	}

}
