/**
 * Copyright 2008-2017 Mays Systems LLC.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *       http://www.apache.org/licenses/LICENSE-2.0
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package com.mays.snomed.test.ordered;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertTrue;

import java.sql.Connection;
import java.util.List;
import java.util.logging.Logger;

import com.mays.snomed.ConceptBasic;
import com.mays.snomed.Snomed;
import com.mays.snomed.SnomedIsa;
import com.mays.util.BasicLogger;
import com.mays.util.Config;
import com.mays.util.Sql;
import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.util.logging.Level;

public class SnomedIsaTst {

    protected static Logger LOGGER;

    protected static Config CONFIG;

    protected static SnomedIsa SNOMED_DB;

    public SnomedIsaTst() throws IOException, Exception {
        Files.createDirectories(Paths.get("logs"));
        LOGGER = BasicLogger.init(SnomedIsaTst.class, true, true);
        Sql.LOGGER = LOGGER;
        CONFIG = Config.load(System.getProperty("SnomedLoader.properties"), LOGGER);
        Connection conn = Sql.getConnection(CONFIG);
        SNOMED_DB = new SnomedIsa();
        SNOMED_DB.init(conn);
    }

     public void doChildren() throws Exception {
        int has_children_count = 0;
        for (ConceptBasic con : SNOMED_DB.getConcepts()) {
            List<Long> children = SNOMED_DB.getChildren(con.getConceptId());
            assertEquals(children.size() > 0,
                    SNOMED_DB.hasChildren(con.getConceptId()));
            if (children.size() > 0) {
                has_children_count++;
            }
        }
        LOGGER.log(Level.INFO, "Children count: {0}", has_children_count);
        assertTrue(has_children_count > 50000);
    }

    public void doParents() throws Exception {
        for (ConceptBasic con : SNOMED_DB.getConcepts()) {
            List<Long> parents = SNOMED_DB.getParents(con.getConceptId());
            if (con.getConceptId() == Snomed.ROOT_ID) {
                assertTrue(parents.isEmpty());
            } else {
                assertTrue(parents.size() > 0);
            }
        }
    }

    public final void getTopLevelSubsumers() {
        for (long tls : SNOMED_DB.getChildren(Snomed.ROOT_ID)) {
            for (long desc : SNOMED_DB.getDescendants(tls)) {
                assertTrue(SNOMED_DB.getTopLevelSubsumers(desc).contains(tls));
            }
        }
    }

    public final void getDescendants() {
        for (ConceptBasic con : SNOMED_DB.getConcepts()) {
            for (long desc : SNOMED_DB.getDescendants(con.getConceptId())) {
                assertTrue(SNOMED_DB.isaSubsumesP(con.getConceptId(), desc));
                assertFalse(SNOMED_DB.isaSubsumesP(desc, con.getConceptId()));
            }
            assertTrue(SNOMED_DB.getDescendants(con.getConceptId())
                    .containsAll(SNOMED_DB.getChildren(con.getConceptId())));
        }
    }

     public final void getAncestors() {
        for (ConceptBasic con : SNOMED_DB.getConcepts()) {
            for (long anc : SNOMED_DB.getAncestors(con.getConceptId())) {
                assertFalse(SNOMED_DB.isaSubsumesP(con.getConceptId(), anc));
                assertTrue(SNOMED_DB.isaSubsumesP(anc, con.getConceptId()));
            }
            assertTrue(SNOMED_DB.getAncestors(con.getConceptId()).containsAll(
                    SNOMED_DB.getParents(con.getConceptId())));
        }
    }

    public final void getSiblings() throws Exception {
        for (ConceptBasic con : SNOMED_DB.getConcepts()) {
            // This will run forever over the retired
            if (Snomed.isStatusActive(con.getActive())) {
                for (long sib : SNOMED_DB.getSiblings(con.getConceptId())) {
                    assertFalse(SNOMED_DB.isaSubsumesP(con.getConceptId(), sib));
                    assertFalse(SNOMED_DB.isaSubsumesP(sib, con.getConceptId()));
                }
            }
        }
    }
}
