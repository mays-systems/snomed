/**
 * Copyright 2008-2017 Mays Systems LLC.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *       http://www.apache.org/licenses/LICENSE-2.0
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package com.mays.snomed.test;

import java.sql.Connection;
import java.util.Arrays;
import java.util.UUID;
import java.util.logging.Logger;

import com.mays.snomed.ConceptBasic;
import com.mays.snomed.SnomedDb;
import com.mays.snomed.subset.SubsetDb;
import com.mays.util.BasicLogger;
import com.mays.util.Config;
import com.mays.util.Sql;

import java.nio.file.Files;
import java.nio.file.Paths;
import org.junit.Ignore;

@Ignore
public class TestSubset {

	private static Logger LOGGER;

	private Config config;

	private Connection conn;

	private SnomedDb snomed_db;

	private SubsetDb subset_db;

	public TestSubset(String config_name) throws Exception {
		super();
                Files.createDirectories(Paths.get("logs"));
		this.config = Config.load(config_name, LOGGER);
	}

	public void init() throws Exception {
		String derby_home = config.getProperty(Sql.DERBY_HOME_KEY, LOGGER);
		String schema = config.getProperty(Sql.SCHEMA_KEY, LOGGER);
		this.init(Sql.getConnection(derby_home, schema));
		this.snomed_db = new SnomedDb();
		this.snomed_db.init(this.conn);
		this.subset_db = new SubsetDb(conn);
		this.subset_db.init();
	}

	public void init(Connection conn) throws Exception {
		this.conn = conn;
	}

	void showAncestors(UUID subset_id, long concept_id, int indent)
			throws Exception {
		if (concept_id == 0)
			return;
		for (long id : this.subset_db.getParents(subset_id, concept_id)) {
			ConceptBasic parent = this.snomed_db.getConcept(id);
			String tabs = "";
			for (int i = 0; i < indent; i++) {
				tabs += " ";
			}
			LOGGER.info(tabs + parent.getFullySpecifiedName());
			showAncestors(subset_id, id, ++indent);
		}
	}

	public void run() throws Exception {
		this.init();
		for (long sctid : Arrays.asList(123037004l)) {
			ConceptBasic con = this.snomed_db.getConcept(sctid);
			LOGGER.info(con.getFullySpecifiedName());
			// showAncestors(100000, sctid, 1);
		}
		// long subset_id = 100000;
		// this.subset_db.deleteSubset(subset_id);
		// this.subset_db.createSubset("test subset", subset_id);
		// ArrayList<SubsetExpression> exprs = new
		// ArrayList<SubsetExpression>();
		// long sctid = 416698001;
		// ConceptBasic con = this.snomed_db.getConcept(sctid);
		// LOGGER.info(con.getFullySpecifiedName());
		// exprs
		// .add(new SubsetExpression(SubsetExpression.Operator.INCLUDE,
		// SubsetExpression.Modifier.DESCENDANTS, true, con
		// .getConceptId()));
		// this.subset_db.saveSubsetExpression(subset_id, exprs);
		// this.subset_db.buildSubset(subset_id);
	}

	public static void main(String[] args) {
		try {
			TestSubset.LOGGER = BasicLogger.init(TestSubset.class);
			Sql.LOGGER = TestSubset.LOGGER;
			String config_name = Config.getConfigName(args, LOGGER);
			TestSubset m = new TestSubset(config_name);
			m.run();
		} catch (Exception ex) {
			BasicLogger.log(ex, LOGGER);
		} finally {
			BasicLogger.end(LOGGER);
		}
	}

}
