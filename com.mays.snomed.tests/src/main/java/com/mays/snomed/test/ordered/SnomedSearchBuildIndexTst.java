/**
 * Copyright 2008-2017 Mays Systems LLC.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *       http://www.apache.org/licenses/LICENSE-2.0
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package com.mays.snomed.test.ordered;


import com.mays.snomed.SnomedLoader;
import com.mays.snomed.search.SnomedSearchBuildLuceneIndex;
import com.mays.util.BasicLogger;
import com.mays.util.Config;
import com.mays.util.Sql;

import java.nio.file.Files;
import java.nio.file.Paths;

public class SnomedSearchBuildIndexTst {


	public static void doLoad() throws Exception {
                Files.createDirectories(Paths.get("logs"));
		SnomedSearchBuildLuceneIndex.LOGGER = BasicLogger.init(
				SnomedLoader.class, true, true);
		Sql.LOGGER = SnomedSearchBuildLuceneIndex.LOGGER;
		String config_name = Config.getConfigName(
				new String[] { System.getProperty("SnomedLoader.properties") },
				SnomedSearchBuildLuceneIndex.LOGGER);
		SnomedSearchBuildLuceneIndex m = new SnomedSearchBuildLuceneIndex(
				config_name);
		m.run();
	}

}
