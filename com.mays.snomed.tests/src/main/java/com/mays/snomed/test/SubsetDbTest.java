/**
 * Copyright 2008-2017 Mays Systems LLC.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *       http://www.apache.org/licenses/LICENSE-2.0
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package com.mays.snomed.test;

import static org.junit.Assert.assertTrue;

import java.sql.Connection;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.Set;
import java.util.TreeSet;
import java.util.UUID;
import java.util.logging.Logger;

import org.junit.After;
import org.junit.AfterClass;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;

import com.mays.snomed.ConceptBasic;
import com.mays.snomed.Snomed;
import com.mays.snomed.SnomedIsa;
import com.mays.snomed.subset.SubsetDb;
import com.mays.util.BasicLogger;
import com.mays.util.BasicProgressMonitorNoOp;
import com.mays.util.Config;
import com.mays.util.Sql;
import com.mays.util.subset.Modifier;
import com.mays.util.subset.Operator;
import com.mays.util.subset.SubsetExpression;
import com.mays.util.subset.SubsetFactory;

import java.nio.file.Files;
import java.nio.file.Paths;
import java.util.logging.Level;
import org.junit.Ignore;

@Ignore
public class SubsetDbTest {

	protected static Logger LOGGER;

	protected static Config CONFIG;

	protected static SnomedIsa SNOMED_DB;

	protected static SubsetDb SUBSET_DB;

	public final static long DIABETES = 73211009L;

	public final static long GESTATIONAL_DIABETES = 11687002L;

	public final static long CLINICAL_FINDING = 404684003L;

	public final static long ENTIRE_TOOTH = 302214001L;

	protected static UUID SUBSET_ID;

	@BeforeClass
	public static void setUpBeforeClass() throws Exception {
                Files.createDirectories(Paths.get("logs"));
		LOGGER = BasicLogger.init(SubsetDbTest.class, false, true);
		Sql.LOGGER = LOGGER;
		SubsetFactory.LOGGER = LOGGER;
		CONFIG = Config.load(System.getProperty("SnomedLoader.properties"), LOGGER);
		Connection conn = Sql.getConnection(CONFIG);
		SNOMED_DB = new SnomedIsa();
		SNOMED_DB.init(conn);
		SUBSET_DB = new SubsetDb(conn);
		SUBSET_DB.init();
		SUBSET_ID = UUID.randomUUID();
	}

	@AfterClass
	public static void tearDownAfterClass() throws Exception {
	}

	@Before
	public void createSubset() throws Exception {
		SUBSET_DB.deleteSubset(SUBSET_ID);
		SUBSET_DB.createSubset("test subset", SUBSET_ID);
	}

	@After
	public void deleteSubset() throws Exception {
		SUBSET_DB.deleteSubset(SUBSET_ID);
	}

	public void buildSubsetInclude(Modifier mod) throws Exception {
		buildSubsetInclude(DIABETES, mod);
	}

	public void buildSubsetInclude(long id, Modifier mod) throws Exception {
		ArrayList<SubsetExpression> exprs = new ArrayList<>();
		ConceptBasic con = SNOMED_DB.getConcept(id);
		LOGGER.info(con.getFullySpecifiedName());
		exprs.add(new SubsetExpression(Operator.INCLUDE, mod, con
				.getConceptId()));
		SUBSET_DB.saveSubsetExpression(SUBSET_ID, exprs);
		SUBSET_DB.buildSubset(SUBSET_ID, SNOMED_DB,
				new BasicProgressMonitorNoOp());
	}

	@Test
	public void includeDescendantsTest() throws Exception {
		buildSubsetInclude(Modifier.DESCENDANTS);
		Set<Long> s1 = SUBSET_DB.getSubsetMembers(SUBSET_ID);
		List<Long> s2 = SNOMED_DB.getDescendants(DIABETES);
		s2.add(DIABETES);
		assertTrue(s1.containsAll(s2));
		assertTrue(s2.containsAll(s1));
		assertTrue(s1.size() > 90);
		LOGGER.log(Level.INFO, "Subset size: {0}", s1.size());
		for (long c1 : s1) {
			ArrayList<Long> ch1 = SUBSET_DB.getChildren(SUBSET_ID, c1);
			ArrayList<Long> ch2 = SNOMED_DB.getChildren(c1);
			assertTrue(ch1.containsAll(ch2));
			assertTrue(ch2.containsAll(ch1));
			ch1 = SUBSET_DB.getParents(SUBSET_ID, c1);
			if (c1 == DIABETES) {
				ch1.remove(0l);
			}
			ch2 = SNOMED_DB.getParents(c1);
			ch2.retainAll(s2);
			assertTrue(ch1.containsAll(ch2));
			assertTrue(ch2.containsAll(ch1));
		}
	}

	@Test
	public void includeDescendantsOnlyTest() throws Exception {
		buildSubsetInclude(Modifier.DESCENDANTS_ONLY);
		Set<Long> s1 = SUBSET_DB.getSubsetMembers(SUBSET_ID);
		List<Long> s2 = SNOMED_DB.getDescendants(DIABETES);
		assertTrue(s1.containsAll(s2));
		assertTrue(s2.containsAll(s1));
		assertTrue(s1.size() > 90);
		LOGGER.log(Level.INFO, "Subset size: {0}", s1.size());
	}

	@Test
	public void includeChildrenTest() throws Exception {
		buildSubsetInclude(Modifier.CHILDREN);
		Set<Long> s1 = SUBSET_DB.getSubsetMembers(SUBSET_ID);
		List<Long> s2 = SNOMED_DB.getChildren(DIABETES);
		s2.add(DIABETES);
		assertTrue(s1.containsAll(s2));
		assertTrue(s2.containsAll(s1));
		assertTrue(s1.size() > 1);
		LOGGER.log(Level.INFO, "Subset size: {0}", s1.size());
	}

	@Test
	public void includeChildrenOnlyTest() throws Exception {
		buildSubsetInclude(Modifier.CHILDREN_ONLY);
		Set<Long> s1 = SUBSET_DB.getSubsetMembers(SUBSET_ID);
		List<Long> s2 = SNOMED_DB.getChildren(DIABETES);
		assertTrue(s1.containsAll(s2));
		assertTrue(s2.containsAll(s1));
		assertTrue(s1.size() > 1);
		LOGGER.log(Level.INFO, "Subset size: {0}", s1.size());
	}

	@Test
	public void includeOnlyTest() throws Exception {
		buildSubsetInclude(Modifier.ONLY);
		Set<Long> s1 = SUBSET_DB.getSubsetMembers(SUBSET_ID);
		List<Long> s2 = Arrays.asList(DIABETES);
		assertTrue(s1.containsAll(s2));
		assertTrue(s2.containsAll(s1));
		assertTrue(s1.size() == 1);
		LOGGER.log(Level.INFO, "Subset size: {0}", s1.size());
	}

	@Test
	public void includeDescendantsTestHierarchy() throws Exception {
		assertTrue(includeDescendantsTestHierarchy(ENTIRE_TOOTH));
		List<Long> cons = new ArrayList<>();
		// for (long id : SNOMED_DB.getChildren(Snomed.ROOT_ID)) {
		// cons.add(id);
		// cons.addAll(SNOMED_DB.getChildren(id));
		// }
		// Collections.sort(cons);
		// for (long id : cons) {
		// assertTrue(includeDescendantsTestHierarchy(id));
		// }
		cons = SNOMED_DB.getDescendants(Snomed.ROOT_ID);
		for (long id : cons) {
			int s = SNOMED_DB.getDescendants(id).size();
			if (s > 99 && s < 1000)
				assertTrue(includeDescendantsTestHierarchy(id));
		}
	}

	public boolean includeDescendantsTestHierarchy(long con) throws Exception {
		boolean error_p = false;
		buildSubsetInclude(con, Modifier.DESCENDANTS);
		Set<Long> s1 = SUBSET_DB.getSubsetMembers(SUBSET_ID);
		List<Long> s2 = SNOMED_DB.getDescendants(con);
		s2.add(con);
		assertTrue(s1.containsAll(s2));
		assertTrue(s2.containsAll(s1));
		// assertTrue(s1.size() > 100);
		LOGGER.log(Level.INFO, "Subset size: {0}", s1.size());
		for (long c1 : new TreeSet<>(s1)) {
			ArrayList<Long> ch1 = SUBSET_DB.getChildren(SUBSET_ID, c1);
			ArrayList<Long> ch2 = SNOMED_DB.getChildren(c1);
			ch1.removeAll(ch2);
			for (long x : ch1) {
				ConceptBasic c1_con = SNOMED_DB.getConcept(c1);
				ConceptBasic x_con = SNOMED_DB.getConcept(x);
				LOGGER.log(Level.SEVERE, "Subset child, not SNOMED: {0}\t{1}", new Object[]{c1_con.getLogString(), x_con.getLogString()});
				error_p = true;
			}
			ch1 = SUBSET_DB.getChildren(SUBSET_ID, c1);
			ch2 = SNOMED_DB.getChildren(c1);
			ch2.removeAll(ch1);
			for (long x : ch2) {
				ConceptBasic c1_con = SNOMED_DB.getConcept(c1);
				ConceptBasic x_con = SNOMED_DB.getConcept(x);
				LOGGER.log(Level.SEVERE, "SNOMED child, not subset: {0}\t{1}", new Object[]{c1_con.getLogString(), x_con.getLogString()});
				error_p = true;
			}
			// assertTrue(ch1.containsAll(ch2));
			// assertTrue(ch2.containsAll(ch1));
			ch1 = SUBSET_DB.getParents(SUBSET_ID, c1);
			if (c1 == con) {
				ch1.remove(0l);
			}
			ch2 = SNOMED_DB.getParents(c1);
			ch2.retainAll(s2);
			ch1.removeAll(ch2);
			for (long x : ch1) {
				ConceptBasic c1_con = SNOMED_DB.getConcept(c1);
				ConceptBasic x_con = SNOMED_DB.getConcept(x);
				LOGGER.log(Level.SEVERE, "Subset parent, not SNOMED: {0}\t{1}", new Object[]{c1_con.getLogString(), x_con.getLogString()});
				error_p = true;
			}
			ch1 = SUBSET_DB.getParents(SUBSET_ID, c1);
			if (c1 == con) {
				ch1.remove(0l);
			}
			ch2 = SNOMED_DB.getParents(c1);
			ch2.retainAll(s2);
			ch2.removeAll(ch1);
			for (long x : ch2) {
				ConceptBasic c1_con = SNOMED_DB.getConcept(c1);
				ConceptBasic x_con = SNOMED_DB.getConcept(x);
				LOGGER.log(Level.SEVERE, "SNOMED parent, not subset: {0}\t{1}", new Object[]{c1_con.getLogString(), x_con.getLogString()});
				error_p = true;
			}
			// assertTrue(ch1.containsAll(ch2));
			// assertTrue(ch2.containsAll(ch1));
		}
		return !error_p;
	}

}
